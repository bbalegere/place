-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2017 at 06:22 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `place2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `nooffers`
--

CREATE TABLE `nooffers` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nooffers`
--

INSERT INTO `nooffers` (`id`, `name`, `flag`) VALUES
(111, 'Abishek S', 0),
(183, 'Arunisha Battula', 0),
(193, 'Asish Sethi', 0),
(195, 'Asmit Das', 0),
(199, 'Atul Sharan', 0),
(201, 'Avinash Kumar L', 0),
(207, 'Bhalakumar V', 0),
(215, 'Biswajit Marndi', 0),
(241, 'Dhinesh R', 0),
(249, 'Divy Sharma', 0),
(255, 'Faizan Hashmi', 0),
(261, 'Geet Amrit', 0),
(263, 'Girish Krishna', 0),
(271, 'Himansu Sahu', 0),
(291, 'Jitendra Singh', 0),
(301, 'Jyoti Doley', 0),
(313, 'Kavya K', 0),
(325, 'Kuldeep Yadav', 0),
(329, 'Kunal Kejriwal', 0),
(381, 'Nandita B', 0),
(383, 'Navaneetha K P', 0),
(385, 'Nihar Khillar', 0),
(395, 'Pala Sriram', 0),
(397, 'Paldeep Das', 0),
(399, 'Parajdeep Singh', 0),
(411, 'Pragyagni Mehta', 0),
(413, 'Prakrin Kumar', 0),
(417, 'Pranay Siddharth', 0),
(421, 'Prathamesh L', 0),
(423, 'Pratik Pathrabe', 0),
(431, 'Preetisha Das', 0),
(439, 'Priyanka Attri', 0),
(449, 'Rachit Tanjeem', 0),
(461, 'Rakshith K', 0),
(471, 'Rathish R', 0),
(507, 'Sachin Xalxo', 0),
(521, 'Sandeep Sahu', 0),
(537, 'Satyaprakash S', 0),
(577, 'Shubham Saurabh', 0),
(583, 'Shubhankar S', 0),
(585, 'Shubhayu Bera', 0),
(591, 'Siddharth Singh', 0),
(623, 'Suzith D', 0),
(655, 'Viraj Rathwa', 0),
(659, 'Vivek K', 0),
(665, 'Vysakh Murali', 0),
(671, 'Ravi Kumar', 0),
(673, 'Ambarish Sharma', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
