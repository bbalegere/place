import pandas as pd
from fuzzywuzzy import process


def parse_shortlists(shname):
    name = str(shname).strip()
    if name == 'nan':
        return ''

    val = pndict.get(name, '')
    if val == '':

        val = process.extractOne(name, pdict)

        print(name + ' missing, using fuzzy wuzzy => ' + str(val[2]))
        print(val)
        val= val[2]

    return str(val)


pdf = pd.read_csv('processnames2.csv', dtype=object)
shl = list(pdf[['Name', 'Process No']].values)
pdict = {pn: n for n, pn in shl}
pndict = {n: pn for n, pn in shl}
a = process.extractOne("Shruti Newalee", pdict)
filename = 'shortlists.csv'
sldf = pd.read_csv(filename, dtype=object)
slnodf = sldf.applymap(lambda x: parse_shortlists(x))
slnodf.to_csv('rawshortlists2.csv', index=False)

