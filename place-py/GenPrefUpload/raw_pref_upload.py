import argparse
import pandas as pd

def get_raw_preference(fprefs, day, slot):
    prefs = pd.read_csv(fprefs)
    cols = list(prefs.columns)
    data = []

    for index, row in prefs.iterrows():
        for j in cols[1:]:
            d = {}
            d['process_id'] = row[cols[0]]
            d['company'] = j
            d['preference'] = row[j]
            d['relevant'] = 0
            d['day'] = day
            d['slot'] = slot
            data.append(d)

    output_df = pd.DataFrame(data)
    output_df = output_df[['process_id', 'company', 'preference', 'relevant', 'day', 'slot']]
    output_df.to_csv("system_rawprefs.csv",index=False)

# Run python3 prefs.csv 0 0 -> for day 0 slot 0 

if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('prefs', help='CSV with a matrix containing names and companies', metavar='prefs.csv')
    parser.add_argument('day', help='Shortlists File per company as CSV', metavar=0)
    parser.add_argument('slot', help='CSV with process names', metavar=0)
    args = parser.parse_args()

    get_raw_preference(args.prefs, args.day, args.slot)