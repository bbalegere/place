import pandas as pd
hotlistdf = pd.read_csv("hotlist_raw.csv", dtype=object)
comps = list(hotlistdf.columns.values)
comtupl = [(str(n),c) for c in comps for n in list(hotlistdf[c].dropna().values)]
finaldf = pd.DataFrame(comtupl)
finaldf.to_csv("system_hotlist.csv", header=["place_id", "company"], index=False)