import argparse
import xlsxwriter
import pandas as pd
import numpy as np
import pdb
from collections import OrderedDict

class ProcessPlacementData:

    def __init__(self, fshortlists, fpreferences, fprocessnames, fcompanies, flp=None, gd_mode=False):
        # Column series for excel - 200 columns taken
        self.excel_cols = [str(chr(ord('A')+int(i/26)-1) if i>=26 else '') + chr(ord('A')+i%26) for i in range(200)]
        self.gd_mode = gd_mode
        
        # Read data from CSVs
        self.shortlists, self.shcompanies, self.names = self.read_shortlists(fshortlists)
        self.prefs, self.comps3, self.names2 = self.read_preferences(fpreferences)
        self.process_names = self.read_process_names(fprocessnames)
        self.company_names = self.read_company_names(fcompanies)
        self.lp_list = self.read_lp(flp) if flp else [] # List of already placed

        # Remove lp names
        self.names = [n for n in self.names if n not in self.lp_list]
        self.names2 = [n for n in self.names2 if n not in self.lp_list]
        # Verify Data
        self.verify_data()
        
        # Initialize base dataframe and other reusable variables
        self.dfgridnoblanks, self.crit_max, self.critnames, self.prefsnew, self.criticalities = self.get_gridnoblanksdf()

    def verify_data(self):
        for vals in self.prefs.values():
            for val in vals.values():
                if val not in range(1, len(self.shcompanies) + 1):
                    raise ValueError('Incorrect preference ' + str(val) + '. It should be between 1 and ' + str(len(self.shcompanies)))

        print(set(self.shcompanies) ^ set(self.comps3))
        assert (self.shcompanies == self.comps3)

        missing = set(self.names) - set(self.names2)
        if len(missing):
            print('Preferences are missing for below names')
            print(missing)
            raise ValueError('Some names are missing')

        pmissing = set(self.names) - set(self.process_names)
        if len(pmissing):
            print('Process Names are missing for below names')
            print(pmissing)
            raise ValueError('Some process names are missing')

    def read_company_names(self, fcompanies):
        return pd.read_csv(fcompanies, header=None, index_col=0, squeeze=True).to_dict()    

    def read_preferences(self, fpreferences, typ=None):
        sldf = pd.read_csv(fpreferences, header=0, dtype=typ)
        sldf.columns = sldf.columns.str.strip()
        sldf[sldf.columns[0]] = sldf[sldf.columns[0]].astype(str).str.strip()
        sldf.set_index(sldf.columns[0], inplace=True)
        return sldf.to_dict('index'), sorted(sldf.columns.values), list(sldf.index.values)

    def read_shortlists(self, fshortlists):
        sldf = pd.read_csv(fshortlists, dtype=object)
        comps = list(sldf.columns.values)
        comtupl = [(c, str(n)) for c in comps for n in list(sldf[c].dropna().values)]    
        return dict((x, 1) for x in comtupl), sorted(comps), sorted(set([x[1] for x in comtupl]))

    def read_process_names(self, fprocessnames):
        sldf = pd.read_csv(fprocessnames, dtype=object)
        sldf.set_index(sldf.columns[1], inplace=True)
        return sldf.to_dict('index')

    def read_lp(self, flp):
        exnames = []
        with open(flp) as f:
            for index, csvline in enumerate(f):
                if index:
                    exnames = exnames + [str(x).strip().lower().replace(' ', '_') for x in csvline.strip().split(',') if len(str(x).strip()) > 0]
        return sorted(set(exnames))
    
    def get_gridnoblanksdf(self):
        crit = dict((n, sum(self.shortlists.get((c, n), 0) for c in self.shcompanies)) for n in self.names)
        critnames = sorted(crit, key=crit.get, reverse=True)
        crit_max = crit.get(critnames[0]) if len(crit) else 1
        prefsnew = dict()
        gridnoblanks = OrderedDict()

        for n in critnames:
            actpref = dict((c, self.prefs[n][c] * self.shortlists.get((c, n), 0)) for c in self.shcompanies if self.shortlists.get((c, n), 0) > 0)
            scaledpref = dict(enumerate(sorted(actpref, key=actpref.get), 1))
            process_detail = pd.Series([self.process_names.get(n).get('Name'),str(crit.get(n))])
            gridnoblanks[n] = pd.concat([process_detail,pd.Series(list(scaledpref.values()))], ignore_index=True)       
            for rank, c in scaledpref.items():
                prefsnew[n, c] = rank
        dfgridnoblanks = pd.DataFrame(gridnoblanks).transpose()
        dfgridnoblanks.columns = ['Applicant', 'Criticality'] + ['Choice '+ str(i+1) for i in range(crit_max)]
        return dfgridnoblanks, crit_max, critnames, prefsnew, crit
    
    def get_data_schedulers(self):
        dfgnb = self.dfgridnoblanks.copy(deep=True)
        for i in range(self.crit_max):
            dfgnb[dfgnb.columns[2+i]] = dfgnb[dfgnb.columns[2+i]].apply(lambda x: self.company_names.get(x))
        dfgnb.to_csv("GridNoBlanks.csv")

    def get_data_system_preferences(self):
        headers = ['process_id', 'company', 'relevant_pref']
        return pd.DataFrame([(k[0], k[1], 1 if self.gd_mode else v) for k, v in self.prefsnew.items()]).to_csv("system_shortlists.csv", header=headers, index=False)

    def format_board_excel(self, workbook, worksheet):
        format_company = workbook.add_format({'font_size':11, 'border':1})
        format_small = workbook.add_format()
        format_name = workbook.add_format({'font_size': 11, 'border':1})
        format_border = workbook.add_format({'border':1})
        
        for i in range(self.crit_max):
            col = 5+2*i
            worksheet.set_column(self.excel_cols[col]+':'+self.excel_cols[col], 18, format_company)
            worksheet.set_column(self.excel_cols[col+1]+':'+self.excel_cols[col+1], 2, format_small)
        worksheet.set_column('A:AZ',None,format_border)
        worksheet.set_column('B:B',18, format_name)
        worksheet.set_column('A:A', 3, format_small)
        worksheet.set_column('D:E', 2, format_small)
        return worksheet
    
    def get_company_shortlist_data_tracker(self, index, company):
        shortlist_rows = []
        for sh in self.shortlists:
            if company in sh and sh[1] in self.names:
                # First column 'Critical' if only >2
                shortlist_rows.append(['Critical' if self.criticalities.get(sh[1]) > 2 else '', sh[1], self.process_names.get(sh[1]).get('Name'), '', '', '', ''])
        shortlist_rows = sorted(shortlist_rows, key = lambda x: int(x[1]))        
        shortlist_n = len(shortlist_rows)
        if shortlist_n < 200:
            for i in range(200-shortlist_n): # Pad below - make sure there are total 200
                shortlist_rows.append(['','','','','','',''])
        else:
            print ("Max shortlist per company set to 200! update it!!")

        print_company_data = []
        print_company_data.append([str(index+1),company,str(shortlist_n),'','','',''])
        print_company_data.append(['Co','',self.company_names.get(company),'','','',''])
        print_company_data.append(['Cr','ID','Name','In','Out','P',''])
        print_company_data.append([index,'','','','','',''])        
        print_company_data.append(['','','','','','',''])
        print_company_data = print_company_data + shortlist_rows                
        return print_company_data

    def get_data_print_tracker(self, workbook):
        companies_data = None
        for index, company in enumerate(self.shcompanies):
            company_data = self.get_company_shortlist_data_tracker(index, company)
            if index == 0:
                companies_data = company_data
            else:
                for j, row in enumerate(companies_data):
                    companies_data[j] = row + company_data[j]
        worksheet = workbook.add_worksheet('trackers')
        for i, row in enumerate(companies_data):
            for j, column in enumerate(row):
                worksheet.write(i,j, column)
        
        # Formatting column widths
        cell_format = workbook.add_format({'font_size': '32', 'border': 1})
        merge_format = workbook.add_format({'align':'center','font_size': '32', 'border': 1})
        for i in range(0, 7*len(self.shcompanies), 7):
            worksheet.set_column(self.excel_cols[i]+':'+self.excel_cols[i], 22, cell_format)
            worksheet.set_column(self.excel_cols[i+1]+':'+self.excel_cols[i+1], 15, cell_format)
            worksheet.set_column(self.excel_cols[i+2]+':'+self.excel_cols[i+2], 45, cell_format)
            worksheet.set_column(self.excel_cols[i+3]+':'+self.excel_cols[i+3], 20, cell_format)
            worksheet.set_column(self.excel_cols[i+4]+':'+self.excel_cols[i+4], 20, cell_format)
            worksheet.set_column(self.excel_cols[i+5]+':'+self.excel_cols[i+5], 12, cell_format)
            worksheet.set_column(self.excel_cols[i+6]+':'+self.excel_cols[i+6], 150, cell_format)
            worksheet.merge_range(self.excel_cols[i+1]+'2:'+self.excel_cols[i+5]+'2', self.company_names.get(self.shcompanies[int(i/7)]), merge_format)

    def get_company_shortlist_data_pr(self, index, company):
        shortlist_rows = []
        for sh in self.shortlists:
            if sh[1] not in self.names:
                continue
            if company in sh:
                if self.gd_mode:
                    relevant_pref = 1
                else:
                    relevant_pref = None
                    for i in range(self.crit_max):
                        if company == self.dfgridnoblanks.at[sh[1],'Choice '+str(i+1)]:
                            relevant_pref = i+1
                            continue
                    if not relevant_pref:
                        print ("Cant find relevant pref for " + sh[1] + " !!!")
                shortlist_rows.append([self.criticalities.get(sh[1]), sh[1], self.process_names.get(sh[1]).get('Name'), self.prefs.get(sh[1]).get(company), '('+ str(relevant_pref) +')'])
        shortlist_rows = sorted(shortlist_rows, key = lambda x: int(x[1]))
        shortlist_n = len(shortlist_rows)
        if shortlist_n < 200:
            for i in range(200-shortlist_n): # Pad below - make sure there are total 200
                shortlist_rows.append(['','','','',''])
        else:
            print ("Max shortlist per company set to 200! update it!!")

        print_company_data = []
        print_company_data.append(['Co',self.company_names.get(company),'','','Ph'])
        print_company_data.append(['PR','','','',''])
        print_company_data.append(['Tr','','','',''])
        print_company_data.append([index+1,'',str(shortlist_n),'',''])                
        print_company_data.append(['Cr','ID','Name',company,'Relative'])
        print_company_data = print_company_data + shortlist_rows                
        return print_company_data

    def get_company_shortlist_data_boarder(self, index, company):
        shortlist_rows = []
        for sh in self.shortlists:
            if sh[1] not in self.names:
                continue
            if company in sh:
                if self.gd_mode:
                    relevant_pref = 1
                else:
                    relevant_pref = None
                    for i in range(self.crit_max):
                        if company == self.dfgridnoblanks.at[sh[1],'Choice '+str(i+1)]:
                            relevant_pref = i+1
                            continue
                    if not relevant_pref:
                        print ("Cant find relevant pref for " + sh[1] + " !!!")
                shortlist_rows.append(['0', sh[1], self.process_names.get(sh[1]).get('Name'), self.prefs.get(sh[1]).get(company), self.criticalities.get(sh[1]), '('+ str(relevant_pref) +')'])
        
        shortlist_rows = sorted(shortlist_rows, key= lambda x: int(x[4]), reverse=True)
        shortlist_n = len(shortlist_rows)
        if shortlist_n < 200:
            for i in range(200-shortlist_n): # Pad below - make sure there are total 200
                shortlist_rows.append(['','','','','',''])
        else:
            print ("Max shortlist per company set to 200! update it!!")

        print_company_data = []
        print_company_data.append(['Co',self.company_names.get(company),'','','',''])
        print_company_data.append(['PR','0','','','',''])
        print_company_data.append(['Tr','0','','','',''])
        print_company_data.append([index+1,'0',str(shortlist_n),'','',''])                
        print_company_data.append(['Cr','ID','Name','Pr','Cr','Rel'])
        print_company_data = print_company_data + shortlist_rows                
        return print_company_data

    def get_data_print_pr(self, workbook):
        companies_data = None
        for index, company in enumerate(self.shcompanies):
            company_data = self.get_company_shortlist_data_pr(index, company)
            if index == 0:
                companies_data = company_data
            else:
                for j, row in enumerate(companies_data):
                    companies_data[j] = row + company_data[j]
        worksheet = workbook.add_worksheet('pr')
        for i, row in enumerate(companies_data):
            for j, column in enumerate(row):
                worksheet.write(i,j, column)
        
        # Formatting column widths
        cell_format = workbook.add_format({'font_size': '23', 'border':1})
        for i in range(0, 5*len(self.shcompanies), 5):
            worksheet.set_column(self.excel_cols[i]+':'+self.excel_cols[i], 7, cell_format)
            worksheet.set_column(self.excel_cols[i+1]+':'+self.excel_cols[i+1], 15, cell_format)
            worksheet.set_column(self.excel_cols[i+2]+':'+self.excel_cols[i+2], 32, cell_format)
            worksheet.set_column(self.excel_cols[i+3]+':'+self.excel_cols[i+3], 10, cell_format)
            worksheet.set_column(self.excel_cols[i+4]+':'+self.excel_cols[i+4], 97, cell_format)

    def get_data_print_boarder(self, workbook):
        companies_data = None
        for index, company in enumerate(self.shcompanies):
            company_data = self.get_company_shortlist_data_boarder(index, company)
            if index == 0:
                companies_data = company_data
            else:
                for j, row in enumerate(companies_data):
                    companies_data[j] = row + company_data[j]
        worksheet = workbook.add_worksheet('boarder')
        for i, row in enumerate(companies_data):
            for j, column in enumerate(row):
                worksheet.write(i,j, column)
        
        # Formatting column widths
        cell_format = workbook.add_format({'font_size': '14', 'border':1})
        for i in range(0, 6*len(self.shcompanies), 6):
            worksheet.set_column(self.excel_cols[i]+':'+self.excel_cols[i], 3.75, cell_format)
            worksheet.set_column(self.excel_cols[i+1]+':'+self.excel_cols[i+1], 9, cell_format)
            worksheet.set_column(self.excel_cols[i+2]+':'+self.excel_cols[i+2], 21, cell_format)
            worksheet.set_column(self.excel_cols[i+3]+':'+self.excel_cols[i+3], 3.75, cell_format)
            worksheet.set_column(self.excel_cols[i+4]+':'+self.excel_cols[i+4], 3.75, cell_format)
            worksheet.set_column(self.excel_cols[i+5]+':'+self.excel_cols[i+5], 5, cell_format)

    def get_data_print_gd_board(self, workbook):
        header = ['ID', 'Criticality', 'Applicants']+['' for i in range(12)] + [self.company_names.get(c) for c in self.shcompanies]
        # Get data for NonZero
        board_data = []
        board_data.append(header)
        for applicant in self.names:
            row_data = [applicant, self.criticalities.get(applicant), self.process_names.get(applicant).get('Name')] + ['' for i in range(12)]
            company_row = []
            for company in self.shcompanies:
                cell_data = None
                for i in range(self.crit_max):
                    if company == self.dfgridnoblanks.at[applicant,'Choice '+str(i+1)]:
                        cell_data = i+1 if not self.gd_mode else 1
                        break
                if not cell_data:
                    cell_data = applicant + company
                company_row.append(cell_data)
            row_data = row_data + company_row
            board_data.append(row_data)
        
        # Get data for Zero
        znames = set(self.names2)-set(self.critnames)
        board_data_z = []
        board_data_z.append(header)
        for applicant in sorted(list(znames)):
            row_data = [applicant, 0, self.process_names.get(applicant).get('Name')] + ['' for i in range(12)]
            company_row = []
            for company in self.shcompanies:
                cell_data = applicant + company
                company_row.append(cell_data)
            row_data = row_data + company_row
            board_data_z.append(row_data)
        
        worksheet = workbook.add_worksheet('GD_Board_NonZero')
        worksheet_z = workbook.add_worksheet('GD_Board_Zero')
        
        # Write to sheet
        rel_format = workbook.add_format({'align':'center', 'font_size': '48', 'border':1, 'bg_color':'silver', 'bold': True})
        for i, row in enumerate(board_data):
            for j, column in enumerate(row):
                if j>=15 and type(column)==int:
                    worksheet.write(i,j, column, rel_format)
                else:
                    worksheet.write(i,j, column)
        
        for i, row in enumerate(board_data_z):
            for j, column in enumerate(row):
                if j>=15 and type(column)==int:
                    worksheet_z.write(i,j, column, rel_format)
                else:
                    worksheet_z.write(i,j, column)
        
        # Formatting columns
        self.format_worksheet_gd_board(workbook, worksheet)
        self.format_worksheet_gd_board(workbook, worksheet_z)
        

    def format_worksheet_gd_board(self, workbook, worksheet):
        cell_format_name = workbook.add_format({'font_size': '72', 'border':1})
        cell_format_crit = workbook.add_format({'align':'center', 'font_size': '50', 'border':1})
        cell_format_company = workbook.add_format({'align':'center', 'font_size': '48', 'border':1})
        cell_format_company.set_text_wrap()
        worksheet.set_column(self.excel_cols[0]+':'+self.excel_cols[0], 58, cell_format_name)
        worksheet.set_column(self.excel_cols[1]+':'+self.excel_cols[1], 42, cell_format_crit)
        worksheet.set_column(self.excel_cols[2]+':'+self.excel_cols[2], 212, cell_format_name)
        worksheet.set_column(self.excel_cols[3]+':'+self.excel_cols[14], 40, cell_format_crit)
        worksheet.set_column(self.excel_cols[15]+':'+self.excel_cols[15+len(self.shcompanies)], 40, cell_format_company)

    def get_data_print_board(self):
        # 2 Sheets to be written to workbook
        dfgridnoblanks, dfgridnoblanks_zero = self.get_print_dataframe()
        for i in range(self.crit_max):
            dfgridnoblanks[dfgridnoblanks.columns[4+2*i]] = dfgridnoblanks[dfgridnoblanks.columns[4+2*i]].apply(lambda x: self.company_names.get(x))
        # Create XLS
        writer = pd.ExcelWriter("Print_GridNoBlanks_Board.xlsx", engine='xlsxwriter')
        dfgridnoblanks = dfgridnoblanks.sort_index()
        dfgridnoblanks_zero = dfgridnoblanks_zero.sort_index()
        dfgridnoblanks.to_excel(writer, sheet_name='NonZero')
        dfgridnoblanks_zero.to_excel(writer, sheet_name='Zero')
        workbook  = writer.book
        worksheet = writer.sheets['NonZero']
        worksheet_z = writer.sheets['Zero']
        # Excel formatting
        worksheet = self.format_board_excel(workbook, worksheet)
        worksheet_z = self.format_board_excel(workbook, worksheet_z)
        writer.save()

    def get_print_dataframe(self):
        # Obtain modified dataframe in format for board
        dfgridnoblanks = self.dfgridnoblanks.copy()
        
        # 2 Extra Columns
        dfgridnoblanks.insert(2, ' ', np.nan, allow_duplicates=True)
        dfgridnoblanks.insert(2, ' ', np.nan, allow_duplicates=True)
        
        # Extra column after every company_name
        for i in range(self.crit_max):
            dfgridnoblanks.insert(5+2*i, ' ', np.nan, allow_duplicates=True)
        
        # Create another df for zero shortlist candidates
        znames = set(self.names2)-set(self.critnames)
        dfgridnoblanks_zero = pd.DataFrame(data=None, columns=dfgridnoblanks.columns)
        empty_col = [np.nan for i in range(len(dfgridnoblanks.columns)-2)]
        for name in list(znames):
            dfgridnoblanks_zero.loc[name] = [self.process_names.get(name).get('Name'), '0'] + empty_col
        return dfgridnoblanks, dfgridnoblanks_zero

    def get_data_all_prints(self):
        workbook = xlsxwriter.Workbook('Print_Tr_Pr_Boarder_GDBoard.xlsx')
        placement_data.get_data_print_board() # Print for Board (interview days) - Nonzero & Zero (2 Sheets)
        placement_data.get_data_print_pr(workbook) # Print shortlists for PR
        placement_data.get_data_print_tracker(workbook) # Print shortlists for trackers
        placement_data.get_data_print_gd_board(workbook) # Print board for GD days
        placement_data.get_data_print_boarder(workbook) # Print boarder for schedulers
        workbook.close()


# Run - python3 GenPrefUpload.py shortlists.csv prefs.csv names.csv
# Add -l lp.csv if LP
# Add -gd if GD slot
if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('shortlists', help='Shortlists File per company as CSV', metavar='Shortlists.csv')
    parser.add_argument('prefs', help='CSV with a matrix containing names and companies', metavar='prefs.csv')
    parser.add_argument('processnames', help='CSV with process names', metavar='processnames2.csv')
    parser.add_argument('companies', help='CSV with company codes and names', metavar='companies.csv')
    parser.add_argument('-l', '--leftprocess', help='CSV with a list of candidates who have left the process', metavar='lp.csv')
    parser.add_argument('-gd', action='store_true', help='If set, ignores preferences', default=False)

    args = parser.parse_args()
    placement_data = ProcessPlacementData(args.shortlists, args.prefs, args.processnames, args.companies, args.leftprocess, args.gd)
    
    placement_data.get_data_system_preferences() # Upload to System - Relevant shortlists with preferences
    placement_data.get_data_schedulers() # Data - Email to schedulers
    placement_data.get_data_all_prints() # Generate workbook for printing purposes