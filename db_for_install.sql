-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2018 at 03:02 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `placement`
--

-- --------------------------------------------------------

--
-- Table structure for table `backuplog`
--

CREATE TABLE `backuplog` (
  `id` int(11) UNSIGNED NOT NULL,
  `backup_time` int(11) UNSIGNED DEFAULT NULL,
  `backup_file_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `companylist`
--

CREATE TABLE `companylist` (
  `id` int(2) NOT NULL,
  `company` varchar(30) DEFAULT NULL,
  `company_name` varchar(30) NOT NULL,
  `panels` int(1) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  `company_status` int(11) NOT NULL,
  `day` varchar(5) DEFAULT NULL,
  `slot` varchar(5) DEFAULT NULL,
  `freeflow` int(1) DEFAULT NULL,
  `company_room` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companylist`
--

INSERT INTO `companylist` (`id`, `company`, `company_name`, `panels`, `type`, `company_status`, `day`, `slot`, `freeflow`, `company_room`) VALUES
(14, '00dummy', 'zzDummy', 1, 'i', 0, '0', '0', 0, ''),
(15, 'zzlp', 'zzLP', 1, 'i', 0, '0', '0', 0, NULL);
-- --------------------------------------------------------

--
-- Table structure for table `entry`
--

CREATE TABLE `entry` (
  `id` varchar(40) NOT NULL,
  `place_id` int(3) DEFAULT NULL,
  `student_roll` int(7) DEFAULT NULL,
  `student_section` varchar(1) DEFAULT NULL,
  `student_name` varchar(46) DEFAULT NULL,
  `student_gender` varchar(6) DEFAULT NULL,
  `previous_company` varchar(30) NOT NULL,
  `company` varchar(30) DEFAULT NULL,
  `next_company` varchar(30) DEFAULT NULL,
  `type` varchar(1) DEFAULT NULL,
  `gd_round` int(11) DEFAULT NULL,
  `gd_panel` int(11) NOT NULL,
  `shortlist` int(1) DEFAULT NULL,
  `preference` int(2) DEFAULT NULL,
  `stat1` int(1) DEFAULT NULL,
  `time1` int(1) DEFAULT NULL,
  `stat2` int(1) DEFAULT NULL,
  `time2` int(1) DEFAULT NULL,
  `stat3` int(1) DEFAULT NULL,
  `time3` int(1) DEFAULT NULL,
  `stat4` int(1) DEFAULT NULL,
  `time4` int(1) DEFAULT NULL,
  `stat5` int(1) DEFAULT NULL,
  `time5` int(1) DEFAULT NULL,
  `stat6` int(1) DEFAULT NULL,
  `time6` int(1) DEFAULT NULL,
  `stat7` int(1) DEFAULT NULL,
  `time7` int(1) DEFAULT NULL,
  `stat8` int(1) DEFAULT NULL,
  `time8` int(1) DEFAULT NULL,
  `stat9` int(1) DEFAULT NULL,
  `time9` int(1) DEFAULT NULL,
  `stat10` int(1) DEFAULT NULL,
  `time10` int(1) DEFAULT NULL,
  `stat11` int(1) DEFAULT NULL,
  `time11` int(1) DEFAULT NULL,
  `hotlist` tinyint(1) NOT NULL DEFAULT '0',
  `later` tinyint(1) NOT NULL DEFAULT '0',
  `latertime` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `gridnoblanks`
--

CREATE TABLE `gridnoblanks` (
  `id` int(3) NOT NULL,
  `day` varchar(15) NOT NULL,
  `slot` int(3) NOT NULL,
  `criticality` int(3) DEFAULT NULL,
  `pref1` varchar(70) DEFAULT NULL,
  `pref2` varchar(70) DEFAULT NULL,
  `pref3` varchar(70) DEFAULT NULL,
  `pref4` varchar(70) DEFAULT NULL,
  `pref5` varchar(70) DEFAULT NULL,
  `pref6` varchar(70) DEFAULT NULL,
  `pref7` varchar(70) DEFAULT NULL,
  `pref8` varchar(70) DEFAULT NULL,
  `pref9` varchar(70) DEFAULT NULL,
  `pref10` varchar(70) DEFAULT NULL,
  `pref11` varchar(70) DEFAULT NULL,
  `pref12` varchar(70) DEFAULT NULL,
  `pref13` varchar(70) DEFAULT NULL,
  `pref14` varchar(70) DEFAULT NULL,
  `pref15` varchar(70) DEFAULT NULL,
  `pref16` varchar(70) DEFAULT NULL,
  `pref17` varchar(70) DEFAULT NULL,
  `pref18` varchar(70) DEFAULT NULL,
  `pref19` varchar(70) DEFAULT NULL,
  `pref20` varchar(70) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) UNSIGNED NOT NULL,
  `https` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_addr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_host` int(11) UNSIGNED DEFAULT NULL,
  `remote_port` int(11) UNSIGNED DEFAULT NULL,
  `remote_user` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_uri` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_base` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path_element_0` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path_element_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path_element_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path_element_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_referrer` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_user_agent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `request_time_float` decimal(10,2) DEFAULT NULL,
  `processing_time` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `raw_preferences`
--

CREATE TABLE `raw_preferences` (
  `id` int(4) NOT NULL,
  `process_id` int(3) NOT NULL,
  `company_code` varchar(70) NOT NULL,
  `preference` int(2) NOT NULL,
  `relative` int(2) NOT NULL,
  `day` varchar(15) NOT NULL,
  `slot` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `studentdata`
--

CREATE TABLE `studentdata` (
  `id` int(3) NOT NULL,
  `name` varchar(70) NOT NULL,
  `process_name` varchar(46) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `studentlist`
--

CREATE TABLE `studentlist` (
  `id` int(3) NOT NULL,
  `student_roll` int(7) DEFAULT NULL,
  `student_section` varchar(1) DEFAULT NULL,
  `student_name` varchar(46) DEFAULT NULL,
  `student_gender` varchar(6) DEFAULT NULL,
  `current_location` varchar(30) NOT NULL,
  `next_company` varchar(30) NOT NULL,
  `placed` int(11) NOT NULL,
  `placed_company` varchar(30) NOT NULL,
  `dream` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
-- --------------------------------------------------------

--
-- Table structure for table `userlog`
--

CREATE TABLE `userlog` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `loggedin` int(11) UNSIGNED DEFAULT NULL,
  `logintime` int(11) UNSIGNED DEFAULT NULL,
  `logouttime` int(11) UNSIGNED DEFAULT NULL,
  `loginhash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remote_host` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namak` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `displayname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `role`, `company`, `hash`, `namak`, `displayname`) VALUES
(1, 'vader', 'admin', '0', 'f8e3b80bc294477a23bac20152143d32afd6b1df5a26a9d0a1e813f845baa2dc4b567083356445157aebeea5d6e898f20ed225cda3a2ed55be4efb5aee6a570f', '175944027857eb0d25e6e3f9.30864827', NULL),
(310, 'rahul_ct', 'control', '0', '098005f6222e99bfd5b8b5d7f9ec8ca13a3279e292d336abc5d98fa97a1db291220eaaf6e989fbe908cbbc6dbba90203032200e0e9579f974fa531ddeda11456', '57015959859cb9dd162cea6.09256489', NULL),
(439, 'admin', 'admin', '0', '29ab9364e163588458b435ef38de7621d05585fc8f26e8abe3cfe7d7f5b566ab938fa9e39ae8202c353b799ac2ebac6e8d26046a51b1034e88371db6e90d4a06', '11792614359ea381d14dbc3.92371508', ''),
(494, 'priyank_ct', 'control', '0', 'a59fcbdff8396acae200a608938ca2d200f734809ccd3c5815f7b5e6a1c51b1df90663f6f12dbb62b66b0405bae86cf641375dd70b3dac653b322707adb01ced', '5612977745a4e3146d897e2.39661587', 'Priyank Yadav'),
(512, 'ankitr17', 'control', '0', 'da42e0b5a70c74c61cbabbbf6fbfc19b25ae69d5eecf4626166ef4d141bf56721cb91f44391f0c13092748c71fccb97a80414e46fe74c3934cb9aa0e7b13bb9a', '362231495a4e3fdcae0e10.89107492', 'Ankit Rathore'),
(513, 'abhimanuej17', 'control', '0', '5ed4f8691f37bae7b2709baff51e849c68e8403b432b54e7cdf12a7561cc8f987a51bee33813e9f46e9741ab78f28413e1bba2747a81cedf5ef1b9664ac83d99', '971343235a4e401940b315.78915758', 'Abhimanue'),
(514, 'dhrutid17', 'control', '0', '3928a868394205e500903cd163d9f7ef92457bb37cca16622dc0725c5d20cf4843c0cbb2d0b50c23dbfb8240922f9195fb85f34c2f8cff8171a948d171955faf', '10098273225a4e409b275fb8.95395498', 'Dhruti'),
(515, 'ankitg17', 'control', '0', 'c52fbfb226dfa56dab88a7bb30b7bd894af20c354aa8f95e2aedcdd89380b82379b485b90982bb2cfb8db5c07eb47fffb78829fa3fc3ee9339b4d2df981c21e4', '4824055955a4e410832f949.31860301', 'Ankit Gupta'),
(516, 'ankurag17', 'control', '0', 'a7a3cb5ada3ea110207ff5072c0b6864eb42a67f5ce423d349df3c6bb093f810b5761a457ee7988c4df609cf6ba8226b4030a8279196f5c314d12606396bf171', '11097095325a4e4120880966.80403375', 'Ankur Agarwal'),
(517, 'payalg17', 'control', '0', 'dd856314428cd508fb87c560fa192deb89aebfd746dcaabcce7d7f95473a578ee145cac4d99ab1e317716295c6e8e7e88971378509752bde549382401b0b564b', '20867215285a4e4121f1df36.75924567', 'Payal Gupta'),
(518, 'abhishekk17', 'control', '0', '90265b0f5aba86e8efe98bba0fa7f2f0087efa055085538839c46d1ea3febb646764b6dc01fb314971025ef3af3452107246bb91acee645ac8ce2e469707d8ae', '13563557135a4e41d7a5a743.25840598', 'ABHISHEK kUMAR'),
(519, 'ajits17', 'control', '0', '5ba6a840f3f92c3fc306fe6887ff1de348756553d99cfe63911ba5e124d2bb7fb8d4a0d2baad9ce3b5c9f927438aa89d6d5fb7e9617f738f98843cadd1b39c14', '20728901765a4e4245e60224.13013754', 'ajit singh'),
(520, 'piyusha17', 'control', '0', 'c0144057cd8f10537e35d1e918a7809c30a903d5df40e8e0a5139e07cec492e6891932ae09a9f6f37537bf46e7b31b4ffbad95449ab64b6dadc993dd58223c44', '6148284735a4e436e3b94f6.47894390', 'Piyush Agrawal'),
(521, 'ajitu17', 'control', '0', 'b0fb01fee47435d41e5223299e4dbc8a42e94e04b3558a710673a76a5b3d183882c67593b1d3eb254838191306bcedee824881ed4078ec4216a09b6bfa6e3b6d', '16424836345a4e439d60bd39.30584311', 'Ajit Singh Umath'),
(523, 'bharatk17', 'mobile', '0', '1116abde492e9c33998a0b1036a5adbe2313d7601e0e2520284e81acbeb78dc44fb817fdccd4870d2a3d7004e8aa74718ee6ffaa482a7ac55ad9c00d17df83ec', '2830647745a4e4abbcddf63.45343055', 'Bharat Khanna'),
(524, 'shivamsh17', 'mobile', '0', '928e44d8e01aa7b4969f7bd194dcaf762e72068f626dbb8f8794e6ad5fa2070cc3205ed9bd5bec42e480c963309388daf9a273629e2ea6977f9432bb9c72d6ce', '20311670665a4e4ad39a80f7.49900275', 'Shivam Shekhar'),
(525, 'riddhimanp17', 'company', 'marico', '72ca19838f1ceac48b7b09cb8e45f07fb423a2fc411edb30fa64752990080fc463e8539da9e59c7f0cb9dd114af0f014c403c7e51eba75142908139b16320c17', '5961273365a4e4b10b66f52.77548962', 'Riddhiman Pal'),
(526, 'akshitas17', 'company', 'zzdummy', '56b6b3045995f62ad5b3367168d724dfd36ebf7255b4eef9ade97eb91826764364e4a1b23158b2797ef582bfd0ac52b014969d66140539dd010fe7c83dd6896d', '187613905a4e4b2d6446e6.88926135', 'Akshita Singh'),
(527, 'pragneshp17', 'company', 'zzdummy', '672a1fa0889ce2074fc35b8e5dec54f95a00387398dcad42960146fd109088fef08698ed2ed6b424902b38b9b3bfd878c2d7b75bfc6eb63714d04c70af9afa76', '6253657045a4e4b57a6df76.33066550', 'Pragnesh Pahuja'),
(529, 'sasidharr17', 'mobile', '0', '4ed5c9d2e9782f5c7a68caa93b03357be4472ee8129c56a49b63251e6eb4c5cfa76ecb82a71263404ad07d34b9fa771614854c333ad63676e0d196a88396e047', '16364113515a4e4ba85fc1f3.60558029', 'sasidhar reddy'),
(530, 'shubhamra17', 'mobile', '0', '44a52ce1f964c74cca3d709238f5ec0d742cea7338f936bb83b8d9232ae8e888cea81741a20feec81d4e63b389b533b5a2902902225bd103c3d8dd48fe3afa16', '11848498775a4e4bb3ac9c80.86311949', 'Shubham Raj'),
(531, 'ankushs17', 'company', 'abi', 'f96645ff9f131fdf827831c9bcb002a6f37f1235a60848268c1d35bc5f27d8db0c007f030f9f89f2e5aa268eeb5f5b695eafa52148a0048ec9925c8760cdc627', '10255996995a4e4bd6e71d88.84082727', 'Ankush'),
(533, 'anuragg17', 'company', 'pgit', '3a07b677ed6f51af33694421cdc14ddadd01511035040c818a7b838e73ea75531c030ad788380bc18096fbbaf67dc19632961c3b782ccde7ad1557adc4d92ab3', '5720137165a4e4c0d0cd466.66711664', 'Anurag Kumar Gupta'),
(538, 'arkak17', 'company', 'bain', '0279e6e1df6f1c81670b39f4bbe5053a58a5c19fbe9aa85eae00c7215c15c23c04c9b8de592d8979678430b41a449f987712345f6d6b7f3642ae1aee295d6f6a', '14252185455a4e4c7a4a4193.04470934', 'Arka Pravo'),
(539, 'vaibhav17', 'mobile', '0', 'b6dedbe75aecbb57664484c3e86592cc023d1cd2c3b1614bb7e50396f0e1b1c93863b47f04ff60391366855bd77028de9a75350f531694813687b5bd61d127f6', '11376779315a4e4c90e8bad5.43396804', 'Vaibhav'),
(540, 'pratiksham17', 'company', 'abi', '9c84dc3f23d1a0aabcf817a2c807e81a2c62afc7fbcb25579d577da272350a6870d19b7eec8e328bfb0dc401f52b69d109c7c8e70b68224e7bef7076a9c467ce', '13218154165a4e4caddc7582.23809087', 'Pratiksha'),
(544, 'anuv17', 'company', 'zzdummy', '0094c0107d0400d032c56da20abd7a811d052121a88f2f163c111d18a1a618d9e83ace953a69f69d60faf2562b8a9e7f3ccf53d6b5978e470111bf73142405aa', '20729818975a4e4cf0d0e233.73583206', 'Anu Varghese'),
(545, 'shreenivasr17', 'company', 'zzdummy', '59d02426eb9e6caed1745401677b69725999255171856ef63867a8be2d277cae5ab0818aa9e25759d6bc1ed26e447b969476ef208a64bb2c92c1becab0444c3b', '7126493865a4e4cf138ea96.00581534', 'Shreenivas P R'),
(546, 'manishr17', 'company', 'samsunge', '61e2d176cd5470ae770ca5d3d33783806c8a4b7e21e9279f4bd38e41cdb5b7c4a77ba0bd9206fc12a56bbec008fd6bd8ecaa66ce224be0fd0598923b3e7545f1', '2889136065a4e4d11e79f47.41443249', 'Manish Rathi'),
(547, 'kalpajyotim17', 'company', 'barclays', '3cd1783d7b9b68158ae8d511b7a2e037d2d3eb5f3f6a3eef673b14e223d33c984c8ba907162247dc1f4c8352d525432a937d50a165284fb55bbd9e8507c7f904', '10772990665a4e4d1dd124a5.35641510', ''),
(548, 'shivams17', 'company', 'zzdummy', 'b8cdff5aa742dfb9029c0697498d09432fa957005ea33f59745407b078c886de5b039d34cf0e2033f3703494d7ddb6a022451f417b639da7e183827e161548ad', '12146068455a4e4d2e296ca5.69606688', 'Shivam Singhal'),
(549, 'abhishekks17', 'company', 'zzdummy', '4bf51d9b3a26c3202393b7b011f5616f77b71b469048dbd15e9e4eb61bcc26b036eb601a2c750b8106d996a093f848e5592ad0a1cc7561476283d93ce793b97d', '8078817475a4e4d4f5befe9.31237158', 'Abhishek Kshirsagar'),
(550, 'abhishekr17', 'company', 'zzdummy', '102b4f0f5356ac90f60c8f6454a6c8472698a38d5a340bf37704bdbcc8b7ecb59069cfd2bcc4ee0aea0e1998e78239b8f85c65d2fb8f4f3e15cdb90c27c7650f', '16151087825a4e4d5d54db68.85526907', 'Abhishek Raj'),
(551, 'mayanku17', 'company', 'zzdummy', '147ec0dfbf81856dd27e4b05ff71128e5d3fda7430dd037eff0b4a62d6ac24bc4f978d3fb1c224c590737454bce3aa96ed1c5d4a8dd18a22d219fce4ed3f77cb', '9763039975a4e4d80a97093.46027891', 'Mayank Upadhyay'),
(552, 'sahilg17', 'company', 'jjcon', '2a3b28903914e24d87727e1a6e9a016ce6137c2742fe4f8db0bb542969f9f41020cf8e8e76857d91417f7501ef315e87c8c42f8bd00fdca3e18e19afe5c2c844', '4952317845a4e4d8667b271.88753921', 'Sahil Gupta'),
(553, 'hemantht17', 'company', 'zzdummy', '825afc93006227df8ac9758d5431a2a1d62585a8ccc527e51874398f6d01eb5400f6ad0f7351eeb9d5ca5b9d48f3d3bb4597b9c8090058057ac8edb9edf6a369', '7804779955a4e4da62c9d55.64684149', 'Hemanth '),
(554, 'roopalis17', 'company', 'zzdummy', '766042a6f06812fd2f7b62b2f0ef2700bf5760fcfb0f7ab2a4029d7d03eb1073a5daa1367f68c4e5da14feb56c39aab53b4d6c9b3668cfaaf183c4f3c87615e7', '7833599165a4e4dc4cdb335.91674677', 'Roopali Singh'),
(555, 'piyushm17', 'company', 'tas', 'ee08c2b2cd0e084f7205ff70a077aa9f9b509c67665d663dc40dc427b2219fbba5fab0966128c4551f8b9b936f69be8541702eb1f29cc5f519e1272ab19fbc6a', '18589684665a4e4de06f4158.86736059', 'Piyush Mittal'),
(556, 'naliny17', 'company', 'marico', '5c4c6f3f9919773377cb1069812293fe5276d0efff2bd3ebb3b50e5e4f9e5198534a57e824acec7512d76471ccdebcbec1e206295cb2db0d73eafe7c7c35283b', '10853620495a4e4e05704ad5.37162670', 'Nalin Yadav'),
(557, 'rahulk17', 'company', 'mondelez', '8f56dc637ca4c30deab9073dd34c02e0890f3778e1af56cf8a028027168c16ef8041463fc8d7cfb3a4c34dd8ee0f29c6ceb901ccfd4560d770004bdf27abc5f1', '10119174915a4e4e06c72207.13368976', 'Rahul Kumar'),
(558, 'puskars17', 'company', 'apit', 'cd0c89eba1e480dba38f00290350146b58fb2386feba541c3883c30dcc53e8acbcd68d4d72c59a50f6819c7426b5da59a26b7e6fffe132983009e3921d175cbb', '12394531515a4e4e242594b2.71171323', 'Puskar Sharma'),
(561, 'aksheyr17', 'floor', '0', '84e97f40d36beca87b3ca7a4ea64b687b0f2bb84dc1d71d4bbd5f91983f61b3b45b2d7871481d4a8739a7ae2553ee77732741f123727a4bcd0053f6e9a0d9b47', '15197738595a4e4e77191293.98765076', 'R Akshey'),
(563, 'siddharthav17', 'company', 'bcg', 'b870a4952fb3aafe7b6200f79267917e9ef67c4a9c67f05333b9fb424b8ee6cd0cec04914ee83d21df79e758a0ff9ab12ad4d6689772bb0eacc09752140c4d98', '18737040955a4e4ea2a40a06.88044966', 'Siddhartha V'),
(564, 'kothandaramanr17', 'company', 'apit', 'c252252fa5558790256529badc33eb5c1d06fb26ec24b641b1d940c3ba74f7847e8ffb304d7ca2229f44dfd508cf08697a42b07f7c641eefefcfb77a939580b2', '15721762445a4e4ea48ef324.37620092', 'Kothandaraman'),
(565, 'arjunc17', 'mobile', '0', 'a125845a5b9123514e0dedeb4d1344833724b5885dd6f294eb609f093c59834156fde497787d8ce3cf246bec936542de8ba21e2bd2e464a8a6cc496b8a0c01a5', '17089251945a4e4ed4b32f17.33916126', 'arjunc'),
(566, 'prateekg17', 'company', 'zzdummy', '50c7fad2aee041b30a0ec6b5ac6dfed53d2929f37d04feb1e5d7a09ac733eec8d6d4e876dc8dddcb3eb2e6dbe4d302d569851aa189726f76cc66216516d1be6f', '20881544255a4e4ee59d9ca8.11092924', 'Prateek Sai Gahlot'),
(568, 'harikab17', 'company', 'strategy', '0151a27e9379d0dfd01db30d7a722cc4ffe51b817c7c5526c76fc126945516c00171155cf064ca415964ed4ae60f677e12b3df520a9f3597faf0cdc37870b9ce', '6018691935a4e4f17d82fe6.07482943', 'Harika Basamgari'),
(569, 'yashv17', 'company', 'pgit', '6005b3f6b99195033a3cd6618518670619bb0b416d27e28d54310c7035cb8c1bcd6bad3a6b4490cad9a936e7b3cabd332eb099b4bc1ef158c74ec59d112c1406', '5827284985a4e4f1a57ebe6.65361580', 'Yash Verma'),
(570, 'anirudhy17', 'company', 'zzdummy', '9912d1d867bd8ccad86759578f253ce51c12a4283c219b7726a9bf2f4eed1df5231729dd985382e132a4cd5004304ee7cb99771f2c39e30170ee04460b432179', '21064822885a4e4f3e658525.20416438', 'Anirudh Y'),
(571, 'rajeshj17', 'mobile', '0', '4cc9d3e32cdf34a5775c556ec3577535da507fb975163adab3165c2621a80e5426d55d9aabf16effe85415a7b4e608ed07425e7fcc12db27e7d3b9686e2785cc', '11119769645a4e4f5f4c9314.79840646', 'Rajesh J'),
(572, 'nitishku17', 'company', 'accenture', 'f52c2696c44e7b63337826ba4fd3dc784b3db506ac4e6dfb733c31ad855709d016c6dd000cbc656d6cb99a1442a37f3bb12bd6a7c8df1944ee4997d30965f347', '893524495a4e4f6c480ac4.20634338', 'Nitish Kumar'),
(573, 'prathyushap17', 'mobile', '0', 'f694a3d781e027716bc52aefc6c5cc12c281a320dd34d63af0dd9fc9ed9d4bf6349df3b3e38d6138225d5f7a051e777e298dca8109bc34d4f61cfd8c6ba7fe28', '8987155145a4e4f8a4eda72.94025991', 'Prathyusha'),
(574, 'sabarishg17', 'company', 'zzdummy', '843bfec40d6d3ed2d4bb86437431a1e3486dc03216a8628ac8931c5dccefa276cd7c0ecf1d05a91d9f41a2979604c3f7642400af63a928b96bbf69350dc27338', '12974044835a4e4f9c9ef725.55291168', 'sabarish G'),
(575, 'harika17', 'floor', '0', '5b501a2563a544e25f90b8f421de899043fc07a37e38f5fc9fc013c028b6180ef8848a5ab7ca36ff55718090243f290c59d16790de4879322861dd115d322f08', '12523964915a4e4fc5e94a99.80255446', 'Hari Kakarla'),
(576, 'prasannak17', 'floor', '0', '232b9bdee1819bd7615f5acd1b6572bee1ea45146d0876c0e6bbcd665e8c787e1c597aa1c147b2280e43227a44f79733a643f6aae9c2b06b66ce104befe5151a', '5784967825a4e4fc8578563.74034579', 'prasanna sakthi '),
(577, 'sreyanshd17', 'floor', '0', '025f94121abb7951470fdd5da51772e04e9ea7702447d9a3f43b3a96f30b85b783fd5230c476c85db1e04ab9375aa44943bea3764d812fc8ab9cd073d37ae9ab', '6454990055a4e4fcd9f85e4.67867292', 'Sreyansh Dugar'),
(578, 'shreyasr17', 'floor', '0', 'a73101b85c2a9c93003dfcf2ac2ac583fe3fbd09cd27a3a6c0dc67e8dc63ed95a634e344223a59c6610f0b8941b3bae11edc94c1e03d3c3114715ab01df065ba', '18708211555a4e4fe6183e88.57119134', 'Shreyas Raman'),
(579, 'yuvarajr17', 'company', 'avendus', '6c1d0a015ee5b7de5083aa22703c8a3abc09d7384790522a75875106680211bac53990e4712c305eebc6c26886d327bf9e25ad49aa51f5db6997c1588ad1d6b4', '4153382125a4e4fe652aa41.91166810', 'Yuvaraj'),
(580, 'jayg17', 'floor', '0', '97c332ce65b0cb93fc05a51e8b000445ee5f770f2cb49e75248d352a893ec07efe30de3e138e64b7905d1cec0ed94b894c9598f5dc22d789e5f64038bf4a72c2', '8263589765a4e4ffcb2b769.37470655', 'Jay Gosalia'),
(582, 'namrathas17', 'system', '0', '87525c1e6a097db69ed127b81d400299682063544fdf5f2f0f6e4db9ee63737f29d25bfeff67bb2133e586b9e4abf6e965f93675e742ddf8fa091f31caaa88dc', '20050568935a4e5279a941e1.65363677', 'Namratha'),
(583, 'haarikak17', 'system', '0', '8862e8c68a9366acae3d97346e554e66f6cd30b9160bcd803424513a09a21e299e849a356550eee065259f8d5c3cb69d1ba798ab369af3efccfdfd9acc25d002', '10189361185a4e52c1adb566.52987076', 'Haarika Kante'),
(584, 'nirmals17', 'system', '0', 'fd1a7797482c000a1107d68778737915bb3b9b1ee5dd49a701b912b2c734461133e9c5268f88cfd3f05292ed17768fe2fb1b51587474bca05e30f21f862598b8', '13466110415a4e542a9fc973.41002904', 'Nirmal Sheet'),
(585, 'kedarm17', 'system', '0', 'c00f36409a352271ac9948b4591e0ef57e8ebe0e01452d5d7cecfe2736148af9362a4218803556e8b1969b2f422307470df4a7fb67da2d20be3b4174560dc603', '1884980735a4e5459c67d55.41265383', 'Kedar Mahamuni'),
(586, 'souravg17', 'company', 'zzdummy', 'de22811d1ecf18de6502f006a10cd36a21fe1ac72803d6f22e353f0da690986d4e1d3c69a3f587bc856233e30b74ecff317f6cc7008f3d6db1f2cda67afc150d', '15171489115a4e552d359638.52997406', 'Sourav Ghosh'),
(587, 'aiswariyam17', 'company', 'anm', 'ff8560ab8b2760b6d07667cd36620775bd5ec4ab2caff271dca3cc86d227f267f209e07c6c7513a348cc93549c83fac3eedb10e6cda82b4a698336cbbe0863bf', '12277689915a4e556c5989c2.09448690', 'Aiswariya Mohan'),
(589, 'apurvs17', 'placecom', '0', '16d50341ffe1d6db18c0704f4ac13dab8439003a15c09b0d664cf8fbb4b2106e052023229fef60ef84133857932ac320b279f5594b28e0a9369a1b02a89d0e1f', '20899083725a4e55b00f9252.33826936', 'apurv'),
(590, 'praveenra17', 'placecom', '0', 'aa0ceb98602c952f25a679d4b5d86e67ee91ed4a75c2ef1184bd378397da46ed8005792146af274ca52a610df005be49d77dbb1e96a60751cb9a7e0b83b7158f', '21415907195a4e55d2e82170.33981864', 'Praveen'),
(591, 'eshwarg17', 'backloop', '0', 'd36bded848f05f0dd0fc827a6c4b535b09f89a968318c0e266e1c40f1aa8b663ae2462510b9ce27523bf64ef4eef2006fde77563e0c2b48ef0b8c726bb2f7efc', '4415592055a4e5ef7e54753.79667942', 'Eshwar'),
(592, 'nileshr17', 'mobile', '0', 'ab9bf1afdae8f707c3c563f47498c65f5926a47bd377cdcbdd2508791d07648a4bf27fee6fc55b50c596045fecec00379d36df41c2dc4d5e97d1e8969ae0198d', '21204566135a53aea8cbbd93.90837119', 'nilesh rangari'),
(593, 'mukeshk17', 'company', 'gsibd', 'aa898cf87150f4ba2a3b0dd1c07cb891e693a8e9e54c3203621421991821afc60477781a28adf2a63672f425031e488479818d74f19dd263c1b43febe98e062a', '9146460565a53af535ac018.27475727', 'Mukesh Kumar'),
(595, 'ajithk17', 'floor', '0', '52b23b4ae0a4afdc791ae8edc2ea8d23a6a75465c08488daa2adc0a35faf04e8d91dfc354ee5fa93b7fdf1b10e7286029a1a2ab4bd09ba4a6013a84d057592cc', '9938801255a53af91a46866.81309304', 'Ajith K M'),
(596, 'anveshg17', 'company', 'gsstrat', 'd0cc60363601f9dbb38810e4e3b440a1d8e199189787144d7b845f97a01f6549325ee20deafc2ce9e3becb2ea1c41c33cc7df2af3eeea6b64bf27cb7b18986f3', '4789008355a53afd5b23632.79093578', 'Anvesh Gunji'),
(597, 'vigneshm17', 'floor', '0', '115226678ff5909cf680c28ad51ee31384c87eae47eb623b1ce638a81d9d0d8bfbdb530500a5fd126193e179494474bae9e4a0522a613cc864b31c4834f1dbfb', '15703428385a53b0ea70e927.10226029', 'Vignesh Meyyappan'),
(598, 'sangeethad17', 'company', 'tas', 'a92f8a021b9cbbba04601e0112f390afbc89bf8842e1fa696af6c372f4cba58a469d3b46a6a8292b4267f79f2cce0685c43342bc35bcdf71ed6e3cf009ea133c', '18643792115a53b3f5043b59.61778124', 'sangeetha'),
(599, 'sachinm17', 'company', 'gsstrat', 'e47aa219ada5823c4f4543a1da13a01eb150bbe695873795cbec7187afa4560131703b21ab445500444b1203a9709ea27b65eb5c87d0f62254d30d4ee7e52657', '5580514105a53b68d92ba59.05357414', ''),
(600, 'chahatg17', 'company', 'jpm', 'affcee3bd18d31a3acdd692958fd2680239ca8999dd7d175213435298964d0fc40bcd859c5a5c5da6db87e41535e7105f0fbf14b8fe7b87358a5c5556b12bddb', '7831524655a53b7131fa119.57850981', 'Chahat'),
(601, 'srijitm17', 'company', 'amazonpr', 'ffa02a9b6d847a5745ead990539152ff1fbf6257efac3172484870429d38785a4741ff1ea787064469d427d30926a033582517c0d5e21d8c714ac6b8ad816c92', '20496865615a53b76eaaae68.51629065', 'Srijit Mondal'),
(602, 'd1', 'company', 'delloite', '6c608406deddb7d9c77b02139a7cfb4581007dbb346e9273f67f31ce67e4a8ede00c8ff7d11a286a4a0fcf00062d27b94955d864a6fc687410cb1e296a410b98', '20177228395a53b789c171c5.47735606', ''),
(603, 'abhishekd17', 'company', 'auctus', '1e2297e8f7ec622d5ac305d8d479696c802b20d319b94f6f1cd0e5c46f90fa967e710e0b683f898facbf7e443810cb71555e87f732ab3ac69052f933dd8875fe', '4092950545a53b8191843f8.92243564', 'Abhishek Das'),
(604, 'karishmapa17', 'company', 'itc', '111e16f2ea76acb66ad71297e7830209253f6270b4f19783db55d3576a8e4298e5df6e8d2d1d4a55d7e297e711430338b083b727fe57faf0ff4cc8b2df92184f', '10843450185a53b8ff910aa3.94989052', 'Karishma Patil'),
(605, 'kanakk17', 'company', 'avendus', '6e822ead43a69440ec9ae0126317faf03055ccbf17bf7bf8adc402612a46d7fe3435e3bdab7dcb4ded2cafcf310facd3d3632b99a91f220003abdf467ed12e4e', '6560092695a53cd638043b0.39784363', 'Kumar Kanak'),
(606, 'control', 'control', '0', '371e1f05a888d408958e8a52a714c80693a4ed22564a30b225774b520861feab95d1210fea9191a3c754a2b566583833e419f9608a7757a207302d438c8b47dd', '2105452095a5622bfbf0656.92110669', 'Control dummy'),
(607, 'kishorer17', 'company', 'abg', '66d2d8418ffc9d224a72a338c96ce8cbc0f967ff6f64b68bc890bef4ea27f480c2b90c40f36592c3ed0e1322527865a586f2976552a88c1f120d42c3f6066cb6', '13177364305a5640d2d58b34.05618051', 'Kishore Balaji R'),
(608, 'kritika17', 'mobile', '0', '07531a9305232a8acdaacd5e6dd30db25c435cfa49aa5f25db97a203f1d882ad273431b830a8efa873659f44c5703327bc0928f0c4cfbb18047b1b6b80253dd1', '17923905015a564631bd12b0.75637999', 'Kritika'),
(609, 'sahilb17', 'mobile', '0', '5a5adf538f3251038eb8d47ea52232e1dfd534316d37d9ac21ff46a8b17ec17bec99989d5789a8a2fa61c443b7b311ff64e48495594f7d328a30d5e98f3a0a8e', '20161301055a5646627736b0.21909949', 'Sahil Baid'),
(610, 'anantikan17', 'mobile', '0', 'b6df6892407af5a63f7a9d55d733d34c238368e4523578bb0085dd61c08f7e766b44306a2ff5e777a3e79d7a432f599ffb255268a670bc5322d657dd97d50060', '3826507735a5646bb9c8ef1.60654510', 'Anantika Nautiyal'),
(611, 'ramyak17', 'company', 'zzdummy', 'b2f5120564d3c3f8063337e55d770a76a92d251d0444d4b1726428e85578897c91b9e921a997f12e2b5194ffd728a3f4cb13b674a381e5f7fd2a617efd9a8563', '14354137965a564a9d0d6a11.59503861', 'Ramya Kolli'),
(612, 'ashitag17', 'mobile', '0', '9b4155c44eb0a129a6dcc720f9f0f37b5ed5dfd13c744b86bd1854013d5a2468df36f321a7d6eac2584a35cc53a695f842d009df88102903288b0059894ccf70', '19014326675a5b7eee4fc571.44216284', 'Ashita Gugalia'),
(613, 'ishitag17', 'company', 'microidc', '4a8ef287ed2270ff05f70278f9e730d28484899c5cc0f492fcd4ac2b2d6d3545734513aa69e519d5f94f5af19ee0f35322af69fbda7b28cd20c0f3672983273d', '1922813465a5b82927d3593.77464041', 'Ishita Goel'),
(614, 'sudarshani17', 'company', 'wiprocon', '468edbdf3b144aa587c9abeb0560917480f62e9bb9f765869238b846bb56dfff9fd70ea9411c65b7436f1d029355ff2ea486a918fd4408cbb59bfbafdbc46517', '3139475155a5b85178e8e26.01257282', 'Sudarshan Iyer'),
(615, 'd2', 'company', 'anm', '5c12d685889248c4bd8cb61a23556a5942d8684643516cedfabbe0f52b47a540e4fe67768acb42bb8fc988db62b7fdddecd7ca3f54af88dfe5800331f0dd6da4', '15517575355a5b89d784da93.02696987', 'd2'),
(616, 'd3', 'company', '00dummy', '646eb3ea34631dc19ac90fe0d78f8185c7c4bf398344ee856a372e660892418a5ec06c49c198acbafe0f8cd5b51f78dc10f05f860960d51cf7a527ca50824e9d', '14039117275a5b89e195a689.10579159', 'd3'),
(617, 'd4', 'company', 'citiibd', '2247f2e092e2a8ca69ffc8550ac7ca68cc949256ab18f727e71cb36451412fc26411267e0e8700b04429aab6eda153f6d6092208804b005046a0cd446a84bb01', '3525520715a5b89e9a55949.39041147', 'd4'),
(618, 'd5', 'company', 'znri', '8af75f14f40f8f26f99119b6ee6b1543c82988f8e645d87773f17f18f704f056ff1f3e7926810c2ece055623c5db95164dc35509567da4c614d4f5acab2d533c', '18563537905a5b89f4c62028.44187239', 'd5'),
(619, 'd6', 'company', 'zadl', '525d7f8855e4233f0831a181ead3bcf0a98173a69e9ad07323e8b759f271b201c02e18379d54b56815e901d0c108ad2fc5ad8f93761fd3f89a8ad7f898c8859c', '16115513345a5b89fdcc03b3.37857993', 'd6'),
(620, 'd7', 'company', 'accenture', '04438f85c1bfbc567fcfdc4edb8510600680d00330150023b2f043da55945139443fcf8a095662a6b2451ac713d5e483075861be2e1a15fd4de6a6ec08e9e0e4', '16142512885a5b8a163fc815.71905066', 'd7'),
(621, 'controltest', 'control', '0', 'cb30745ac2bac185f4301c5deeb816a5d9629099d976472a4517313b0f0d6d49dc329d84c972215b50ae0bb56c52f064385c26944c3ab05e2bdcdf8e4b2d7374', '8252889995a5b8a45d1fa09.08554667', ''),
(622, 'd8', 'company', 'zzdummy', 'e95169bd76df7aa345126998c2b920d5b7024958f034ae2c54dc855a6deeffe52389841bfa885b9c2f9d567b6416ab4ae3cdf5e6b9953ef3f80833f55d00eb31', '12693302455a5f7877644500.06567021', ''),
(623, 'd9', 'company', 'zzdummy', '42a7fcd982a5c6238ee8d333b357c0cb12c9bfcb90c423c26653ace2ef4635469769f44e6f108271de9bc0f93eb33fe98bad04e7cb913665de89ba57952c4bde', '11959223215a5f787e5678a3.54419617', ''),
(624, 'd10', 'company', 'zzdummy', '9ac96feb34c5b03cf47eba66bfbf9006d7893c254b13c9bf0ccde8133924f797dc36b4a99b9705d6862a377d4a0a4359ae6d453f1b3511c2d734403fc9815eb1', '10191662615a5f7885b30db9.10645951', ''),
(625, 'd11', 'company', 'zzdummy', '895c0eb2d9b32c239a9adc16cd4f5804fe428f9828a07bb2a55f39c58d95e880230fe6554d8ed433315d71c4ba1a5bb30266390a3396c25ea7b3a210e5b3462a', '15283518175a5f788bcd89c6.36486016', ''),
(626, 'd12', 'company', 'zzdummy', '64c7cb14c34c40a3585f0fe21684bd2f8ae69689695f38f18fd62109e9448479007fc61bd6b716ecc0423296eb053514224c60bb8a98e07fb7f63f0cb7429101', '9216015955a5f789327e491.74186569', ''),
(627, 'd13', 'company', 'zzdummy', '4b06ed164a053b8913246ca2dedc91b111c0a852103040fcb1fac620bca454a476bef9853574d056e94cec34ecc8cc003c2e238ac4ae4fedcfd2172866d09b24', '13404057985a5f7898a11439.33821414', ''),
(628, 'd14', 'company', 'zzdummy', 'd4dca9710713f64e6797c13af9a62bc0ff10557e16dd73b8d5d90e7cd2c69b34fef50e4a8e9ef8bab7929124e979d88e7d40c1ece8610374631978fbbc95253d', '11976605065a5f78a20288e8.17274604', ''),
(629, 'd15', 'company', 'zzdummy', 'e3464f486649524e2ab2dd585dfb591733cb89feead952ec21f60d740a71f9123a0fe90ccd8eab2f68638aa41fdcec3f935c38210803ae928f341c2285e0fa2d', '10105470295a5f78a80284b4.85817041', ''),
(630, 'amitsa17', 'company', 'bcg', 'e3fa103e05b8c0f521fc2505a1a69216d58164ded7bf8011369536f1d43e439f91c6ac80cae9e60d47d00870c14b219ea251a815f94350ed631376dbb8697bc0', '6493859465a5fa090dccf17.46628353', 'Amit Kumar'),
(631, 'ramyar17', 'company', 'jjcon', 'c52ae9baaaa409fd3c3c280d5682ef5b9a438513d517a6f81fd3a46f17f4ac49d47e499262ab88fd1d62c02337d255486f59e8c72486d71e803044ab88761d60', '6120316605a622300b910e4.41328296', 'Ramya R'),
(632, 'arpita17', 'company', 'microidc', 'ec2c3d110bb3f71ddb5888ef57dd6e377096ebd5187b0efa007adf45117e8cee6300abc06d66b5eab6115d79ce7a7f7084156d8af01fff26c483074e01831e33', '15056601935a6223337bcfe8.35696928', 'Arpit Agrawal'),
(633, 'nitinp17', 'company', 'temasek', '8d149f40c0758f4f5eae42488e6a5849d0fce966d493adcb70538a0bec48f7f729b04aac12692c8012cfa95623194aa1c31b1ce71f5412f6d7308b1981a5c391', '16924758465a62274b3f2eb7.43860537', 'nitin'),
(634, 'sparshk17', 'control', '0', '9afc15901dc975679d3d066a0865546c49f108e1e05f035122d589b3d354ffa37812ba845ec99f3a0faf308f4f072d7aede90dfd2d2a33ce7a480b8cf1a03be1', '3165531405a6a165f25bbf5.27347562', 'Sparsh Khandelwal'),
(636, 'admin_2018', 'admin', '0', 'aa7e3a291e454b4beb6de74dfc4cec2c27d9a5cfd458efd0457f805ff0dab3676bafe079b3f20b7247e8893fdffb202feacf16adbd1cf7789ac8fb4656f581c5', '5375352055a6dbc0de02aa1.95645693', ''),
(637, 'admin_ct', 'admin', '0', '00fc5b1ea0e77dc7947ef7dec1b29d0a82e62845c13041732379de09662a52309f07416c27a804e77c23897be780be5e7468df7b3ec453d1c06107bc47e36b9f', '6411444085a6dbc94e4e2b1.28150250', 'Admin_ct'),
(638, 'bbalegere', 'control', '0', '5cc49f51dbc4f6fd7b86c82b5d8589319166e8727243328f195522f6c346026f32a771e6e601afdf58e95dd6d38162371d08968beb0a887ab392001617e2bd59', '13639895165a6e14d64180f2.53026244', 'Bharat'),
(639, 'smritinjoyd17', 'company', 'atk', '1ad4f1c901b1dbfca5e38b27f42a1df01e20a127d04dbbbcc93d549540fd26debe53443cf967e3d377fd7093c45e640f4a1a9c1234debe63c2957309e10ab3a9', '20885566325a7161a9dad693.25429028', 'Smritinjoy'),
(640, 'jeevithas17', 'company', 'mckinsey', '68d99d024cc89a77932bee5713db424092d00405eb8c4d08acdbe01ca9279ea6e47f2308fa46d79480a5bcaed4b50d6e2946f03af3d3a9ed3b336fa66489d670', '17535027865a71e5fa7319d5.80498522', 'Jeevitha S'),
(641, 'swapnilk17', 'system', '0', '8090136052dd9fcc851b2fae075abbd1f294fe6276ec70798a083e3fc8f7a00c8959b73ae60b10c6472e1adf01161b770b2828cbc5416368322226427fd65b83', '5538414915a7207cd84bf03.96755313', 'swapnil'),
(642, 'sparshk172', 'company', '00dummy', '5a7276ae69a9b6307dd436557b3d7a24bf75cbf49894d9b3991797d6cd44fdbf56baabca3e52b440d02223b1bba4c6b73e46a7d40f54accbd890b6f6997d0fc4', '9154211655a79e2bc3ad069.61399584', 'Sparsh');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backuplog`
--
ALTER TABLE `backuplog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companylist`
--
ALTER TABLE `companylist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `entry`
--
ALTER TABLE `entry`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `gridnoblanks`
--
ALTER TABLE `gridnoblanks`
  ADD PRIMARY KEY (`id`,`day`,`slot`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `raw_preferences`
--
ALTER TABLE `raw_preferences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studentdata`
--
ALTER TABLE `studentdata`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `studentlist`
--
ALTER TABLE `studentlist`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `userlog`
--
ALTER TABLE `userlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backuplog`
--
ALTER TABLE `backuplog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `companylist`
--
ALTER TABLE `companylist`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT for table `raw_preferences`
--
ALTER TABLE `raw_preferences`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4846;

--
-- AUTO_INCREMENT for table `userlog`
--
ALTER TABLE `userlog`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=643;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
