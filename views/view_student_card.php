<?php
  require $views_path.'view_card_decisions.php';
 ?>

<div class="card">
  <div class="card-details status-<?php echo $student_max_status; ?>">
      <a target='_blank' href='<?php echo $website_uri.$path_elements[0].'/sst/'.$student['place_id']; ?>'>
        <div class="student_name">
          <?php echo ucwords(strtolower($student['student_name'])); ?>
        </div>
      </a>
      <div class="notif-panel-count">
       
      </div>

    <div class="card-details-left">
      <a target='_blank' href='<?php echo $website_uri.$path_elements[0].'/sst/'.$student['place_id']; ?>'>
        <div class="place_id"><?php echo $student['place_id']; ?></div>
      </a>
      <?php 
       
      if($company_list[$student['company']]['type'] == 'g') {?>
      <div class="card_gd_round"><?php echo 'R'.$student['gd_round'].'-P'.$student['gd_panel']; ?></div>
      <?php } ?>
      <div class="time_due"><?php echo $time_to_display; ?></div>
    </div>

    <div class="card-details-right">
      <div class="company"><span class="company_display">
        <?php //echo $display_company; ?>
        <?php
          if(isset($display_company)) { echo $display_company; }
          elseif($temp_company_name == $common_pool['name']) { echo $common_pool['value']; }
          elseif(isset($company_list[$temp_company_name])) { echo $company_list[$temp_company_name]['company_name']; }
          else { echo $common_pool['value']; }
        ?>
      </span></div>
      <div class="status_level"><?php echo $status_levels[$status_levels_codes[$student_max_status]]["value"]; ?></div>
    </div>
  </div>
  <div class="card-button status-<?php echo $student_max_status; ?>">
  <?php
  // This line for placecom
  if($path_elements[0] == 'placecom') {
    $student_max_status = $status_levels['sent']['code'];
  }
  $student_next_status = $status_levels[$status_levels[$status_levels_codes[$student_max_status]]['button_name']]['code'];
    if((in_array($student_next_status, $communicate_permissions[$path_elements[0]]))) {

      if($path_elements[1] == 'cstv' || $path_elements[1] == 'sst' || $path_elements[1] == 'ncw' || $path_elements[1] == 'ita') { }
      else {
      $temp_button = array(
                              'type'  => "submit",
                              'value' => $status_levels[$status_levels_codes[$student_max_status]]["button_value"],
                              'name'  => $status_levels[$status_levels_codes[$student_max_status]]["button_name"]
                            );
  ?>

    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo $student['place_id']; ?>">
          <input type='hidden' name='place_id' value="<?php echo $student['place_id']; ?>">
          <input type='hidden' name='id' value="<?php echo $student['id']; ?>">
          <input type='hidden' name='company' value="<?php echo $student['company']; ?>">
          <input type='hidden' name='status' value="<?php echo $student_next_status; ?>">
          <?php if($student_max_status == $status_levels['sendaway']['code']) { ?>
            <input type='hidden' name='next_company' value="<?php echo $to_company; ?>">
          <?php } ?>
          <?php if($path_elements[0] != 'control') {
            $confirm = "onclick=\"return confirm('Are you sure?')\"";
          } else { $confirm = ''; } ?>
          <?php /*<input class='cst_button' type="<?php echo $temp_button['type']; ?>" value="<?php echo strtoupper($temp_button['value']); ?>" name="<?php echo $temp_button['name']; ?>" <?php echo $confirm; ?>> */ ?>
          <input class='cst_button' type="<?php echo $temp_button['type']; ?>" value="<?php echo strtoupper($temp_button['value']); ?>" name="submitStatusChange" <?php echo $confirm; ?>>
    </form>
  <?php } }?>
    </div>
</div>
