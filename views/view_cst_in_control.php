<div class='unscheduled'>
    <?php if (isset($student_list_cst_control[0])) { ?>
        <div class='ul_list_index control_ui_box'>
            <h3>
                <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8">
                    <input type="text" value="<?php echo $unscheduled_count; ?> Unscheduled" class='transparent_background'>
                    <input type="submit" value="Sort" name="sort">
                </form>
            </h3>
            <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" class='border_bottom'>
                <input type="text" size="1" value="" name="icon" class='transparent_background' readonly>
                <input type="text" size="3" value="ID" name="place_id" class='transparent_background' readonly>
                <input type="text" size="1" value="D" name="dream" class='transparent_background' readonly>
                <input type="text" size="15" value="Name" name="student_name" class='transparent_background' readonly>
                <input type="text" size="5" value="Pref/Crit" name="preference" class='transparent_background' readonly>
                <input type="text" size="5" value="Hotlist" name="hotlist" class='transparent_background' readonly>
                <input class='transparent_background' value="Later" type="text" size="5" name='later' readonly>
                <input class='transparent_background' value="Action" type="text" size="10" name='submit' readonly>
            </form>

            <?php
            $preflist = array();
            $prefcritlist = array();
            $critlist = array();

            foreach ($student_list_cst_control[0] as $idle_key => $student) {
                $combined_id = $student['id'];
                $stu_id = $student['place_id'];

                if (isset($placed_student_list[$student['place_id']])) {
                    $preflist[$combined_id] = 999;
                } else {

                    $initial_preference = $student['preference'];
                    $interviews = R::getCell("select count(*) from entry where place_id ='$stu_id' and stat1 ='1' and preference < '$initial_preference' and company in (select company from companylist where slot = '$slot' and day = '$day')");
                    $stopped_companies = R::getCell("select count(*) from entry where place_id ='$stu_id' and stat1 ='0' and preference < '$initial_preference' and company in (select company from companylist where slot = '$slot' and day = '$day' and company_status in ('1', '2', '3'))");

                    $interviews = $interviews + $stopped_companies;

                    $preflist[$combined_id] = $initial_preference - $interviews;
                }

                if (count($criticalityList) > 0 && array_key_exists($student['place_id'], $criticalityList)) {
                    $criticality = $criticalityList[$student['place_id']];
                    if ($criticality <= 0) {
                        $criticality = -1;
                    }

                } else {
                    $criticality = -1;
                }
                $critlist[$combined_id] = $criticality;
                $prefcritlist[$combined_id] = array('pref' => $preflist[$combined_id], 'crit' => $criticality);

            }
            if ($sortcstview > 0) {
                array_multisort($preflist, SORT_ASC, $critlist, SORT_DESC, $prefcritlist);
            }

            ?>

            <?php foreach ($prefcritlist as $combkey => $prefcrit):
                $student = $student_list[$combkey];
                $disabled = '';


                if (isset($base_student_list[$student['place_id']])) {
                    $current_location = $base_student_list[$student['place_id']]['current_location'];
                } else {
                    $current_location = $common_pool['value'];
                }

                $student_max_status = max($student['status_levels']);

                if ($current_location == $common_pool['name']) {
                    $current_location_name = $common_pool['value'];
                } elseif (isset($company_list[$current_location])) {
                    $current_location_name = $company_list[$current_location]['company_name'];
                } else {
                    $current_location_name = $common_pool['value'];
                }

                if (isset($placed_student_list[$student['place_id']])) {
                    $student_max_status = $status_levels['placed']['code'];
                    $disabled = 'disabled';
                    $current_location_name = $company_list[$placed_student_list[$student['place_id']]['company']]['company_name'];
                    $criticality = $student['id'];
                }

                if (count($criticalityList) > 0 && array_key_exists($student['place_id'], $criticalityList)) {
                    $criticality = $criticalityList[$student['place_id']];
                } else {
                    $criticality = -1;
                }

                $temp_button_values = array(
                    'type' => "submit",
                    'value' => $status_levels[$status_levels_codes[$student_max_status]]["button_value"],
                    'name' => $status_levels[$status_levels_codes[$student_max_status]]["button_name"]
                );
                ?>

                <form id="scheduleForm" action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8"
                      name="<?php echo $student['place_id']; ?>"
                      class='border_bottom status-<?php echo $student_max_status;
                      if (in_array($student['place_id'], $noOfferList))
                          echo " no_offers"; ?>'>
                    <input type="hidden" value="<?php echo $student['id']; ?>" name="id" readonly>
                    <input type="hidden" value="<?php echo $student['company']; ?>" name="company" readonly>

                    <input type="hidden"
                           value="<?php echo $status_levels[$status_levels[$status_levels_codes[$student_max_status]]['button_name']]['code']; ?>"
                           name="status">

                    <a target='_blank'
                       href='<?php echo $website_uri . $path_elements[0] . '/pref#' . $student['place_id'] ?>'>
                        <i class="material-icons" style="font-size:14px">info</i>
                    </a>

                    <a target='_blank'
                       href='<?php echo $website_uri . $path_elements[0] . '/sst/' . $student['place_id']; ?>'>
                        <input type="text" size="3" value="<?php echo $student['place_id']; ?>" name="place_id"
                               class='transparent_background' readonly>
                        <input type="text" size="1" value="<?php if ($student_data_dream[$student['place_id']] == '1') echo 'D'; ?>" name="dream"
                               class='transparent_background' readonly>
                        <input type="text" size="15"
                               value="<?php echo ucwords(strtolower($student['student_name'])); ?>" name="student_name"
                               class='transparent_background'
                               readonly <?php if ($student['student_gender'] == 'F' || $student['student_gender'] == 'Female') echo 'STYLE="background-color: pink"'; ?>>
                    </a>
                    <input type="text" size="5" value="<?php echo $preflist[$combkey] . '/' . $criticality; ?>"
                           name="preference" class='transparent_background' readonly>
                    <label class="switch">
                        <input name="hotlist" onchange="document.getElementById('submitHotlist-<?php echo $student['place_id']; ?>').click();" value="hotlist" type="checkbox" <?php if ($student['hotlist'] == '1') echo 'checked'; ?>>
                        <span class="slider round"></span>
                    </label>
                    <input type="submit" id="submitHotlist-<?php echo $student['place_id']; ?>" name="submitHotlist" value="<?php echo $student['hotlist']; ?>" hidden>
                    <input type="text" size="5" value="<?php if ($student['later'] == '1') echo round(($_SERVER['REQUEST_TIME']-$student['latertime'])/60); ?>"
                           name="later" class='transparent_background' readonly>
                    <?php

                    $stat_value = getStudentStatus($other_shortlists[$student['place_id']]);
                    if (!array_key_exists('company', $stat_value)) {

                        ?>
                        <input type='<?php echo $temp_button_values['type']; ?>'
                               value='<?php echo $temp_button_values['value']; ?>'
                               name='submitStatusChange' <?php echo $disabled; ?>>
                        <?php
                    } else {
                        ?>
                        <input type='<?php echo $temp_button_values['type']; ?>'
                               value='<?php echo $temp_button_values['value']; ?>'
                               onclick='return confirm("The candidate is already scheduled for another company. Do you want to continue anyways?");'
                               name='submitStatusChange' <?php echo $disabled; ?>>

                        <div type="text" size="5" name="current_student_status" class='student_status-1'>
                            <?php echo $stat_value['company']; ?>
                            <?php echo "    -->      "; ?>
                            <?php echo

                            $status_levels[$status_levels_codes[$stat_value['status']]]['value'];
                            ?>
                        </div>

                    <?php } ?>


                </form>
            <?php endforeach; ?>

        </div>
    <?php } ?>
    <?php if ($num_path_elements > 2 && $company_list[$path_elements[2]]['freeflow'] == '1') { ?>
        <div class='ul_list_index control_ui_box'>
            <h3>Not shortlisted</h3>
            <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8"
                  class='border_bottom'>
                <input type="text" size="3" value="ID" name="place_id" class='transparent_background' readonly>
                <input type="text" size="1" value="D" name="dream" class='transparent_background' readonly>
                <input type="text" size="15" value="Name" name="student_name" class='transparent_background' readonly>
                <input type="text" size="9" value="Location" name="current_location_name" class='transparent_background' readonly>
            </form>

            <?php foreach ($non_placed_list as $non_key => $non_placed_student):
                $non_placed_student_id = "I" . $non_placed_student['id'] . $path_elements[2];
                if (!array_key_exists($non_placed_student_id, $student_list_cst_control[0]) && !array_key_exists($non_placed_student_id, $student_list_cst_control[1])) {
                    $current_location = $non_placed_student['current_location'];
                    if ($current_location == 'cp') {
                        $current_location_name = $common_pool['value'];
                    } else {
                        $current_location_name = $company_list[$current_location]['company_name'];
                    }
                    ?>
                    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8"
                          name="<?php echo $non_placed_student['id']; ?>" class='border_bottom status-0'>
                        <!-- <input type="hidden" value="<?php echo $non_placed_student_id; ?>" name="id" readonly> -->
                        <input type="hidden" value="<?php echo $path_elements[2]; ?>" name="company" readonly>
                        <a target='_blank'
                           href='<?php echo $website_uri . $path_elements[0] . '/sst/' . $non_placed_student['id']; ?>'>
                            <input type="text" size="3" value="<?php echo $non_placed_student['id']; ?>" name="place_id"
                                   class='transparent_background' readonly>
                            <input type="text" size="1" value="<?php if ($student_data_dream[$non_placed_student['id']] == '1') echo 'D'; ?>" name="dream"
                                   class='transparent_background' readonly>
                            <input type="text" size="15"
                                   value="<?php echo ucwords(strtolower($non_placed_student['student_name'])); ?>"
                                   name="student_name" class='transparent_background'
                                   readonly <?php if ($non_placed_student['student_gender'] == 'F' || $non_placed_student['student_gender'] == 'Female') echo 'STYLE="background-color: pink"'; ?>>

                        </a>
                        <input type="text" size="9" value="<?php echo $current_location_name; ?>"
                               name="current_location_name" class='transparent_background' readonly>
                        <input type='submit' value='Shortlist' name='shortlist'>
                    </form>
                    <?php
                }
            endforeach; ?>
        </div>
    <?php } ?>
</div>

<div class='scheduled'>
    <?php
    if (isset($student_list_cst_control[1])) {
        ?>

        <div class='ul_list_index control_ui_box'>
            <h3>Scheduled</h3>
            <div class='form_row border_bottom_white' style="padding-left:5px;">
                <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8"
                      class='control_list_item'>
                    <input type="text" size="3" value="ID" name="place_id" class='transparent_background' readonly>
                    <input type="text" size="1" value="D" name="dream" class="transparent_background" readonly>
                    <input type="text" size="17" value="Name" name="student_name" class='transparent_background'
                           readonly>
                    <input type="text" size="1" value="G" name="student_gender" class='transparent_background'
                           readonly>
                    <span class='control_status_text'>Current Status</span>
                </form>
                <span class='spacer-100'></span>
                <span class='control_companies'>From</span>
                <span class='control_companies'>To</span>
                <span class='control_companies'>Placed</span>
                <span class='control_times'>Sch</span>
                <span class='control_times'>In</span>
                <span class='control_times'>Out</span>
                <span class='control_times'>Later</span>
            </div>

            <?php foreach ($student_list_cst_control[1] as $non_idle_key => $student):
                $disabled = '';
                $current_location = $base_student_list[$student['place_id']]['current_location'];
                $student_max_status = max($student['status_levels']);
                $placed_company_name = '-';

                if ($current_location == $common_pool['name']) {
                    $current_location_name = $common_pool['value'];
                } else {
                    $current_location_name = $company_list[$current_location]['company_name'];
                }

                $previous_company = $student['previous_company'];
                if ($previous_company == $common_pool['name'] || $previous_company == '0') {
                    $previous_company_name = $common_pool['value'];
                } else {
                    if (isset($company_list[$previous_company])) {
                        $previous_company_name = $company_list[$previous_company]['company_name'];
                    } else {
                        $previous_company_name = "Unknown";
                    }
                }

                if ($student_max_status == $status_levels['scheduled']['code'] || $student_max_status == $status_levels['intransit']['code']) {
                    $previous_company_name = $current_location_name;
                }

                require $models_path . 'model_next_company.php';
                if ($student_max_status == $status_levels['sent']['code']) {
                    $next_company = $student['next_company'];
                }
                if ($next_company == $common_pool['name'] || $next_company == '0') {
                    $next_company_name = $common_pool['value'];
                } else {
                    if (isset($company_list[$next_company])) {
                        $next_company_name = $company_list[$next_company]['company_name'];
                    } else {
                        $next_company_name = "Unknown";
                    }
                }

                if ($student_max_status == $status_levels['scheduled']['code'] || $student_max_status == $status_levels['intransit']['code']) {
                    $next_company_name = $company_list[$student['company']]['company_name'];
                }

                if (isset($placed_student_list[$student['place_id']])) {
                    $student_max_status = $status_levels['placed']['code'];
                    $disabled = 'disabled';
                    $placed_company_name = $company_list[$placed_student_list[$student['place_id']]['company']]['company_name'];
                }

                $temp_button_values = array(
                    'type' => "submit",
                    'value' => $status_levels[$status_levels_codes[$student_max_status]]["button_value"],
                    'name' => $status_levels[$status_levels_codes[$student_max_status]]["button_name"]
                );
                //echo $company_list[$student['company']];
                $this_company_name = $company_list[$student['company']]['company_name'];

                for ($i = 0; $i < $max_status; $i++) {
                    if ($student['status_timestamps'][$i] != 0) {
                        $delta = time() - $student['status_timestamps'][$i];
                        $delta = round((time() - $student['status_timestamps'][$i]) / 60, 0);
                        if ($delta > 1440) {
                            $delta = round($delta / 1440, 0) . "d";
                        } elseif ($delta > 60) {
                            $delta = round($delta / 60, 1) . "h";
                        } else {
                            $delta = $delta . "m";
                        }
                        $status_time[$i] = $delta;
                    } else {
                        $status_time[$i] = "-";
                    }
                }

                ?>
                <div class='form_row border_bottom_white status-<?php echo $student_max_status; ?> control_in_row'>
                    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8"
                          name="<?php echo $student['place_id']; ?>" class='control_list_item'>
                        <input type="hidden" value="<?php echo $student['id']; ?>" name="id" readonly>
                        <input type="hidden" value="<?php echo $student['company']; ?>" name="company" readonly>
                        <input type="hidden" value="<?php echo $next_company; ?>" name="next_company" readonly>
                        <input type="hidden"
                               value="<?php echo $status_levels[$status_levels[$status_levels_codes[$student_max_status]]['button_name']]['code']; ?>"
                               name="status">
                        <a target='_blank' class="control_in_row_name_link"
                           href='<?php echo $website_uri . $path_elements[0] . '/sst/' . $student['place_id']; ?>'>
                            <input type="text" size="3" value="<?php echo $student['place_id']; ?>" name="place_id"
                                   class='transparent_background' readonly>
                            <input type="text" size="1" value="<?php if ($student_data_dream[$student['place_id']] == '1') echo 'D'; ?>" name="dream"
                                   class='transparent_background' readonly>
                            <input type="text" size="17"
                                   value="<?php echo ucwords(strtolower($student['student_name'])); ?>"
                                   name="student_name" class='transparent_background' readonly>
                        </a>
                        <input type="text" size="1" value="<?php echo $student['student_gender'][0]; ?>"
                               name="student_gender" class='transparent_background' readonly>
                        <span class='control_status_text'><?php echo $student_max_status . " " . $status_levels[$status_levels_codes[$student_max_status]]['value']; ?></span>
                        <input type='<?php echo $temp_button_values['type']; ?>'
                               value='<?php echo $temp_button_values['value']; ?>' name='submitStatusChange'
                               class='control_cst_button' <?php echo $disabled; ?>>

                        <span class='control_companies'><?php echo $previous_company_name; ?></span>
                        <span class='control_companies'
                              style="background-color:#ffffff"><?php echo $next_company_name; ?></span>
                        <span class='control_companies'><?php echo $placed_company_name; ?></span>
                        <span class='control_times'><?php echo $status_time[0]; ?></span>
                        <span class='control_times'><?php echo $status_time[5]; ?></span>
                        <span class='control_times'><?php echo $status_time[6]; ?></span>
                        <span class='control_times'><?php if ($student['later'] == '1') echo round(($_SERVER['REQUEST_TIME']-$student['latertime'])/60); else echo '-' ?></span>

                        <select name="cst_status">
                            <option value="blank_status" selected disabled>Status</option>
                            <?php foreach ($status_levels as $status_level) { ?>
                                <option value= <?php echo $status_level['code']; ?>><?php echo $status_level['code'] . " - " . $status_level['value']; ?></option>
                            <?php } ?>
                        </select>

                        <input type="submit" value="Change" name="change" class="scw-button"
                               style="font-size: 12px; height: 20px;">

                        <?php if ($student['later'] == 0): ?>
                        <input type="text" value="0"
                               name="later_flag" class='transparent_background' hidden>

                        <input type="submit" value="Later" name="later" class="scw-button"
                               style="font-size: 12px; height: 20px;">
                        <?php endif; ?>
                    </form>


                </div>
            <?php endforeach; ?>
        </div>

    <?php } ?>
</div>

