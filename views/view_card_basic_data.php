<?php
/**
 * Created by PhpStorm.
 * User: srividyag
 * Date: 11/01/18
 * Time: 12:46 AM
 */

// Currently what is the max-status and max-time of the student in this shortlist
$student_max_status = max($student['status_levels']);
$student_max_time = max($student['status_timestamps']);

// Currently what is the company whose name should be displayed on the card
$temp_company_name = $student['company'];

// If candidate is already placed - then move status/time to 'placed' and change company to the placed one
if (isset($placed_student_list[$student['place_id']])) {
    $student_max_status = $status_levels['placed']['code'];
    $student_max_time = $placed_student_list[$student['place_id']]['status_timestamps'][$status_levels['placed']['code'] - 1];
    $temp_company_name = $placed_student_list[$student['place_id']]['company'];
    $display_company = $company_list[$temp_company_name]['company_name'];
}

if (isset($company_list[$temp_company_name])) {
    $display_company = $company_list[$temp_company_name]['company_name'];
} else {
    $display_company = "N/A";
}

// Display time for given status
if ($student_max_status != $status_levels['idle']['code']) {
    $time_to_display = round((time() - $student_max_time) / 60, 0);
    if ($time_to_display > 1440) {
        $time_to_display = round($time_to_display / 1440, 0) . " d";
    } elseif ($time_to_display > 60) {
        $time_to_display = round($time_to_display / 60, 1) . " h";
    } else {
        $time_to_display = $time_to_display . " m";
    }
} else {
    $time_to_display = "NA";
}

// Unset display company for continuity in displaying multiple card
// if(isset($display_company)) { unset($display_company); }

// Panel Counter for NCW/ITA cards
if (($num_path_elements == 2 && ($path_elements[1] == 'ncw' || $path_elements[1] == 'ita' || $path_elements[1] == 'pref')) || ($path_elements[1] == 'cst')) {
    if (isset($panels_running[$student['company']])) {
        $card_panel_counts = $panels_running[$student['company']]['panels_running'] . '/' . $company_list[$student['company']]['panels'];
    } else {
        $card_panel_counts = '0/' . $company_list[$student['company']]['panels'];
    }
} else {
    $card_panel_counts = '';
}

//Compute Queues Running
if (isset($queues_running[$student['company']]['queues_running']))
    $queues_running_grid_disp = $queues_running[$student['company']]['queues_running'];
else
    $queues_running_grid_disp = '0';

?>