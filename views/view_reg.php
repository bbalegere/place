<?php
require $models_path.'model_user_list.php';
?>

<div class = 'reg_left_part'>
  <div class="ul_list_index">
    <?php
      if(isset($form_error)) {
        switch ($form_error) {
          case 0:
            if(isset($user_id)) { echo "User ".$user['username']." Stored"; }
            break;

          case 1:
            echo "Incomplete Information";
            break;

          case 2:
            echo "User already exists";
            break;

        }
      }
      ?>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="regform">
      <input type="text" placeholder="Name" name="name" class="reg-elements name">
        <input type="text" placeholder="Username" name="username" class="reg-elements username">
      <input type="password" placeholder="Password" name="password" class="reg-elements password">
      <select name="role" class = "reg-elements role-select">
        <option value="no-role" selected disabled>Select Role</option>
        <?php
          foreach ($profiles as $key => $profile) { ?>
            <option value="<?php echo $profile; ?>"><?php echo $profile; ?></option>
        <?php } ?>
      </select>
      <select name="company" class = "reg-elements company-select">
        <option value="no-company" selected disabled>Select Company</option>
        <?php
          foreach ($company_list as $key => $company) { ?>
            <option value="<?php echo $company['company']; ?>"><?php echo $company['company_name']; ?></option>
        <?php } ?>
      </select>
      <input type="submit" value="Register" name="register" class="reg-elements reg-button">
    </form>
  </div>
  <?php foreach ($profiles as $profile_key => $profile_for_loop): ?>
  <div class="ul_list_index">
    <h3><?php echo $profile_for_loop; ?></h3>
    <?php
      if(isset($form_error)) {
        switch ($form_error) {
          case 4:
            echo "User Deleted";
            break;

          case 5:
            echo "User Modified";
            break;
        }
      }
      ?>
    <?php foreach ($base_user_list as $key => $existing_user):
      if($existing_user['role'] == $profile_for_loop) {
      ?>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo $existing_user['username']; ?>" class="reg-mod-form">
      <input type="text" value="<?php echo $existing_user['displayname']; ?>" name="name" class="reg-mod-elements name" size=15 readonly>
      <input type="text" value="<?php echo $existing_user['username']; ?>" name="username" class="reg-mod-elements username" size=15 readonly>
      <select name="role" class = "reg-mod-elements role-select">
        <option value="no-role" selected disabled>Select Role</option>
        <?php
          foreach ($profiles as $key => $profile) { ?>
            <option value="<?php echo $profile; ?>" <?php if($existing_user['role']==$profile) { echo ' selected ';}?> ><?php echo $profile; ?></option>
        <?php } ?>
      </select>
      <select name="company" class = "reg-mod-elements company-select">
        <option value="no-company" selected disabled>Select Company</option>
        <?php
          foreach ($company_list as $key => $company) {
            if($company['company_status'] != 3) {?>
            <option value="<?php echo $company['company']; ?>" <?php if($existing_user['company']==$company['company']) { echo ' selected ';}?>><?php echo $company['company_name']; ?></option>
        <?php }} ?>
      </select>
      <input type="submit" value="Modify" name="modify" class="reg-mod-elements reg-mod-button">
      <input type="submit" value="Delete" name="delete" class="reg-mod-elements reg-mod-button">
    </form>
    <?php }
  endforeach; ?>
  </div>

  <?php endforeach; ?>

</div>
<div class='reg_right_part'>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="regform">
    <input type="submit" value="Reset Users" name="reset_users" class="reset-button">
    </form>
<?php
  foreach ($base_user_list as $key1 => $reg_base_user) {
    if($reg_base_user['role'] == 'company') {
      $reg_company_list[$reg_base_user['company']][] = $reg_base_user['username'];
    }
  }

  for($i = 0; $i < 4; $i++) {

  $reg_company_text = 'light_text';
  switch ($i) {
    case 0:
      $reg_company_background = 'on';
      $reg_company_box_title = 'Ongoing Companies';
      break;

    case 1:
      $reg_company_background = 'paused';
      $reg_company_text = 'dark_text';
      $reg_company_box_title = 'Paused Companies';
      break;

    case 2:
      $reg_company_background = 'stopped';
      $reg_company_box_title = 'Stopped Companies';
      break;

    case 3:
      $reg_company_background = 'exited';
      $reg_company_box_title = 'Exited Companies';
      break;

    default:
      $reg_company_background = '';
      $reg_company_text = '';
      break;
  } ?>
  <div class="ul_list_index <?php echo $reg_company_text.' '.$reg_company_background; ?>">
    <div class='ul_list_index_header'><?php echo $reg_company_box_title; ?></div>
    <?php foreach ($company_list as $key2 => $reg_company):
      if ($reg_company['company_status'] == $i) {
        if(isset($reg_company_list[$reg_company['company']])) {
          $num_of_trackers = sizeof($reg_company_list[$reg_company['company']]);
          $num_trackers_class = 'reg_num_trackers_exist';
          $company_trackers = '';
          if($reg_company['company'] != '00dummy') {
            foreach ($reg_company_list[$reg_company['company']] as $key3 => $company_tracker) {
              $company_trackers .= $company_tracker.' ';
            }
          }
        } else {
          $num_of_trackers = '';
          $company_trackers = '';
          $num_trackers_class = 'reg_num_trackers_zero';
        }
        ?>

      <div class='company_list_row <?php echo $reg_company_text.' '.$reg_company_background; ?>'>
        <span class='reg_company_name'><?php echo $reg_company['company_name']; ?></span>
        <span class='<?php echo $num_trackers_class; ?>'><?php echo $num_of_trackers; ?></span>
        <span class='reg_company_trackers'><?php echo $company_trackers; ?></span>

      </div>
    <?php }
        endforeach; ?>
  </div>
<?php } ?>
</div>
