<?php
if(isset($student_list_cst)) {
    if (isset($_POST['submit']) || array_key_exists(2,$path_elements)) {
        foreach ($student_list_cst as $key => $sorting_student) {

            $student_max_status = max($sorting_student['status_levels']);
            if (array_key_exists($sorting_student['place_id'],$placed_student_list)) {
                $student_max_status = $status_levels['placed']['code'];
                $temp_company_name = $placed_student_list[$sorting_student['place_id']]['company'];
            }
            $sorting_student['max_status'] = $student_max_status;
            if($student_max_status == 0) { $sorting_student['max_status_time'] = 0; }
            else { $sorting_student['max_status_time'] = $sorting_student['status_timestamps'][$student_max_status-1]; }

            $new_student_list_cst[$student_max_status][] = $sorting_student;
        }
        if(isset($new_student_list_cst)) {
            ksort($new_student_list_cst);

            /*Create a sequence array for modifying display for different users*/

            $sequence = array();
            $index=0;
            for($i = $min_status; $i <= $max_status; $i++)
            {
                if($i==0 && $path_elements[0] == 'floor'){continue;}

                $sequence[$index] = $i;
                $index++;
            }
            if($path_elements[0] == 'floor')
            {
                $sequence[$index]=0;
            }

            for ($i = 0; $i <= $index; $i++) {
                if(isset($path_elements[0]) && isset($sequence[$i]) &&  in_array($sequence[$i],$card_visibility_permissions[$path_elements[0]])) {
                    $card_count_display = 0;
                    if(array_key_exists($sequence[$i], $new_student_list_cst)) {
                        $card_count_display = sizeof($new_student_list_cst[$sequence[$i]]);
                    }
                    if ($card_count_display != 0 || $path_elements[0] == 'control') {
                        //$card_count_display = '';
                        ?>
                        <div class='card-list card-list-<?php echo $sequence[$i]." control-layout"; if($path_elements[0] == 'company') { echo " company-layout"; }?>'>
                            <div class='card-list-header'><?php echo $status_levels[$status_levels_codes[$sequence[$i]]]['text'].' '.$card_count_display; ?></div>
                            <?php
                            if(array_key_exists($sequence[$i], $new_student_list_cst)) {
                                $student_list_cst_temp = $new_student_list_cst[$sequence[$i]];
                                if($sequence[$i] == 0) {
                                    foreach ($student_list_cst_temp as $old_key => $student_list_cst_temp_student) {
                                        $student_list_cst_temp[$student_list_cst_temp_student['place_id']] = $student_list_cst_temp_student;
                                        unset($student_list_cst_temp[$old_key]);
                                    }
                                    ksort($student_list_cst_temp);
                                }
                                if(in_array($sequence[$i],$card_visibility_permissions[$path_elements[0]])) {

                                    if($sequence[$i] != 0) {
                                        uasort($student_list_cst_temp, function($a, $b) {
                                            return $a['max_status_time'] <=> $b['max_status_time'];
                                        });
                                    }

                                    if($sequence[$i] == $status_levels['placed']['code'] && $path_elements[0] == 'placecom') {
                                        $student_list_cst_temp = array_reverse($student_list_cst_temp);
                                    }

                                    foreach ($student_list_cst_temp as $key => $student) {
                                        $student_max_status = max($student['status_levels']);
                                        $placed = 0;

                                        if (array_key_exists($student['place_id'],$placed_student_list)) {
                                            $student_max_status = $status_levels['placed']['code'];
                                            $temp_company_name = $placed_student_list[$student['place_id']]['company'];
                                            $placed = 1;
                                        }

                                        if($placed == 1 && $temp_company_name != $student['company'] && $path_elements[0] == 'company' && $settings['SHOW_ALL_PLACED_TO_COMPANY'] == 0) {

                                        } else {
                                            if($student_max_status == $sequence[$i]) {
                                                if(in_array($student_max_status,$card_visibility_permissions[$path_elements[0]])) {
                                                    if ($student_max_status == $status_levels['exit']['code']
                                                        || $student_max_status == $status_levels['requestaway']['code']
                                                        || $student_max_status == $status_levels['sendaway']['code']) {
                                                        $next_company = $common_pool['name'];
                                                        require $models_path.'model_next_company.php';
                                                    } elseif ($student_max_status == $status_levels['sent']['code']) {
                                                        $next_company = $student['next_company'];
                                                    }
                                                    require $views_path.'view_student_card.php';
                                                }
                                            }
                                        }
                                    }}} ?>
                        </div>
                    <?php }}

            }}}}

else {?>
    <div class="ul_list_index">
        <div class="ul_list_index_item">No person to show yet</div>
    </div>
    <?php
}
