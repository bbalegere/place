<?php

require $views_path . 'view_card_basic_data.php';

// IF candidate is idle and user is not a 'company_tracker' then show the current location
if(($student_max_status == $status_levels['idle']['code'])
  && ($path_elements[0] != 'company' || ($path_elements[0] == 'company' && $settings['SHOW_CURR_LOC_OF_STAT0_TO_COMPANY'] == 1))
  && isset($base_student_list[$student['place_id']])) {

  $current_location_display = $base_student_list[$student['place_id']]['current_location'];
  if($current_location_display == $common_pool['name']) {
    $display_company = $common_pool['value'];
  }
  elseif(isset($company_list[$base_student_list[$student['place_id']]['current_location']])) {
    $display_company = $company_list[$base_student_list[$student['place_id']]['current_location']]['company_name'];
  }
}

// ELSEIF candidate is scheduled/intransit - show going from current location to the company (for non-company trackers)
elseif (($student_max_status == $status_levels['scheduled']['code'] || $student_max_status == $status_levels['intransit']['code'])
      && ($path_elements[0] != 'company' || ($path_elements[0] == 'company' && $settings['SHOW_CURR_LOC_OF_STAT1_TO_COMPANY'] == 1 && $settings['SHOW_CURR_LOC_OF_STAT2_TO_COMPANY'] == 1))
      && isset($base_student_list[$student['place_id']])) {
  $from_company = $base_student_list[$student['place_id']]['current_location'];
  $to_company = $student['company'];

  if($from_company == $common_pool['name']) { $from_company_display = $common_pool['value']; }
  elseif(isset($company_list[$from_company])) { $from_company_display = $company_list[$from_company]['company_name']; }
  else { $from_company_display = "Unknown"; }

  if($to_company == $common_pool['name']) { $to_company_display = $common_pool['value']; }
  elseif(isset($company_list[$to_company])) { $to_company_display = $company_list[$to_company]['company_name']; }
  else { $to_company_display = "Unknown"; }

  $display_company = $from_company_display.' to '.$to_company_display;
}

// ELSEIF candidate is on out-request/sendaway/sent status - show from/to (not for company tracker on out-request)
elseif (($student_max_status == $status_levels['sendaway']['code']
      || $student_max_status == $status_levels['sent']['code'] )
      || ($student_max_status == $status_levels['requestaway']['code'] && $path_elements[0] != 'company' )) {

  $from_company = $company_list[$student['company']]['company'];
  if(isset($next_company)) {
    $to_company = $next_company;
  } else {
    $to_company = $common_pool['name'];
  }

  if($from_company == $common_pool['name']) { $from_company_display = $common_pool['value']; }
  elseif(isset($company_list[$from_company])) { $from_company_display = $company_list[$from_company]['company_name']; }
  else { $from_company_display = "Unknown"; }

  if($to_company == $common_pool['name']) { $to_company_display = $common_pool['value']; }
  elseif(isset($company_list[$to_company])) { $to_company_display = $company_list[$to_company]['company_name']; }
  else { $to_company_display = "Unknown"; }

  $display_company = $from_company_display.' to '.$to_company_display;
}

 ?>
