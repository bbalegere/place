<script type="text/javascript">
    function blockSpecialChar(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return (k > 96 && k < 123);
        }
    function nonZeroNum(e){
        var k;
        document.all ? k = e.keyCode : k = e.which;
        return (k > 47 && k < 58);
        }
</script>


<?php foreach ($message as $key => $message_prompt): ?>
  <div class="ul_list_index">
    <?php print_r($message_prompt); ?>
  </div>
<?php endforeach; ?>

<!-- This is for Students upload -->
<div class="ul_list_index">
  <div><b>1. Students</b></div>
    <div class="suw_desc">Upload CSV in format: Process_ID | Student RollNo | Student_Section | Student_Gender | Current_Location | Actual Names <br> Include headers in the csv </div>
  <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="student_list_upload_form" class='suw-mod-form' style="display:inline;">
      <input type="file" name="student_list_file" id="upload" accept=".csv" class="" style="width:247px;">
      <input type="submit" value="Upload" name="student_list_upload" class="reg-mod-elements reg-mod-button">
  </form>
  <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_students" class='form-row' style="display:inline;">
      <input type="submit" value="Download" name="download_students" class="reg-mod-elements reg-mod-button">
  </form>
  <?php if($path_elements[0] == 'admin' || $path_elements[0] == 'control') { ?>
  <hr>
  <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="reset_students" class='form-row' style="display:inline;">
      <input type="submit" value="Reset Students" name="reset_students" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to reset all the students? This will Reset the location of all candidates to CP.')">
  </form>
  <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="delete_students" class='form-row' style="display:inline;">
      <input type="submit" value="Delete Students" name="delete_students" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to delete all the students? This will Delete all candidates.')">
  </form>
  <?php } ?>
</div>


<!-- This is for companies upload -->
<div class="ul_list_index">
    <div><b>2. Companies</b></div>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="add_single_copmany" class='suw-mod-form'>
        <input type="text" size="10" placeholder="Code" name="company" onkeypress="return blockSpecialChar(event)" maxlength="10">
        <input type="text" size="15" placeholder="Company Name" name="company_name">
        <input type="text" size="6" placeholder="Panels" name="panels" onkeypress="return nonZeroNum(event)">
        <select name="type" class = ''>
            <option value="anonymous" selected disabled>Type</option>
            <option value="i">Interview</option>
            <option value="g">GD</option>
        </select>
        <select name="company_status" class = ''>
            <option value="unknown" selected disabled>Status</option>
            <option value="0">On</option>
            <option value="1">Paused</option>
            <option value="2">Stopped</option>
            <option value="3">Exit</option>
        </select>
        <select name="day" class = ''>
            <option value="unknown" selected disabled>Day</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="gd">GD</option>
        </select>
        <select name="slot" class = ''>
            <option value="unknown" selected disabled>Slot</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select>
        <select name="freeflow" class = ''>
            <option value="unknown" selected disabled>Free</option>
            <option value="0">Off</option>
            <option value="1">On</option>
        </select>
        <input type="submit" value="Add" name="add_company" class="reg-mod-elements reg-mod-button">
    </form>
    <hr>

    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="company_list_upload_form" class='suw-mod-form' style="display:inline;">
        <input type="file" name="company_list_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="company_list_upload" class="reg-mod-elements reg-mod-button">
    </form>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_company_list" class='form-row' style="display:inline;">
        <input type="submit" value="Download" name="download_company_list" class="reg-mod-elements reg-mod-button">
    </form>

</div>

<!-- This is for raw preferences upload -->
<div class="ul_list_index">
    <div><b>3. Raw Preferences</b></div>
    <div class="suw_desc">Upload CSV in format: Process_ID | Company Code | Preference | relative | day | slot <br> Include headers in the csv </div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="raw_preference_upload_form" class='suw-mod-form' style="display:inline;">
        <input type="file" name="raw_preference_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="raw_preference_upload" class="reg-mod-elements reg-mod-button">
    </form>
        <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="raw_preference_download" class='form-row' style="display:inline;">
            <input type="submit" value="Download" name="raw_preference_download" class="reg-mod-elements reg-mod-button">
        </form>
</div>


<!-- This is for Shortlists upload -->
<div class="ul_list_index">
    <div><b>4. Shortlists</b></div>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="add_single_shortlist" class='suw-mod-form'>
        <select name="place_id" class = 'scw-company-list'>
            <option value="anonymous" selected disabled>Student</option>
            <?php foreach ($base_student_list as $student_key => $student) { ?>
                <option value="<?php echo $student['id']; ?>"><?php echo $student['id']." ".$student['student_name']; ?></option>
            <?php } ?>
        </select>
        <select name="company" class = 'scw-company-list'>
            <option value="<?php echo $common_pool['name']; ?>" selected disabled>Company</option>
            <?php foreach ($company_list as $company_key => $company) { ?>
                <option value="<?php echo $company['company']; ?>"><?php echo $company['company_name']; ?></option>
            <?php } ?>
        </select>
        <input type="submit" value="Add" name="add_single_shortlist" class="reg-mod-elements reg-mod-button">
        <input type="submit" value="Remove" name="remove_single_shortlist" class="reg-mod-elements reg-mod-button">
    </form>
    <hr>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="shortlist_upload_form" class='suw-mod-form' style="display:inline;">
        <input type="file" name="shortlist_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="shortlist_upload" class="reg-mod-elements reg-mod-button">
    </form>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_shortlists" class='form-row' style="display:inline;">
        <input type="submit" value="Download" name="download_shortlists" class="reg-mod-elements reg-mod-button">
    </form>

    <?php if($path_elements[0] == 'admin' || $path_elements[0] == 'control') { ?>
        <hr>
        <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="reset_entries" class='form-row' style="display:inline;">
            <input type="submit" value="Reset Shortlists" name="reset_shortlists" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to reset all the shortlists? This will Reset the state of all candidates.')">
        </form>
        <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="delete_entries" class='form-row' style="display:inline;">
            <input type="submit" value="Delete Shortlists" name="delete_shortlists" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to delete all the shortlists? This will Delete shortlists of all candidates.')">
        </form>
        <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="reset_non_placed_entries" class='form-row' style="display:inline;">
            <input type="submit" value="Reset (non-LP)" name="reset_non_placed_shortlists" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to reset all the non-placed shortlists? This will reset shortlists of all non-LP candidates.')">
        </form>
        <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="delete_non_placed_entries" class='form-row' style="display:inline;">
            <input type="submit" value="Delete (non-LP)" name="delete_non_placed_shortlists" class="reg-mod-elements reg-mod-button" onclick="return confirm('Are you sure you want to delete all the non-placed shortlists? This will Delete shortlists of all non-LP candidates.')">
        </form>

    <?php } ?>
</div>

<!-- This is for Dream Candidates upload -->
<div class="ul_list_index">
    <div><b>5. Dream Candidates List</b></div>
    <div class="suw_desc">Upload CSV in format: Process_ID <br></div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="dream_list_upload_form" class='suw-mod-form'>
        <input type="file" name="dream_list_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="dream_list_upload" class="reg-mod-elements reg-mod-button">
    </form>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_dream_students" class='form-row' style="display:inline;">
        <input type="submit" value="Download" name="download_dream_students" class="reg-mod-elements reg-mod-button">
    </form>
</div>


<!-- This is for Hotlist upload -->
<div class="ul_list_index">
    <div><b>6. Hotlist Upload</b></div>
    <div class="suw_desc">Upload only after uploading shortlist above. Format:: p_id | comp_code</div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="hotlist_upload_form" class='suw-mod-form' style="display:inline;">
        <input type="file" name="hotlist_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="hotlist_upload" class="reg-mod-elements reg-mod-button">
    </form>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_hotlists" class='form-row' style="display:inline;">
        <input type="submit" value="Download" name="download_hotlists" class="reg-mod-elements reg-mod-button">
    </form>
</div>


<!-- This is for LP List upload -->
<div class="ul_list_index">
    <div><b>7. LP List</b></div>
    <div class="suw_desc">Upload CSV in format: Process_ID <br> </div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="lp_list_upload_form" class='suw-mod-form'>
        <input type="file" name="lp_list_file" id="upload" accept=".csv" class="" style="width:247px;">
        <input type="submit" value="Upload" name="lp_list_upload" class="reg-mod-elements reg-mod-button">
        <input type="submit" value="Download" name="download_LP_List" class="reg-mod-elements reg-mod-button">
    </form>
</div>


<!-- This is for Grid No Blank Calculation -->
<div class="ul_list_index">
    <div><b>8. Calculate Grid No Blanks</b></div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post"
          name="grid_noblanks_form" class='suw-mod-form' style="display:inline;">
        <select name="day" class = ''>
            <option value="unknown" selected disabled>Day</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="gd">GD</option>
        </select>
        <select name="slot" class = ''>
            <option value="unknown" selected disabled>Slot</option>
            <option value="0">0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
        </select>
        <br>
        <br>
        <input type="submit" value="Calculate Grid No Blank" name="grid_list_upload" class="reg-mod-elements reg-mod-button">
    </form>
</div>


<div class="ul_list_index">
  <div><b>Static</b></div>
    <div class="suw_desc">Upload CSV in format: Process_ID | Company_Code | GD Round | GD Panel <br> Include headers in the csv. For interview companies GD Round & GD Panel = 0 </div>
  <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="static_list_upload_form" class='suw-mod-form'>
      <input type="file" name="static_list_file" id="upload" accept=".csv" class="" style="width:247px;">
      <input type="submit" value="Upload" name="static_list_upload" class="reg-mod-elements reg-mod-button">
  </form>
  </div>



<div class="ul_list_index">
  <div><b>Process GD List</b></div>
    <div class="suw_desc">Input: Company Name as column header and the shortlisted candidate names below it. Multiple columns for multiple companies<br>
    Output DOC Header: Process ID | Process Names | Company Names</div>
    <form action="<?php echo $request_uri_for_form; ?>" enctype="multipart/form-data" method="post" name="static_list_upload_form" class='suw-mod-form'>
      <input type="file" name="gd_file" id="upload" accept=".csv" class="" style="width:247px;">
      <input type="submit" value="Upload & Process" name="gd_upload" class="reg-mod-elements reg-mod-button">
      <input type="submit" value="Upload & Purge" name="gd_purge" class="reg-mod-elements reg-mod-button">
  </form>
</div>

<div class="ul_list_index">
<div><b>Candidates still in process</b></div>
    <div class="suw_desc">Download all candidates still in process <br> Download LP candidate list <br> Output format: Process_ID | Name</div>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="download_candidates" class='form-row' style="display:inline;">
      <input type="submit" value="Candidates in process" name="download_candidates" class="reg-mod-elements reg-mod-button">
       <input type="submit" value="LP Candidates" name="lp_students" class="reg-mod-elements reg-mod-button">
  </form>
</div>

<div class="ul_list_index">
     <div><b>Candidates in System</b></div>
    <div class="suw_desc">Candidates in states - Scheduled to Inside in the specified round and panel. Output is only for running companies.
        <br> Output format: Process_ID  | Name</div>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="system_candidates" class='suw-mod-form'>
        
        <select name="day" class = ''>
        <option value="unknown" disabled>Day</option>
        <option value="0" selected>0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="gd">GD</option>
    </select>
    <select name="slot" class = ''>
        <option value="unknown" disabled>Slot</option>
        <option value="0" selected>0</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
    </select>
        <input type="submit" value="Download" name="in_system_candidates" class="reg-mod-elements reg-mod-button"/> 
    
    </form>
</div>

<div class="ul_list_index">
    <div><b>Download Shortlists</b></div>
    <br>

    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="get_purged_shortlist" class='suw-mod-form'>
        <select name="company" class = 'scw-company-list'>
            <option value="<?php echo $common_pool['name']; ?>" selected disabled>Company</option>
            <?php foreach ($company_list as $company_key => $company) { ?>
                <option value="<?php echo $company['company']; ?>"><?php echo $company['company_name']; ?></option>
            <?php } ?>
        </select>
        <input type="submit" value="Download Purged Shortlists" name="get_purged_shortlist" class="reg-mod-elements reg-mod-button">
        <br> <br>
        <select name="company" class = 'scw-company-list'>
            <option value="<?php echo $common_pool['name']; ?>" selected disabled>Company</option>
            <?php foreach ($company_list as $company_key => $company) { ?>
                <option value="<?php echo $company['company']; ?>"><?php echo $company['company_name']; ?></option>
            <?php } ?>
        </select>
        <input type="submit" value="Download Non-Shortlists" name="get_non_shortlist" class="reg-mod-elements reg-mod-button">
        <br> <br>
        <select name="company" class = 'scw-company-list'>
            <option value="<?php echo $common_pool['name']; ?>" selected disabled>Company</option>
            <?php foreach ($company_list as $company_key => $company) { ?>
                <option value="<?php echo $company['company']; ?>"><?php echo $company['company_name']; ?></option>
            <?php } ?>
        </select>
        <input type="submit" value="Download Hotlist" name="get_hotlist" class="reg-mod-elements reg-mod-button">
        <br> <br>
        <select name="day_short" class = ''>
            <option value="unknown" disabled>Day</option>
            <option value="0" selected>0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <select name="slot_short" class = ''>
            <option value="unknown" disabled>Slot</option>
            <option value="0" selected>0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
        </select>
        <input type="submit" value="Download Total Shortlists" name="get_total_shortlist" class="reg-mod-elements reg-mod-button"/>



        <br> <br>
        <select name="day_short" class = ''>
            <option value="unknown" disabled>Day</option>
            <option value="0" selected>0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="3">4</option>

        </select>
        <select name="slot_short" class = ''>
            <option value="unknown" disabled>Slot</option>
            <option value="0" selected>0</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="3">4</option>
            <option value="3">5</option>

        </select>
        <input type="submit" value="Download All Purged Shortlists" name="get_all_purged_shortlists" class="reg-mod-elements reg-mod-button"/>
    </form>
</div>










<?php

$day_list = array(0,1,2,3,4,'gd');
$slot_list = array(0,1,2,3,4);

foreach ($company_list as $company_key => $company) {
    $company_slot_list[$company['day']][$company['slot']][] = $company;
}

foreach ($day_list as $day_key => $day) {
  foreach ($slot_list as $slot_key => $slot) {
    if(!empty($company_slot_list[$day][$slot])) {
?>
      <div class="ul_list_index">
        Day <?php echo $day; ?> Slot <?php echo $slot; ?> Companies
        <hr>
        <?php foreach ($company_slot_list[$day][$slot] as $company_key => $company):
                $selected = array('','','','','anon_status'=>'selected', 'i'=>'', 'g'=>'', 'anon_company'=>'selected');
                $day_selected = array('','','','','','gd'=>'','anon_day'=>'selected');
                $slot_selected = array('','','','','','anon_slot'=>'selected');
                $freeflow_selected = array('','');

                $selected[$company['type']] = 'selected';
                $selected[$company['company_status']] = 'selected';
                $day_selected[$company['day']] = 'selected';
                $slot_selected[$company['slot']] = 'selected';
                $freeflow_selected[$company['freeflow']] = 'selected';

                ?>
                <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo $company['company']; ?>" class="suw-mod-form">
                  <input type="text" size="10" value="<?php echo $company['company']; ?>" name="company" class="reg-mod-elements username" readonly>
                  <input type="text" size="15" value="<?php echo $company['company_name']; ?>" name="company_name" class="reg-mod-elements username">
                  <input type="text" size="6" value="<?php echo $company['panels']; ?>" name="panels" class="reg-mod-elements username" onkeypress="return nonZeroNum(event)">
                  <select name="type" class = ''>
                      <option value="anonymous" <?php echo $selected['anon_company']; ?> disabled>Type</option>
                      <option value="i" <?php echo $selected['i']; ?>>Interview</option>
                      <option value="g" <?php echo $selected['g']; ?>>GD</option>
                  </select>
                  <select name="company_status" class = ''>
                      <option value="unknown" <?php echo $selected['anon_status']; ?> disabled>Status</option>
                      <option value="0" <?php echo $selected[0]; ?>>On</option>
                      <option value="1" <?php echo $selected[1]; ?>>Paused</option>
                      <option value="2" <?php echo $selected[2]; ?>>Stopped</option>
                      <option value="3" <?php echo $selected[3]; ?>>Exit</option>
                  </select>
                  <select name="day" class = ''>
                      <option value="unknown" <?php echo $day_selected['anon_day']; ?> disabled>Day</option>
                      <?php for($i = 0; $i < 5; $i++) { ?>
                      <option value="<?php echo $i; ?>" <?php echo $day_selected[$i]; ?>><?php echo $i; ?></option>
                      <?php } ?>
                      <option value="gd" <?php echo $day_selected['gd']; ?>>GD</option>
                  </select>
                  <select name="slot" class = ''>
                      <option value="unknown" <?php echo $slot_selected['anon_slot']; ?> disabled>Slot</option>
                      <?php for($i = 0; $i < 5; $i++) { ?>
                      <option value="<?php echo $i; ?>" <?php echo $slot_selected[$i]; ?>><?php echo $i; ?></option>
                      <?php } ?>
                  </select>
                  <select name="freeflow" class = ''>
                      <option value="unknown" selected disabled>Free</option>
                      <option value="0" <?php echo $freeflow_selected[0]; ?>>Off</option>
                      <option value="1" <?php echo $freeflow_selected[1]; ?>>On</option>
                  </select>
                  <input type="text" size="15" placeholder="Enter Room #" value="<?php echo htmlentities($company['company_room']); ?>" name="company_room" class="reg-mod-elements">
                  <input type="submit" value="Modify" name="modify_company" class="reg-mod-elements reg-mod-button">
                  <input type="submit" value="Delete" name="delete_company" class="reg-mod-elements reg-mod-button">
                </form>
        <?php endforeach; ?>
      </div>
<?php
    }
  }
}
?>