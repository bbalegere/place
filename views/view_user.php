<?php require $views_path."page_header.php"; ?>

<div class="ul_list_index login_box">
  <div class='login_box_header'>
    <div class='place_header'>
      PLACE
    </div>
    <div class='place_name'>
      PLAcement Control Engine
    </div>
  </div>
  <div class='login_form_area'>
    <?php
      if(isset($form_error)) {
        switch ($form_error) {
          case 9:
            header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']);
            die();
          case 1:
            echo "Incorrect Information";
            break;
        }
      }
      ?>

    <?php
  if(isset($_SESSION['user'])) { ?>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="logout">
      <input type="text" value="<?php echo $_SESSION['user']; ?>" name="username" class="reg-elements username login-elements" readonly>
      <input type="submit" value="Logout" name="logout" class="reg-elements reg-button">
    </form>
  <?php } else { ?>
    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="logout">
      <input type="text" placeholder="Username" name="username" class="reg-elements username login-elements">
      <input type="password" placeholder="Password" name="password" class="reg-elements password login-elements">
      <input type="submit" value="Login" name="login" class="reg-elements reg-button login-button">
    </form>
    <?php } ?>
  </div>
</div>

<?php require $views_path."page_footer.php"; ?>
