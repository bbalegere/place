<script lang="javascript">
function toggle(source,rxpy) {
  checkboxes = document.getElementsByClassName(rxpy);
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<?php
$this_non_placed_list = $non_placed_list;
if(isset($student_list_cst_gd) && isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['type'] == 'g') {
  foreach ($student_list_cst_gd as $key1 => $student_list_cst_gd_1) {
    foreach ($student_list_cst_gd_1 as $key2 => $student_list_cst_gd_2) {
      foreach ($student_list_cst_gd_2 as $key => $student) {
        if(isset($this_non_placed_list[$student['place_id']])) {
          unset($this_non_placed_list[$student['place_id']]);
        }
      }
    }
  }
}
unset($student);


$right_side_open = 0;
$right_side_close = 0;
  if(isset($student_list_cst_gd) && isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['type'] == 'g') {
    foreach ($student_list_cst_gd as $key1 => $student_list_cst_gd_1) {
      foreach ($student_list_cst_gd_1 as $key2 => $student_list_cst_gd_2) {
        $i = $key1;
        $j = $key2;
        if(($i == 0 && $j == 0) || ($i != 0 && $j != 0)) {
          if($i == 0 && $j == 0) {
            echo "<div class = 'gd_unpanelled'>";
          } elseif($right_side_open == 0) {
            echo "<div class = 'gd_panelled'>";
            $right_side_open = 1;
            ksort($student_list_cst_gd_2);
          }
        ?>
        <div class='ul_list_index gd_panel_box status-<?php echo $gd_panels_min_status[$i][$j]; ?>'>
          <?php if($i == 0 && $j == 0) { ?>
            <div class='gd_panel_box_header'><?php echo $unscheduled_count." Unpanelled"; ?></div>
          <?php } else { ?>
            <div class='gd_panel_box_header'>R<?php echo $i; ?>-P<?php echo $j." - Min ".$gd_panels_min_status[$i][$j]." ".$status_levels[$status_levels_codes[$gd_panels_min_status[$i][$j]]]['value']; ?></div>
          <?php }?>
          <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo 'gd-'.$i.'-'.$j; ?>">
            <div class='gd_box_rxpy_bar'>
              <input type='hidden' class='gd_rp' value='<?php echo $path_elements[2]; ?>' name='company' readonly>
              <input type='number' class='gd_rp' min='0' placeholder='R' name='gd_round'>
              <input type='number' class='gd_rp' min='0' max = '<?php echo $company_list[$path_elements[2]]['panels']; ?>' placeholder='P' name='gd_panel'>
              <input type="submit" class='navbar_submit gd_rp_submit' value="R/P" name="set_gd"><br>
            </div>
            <div class='gd_box_rxpy_bar'>
              <select name="gd_status" class='gd_status_dropdown'>
                <option selected disabled>---</option>
                <?php foreach ($status_levels as $key => $status_level):
                  echo "<option value='".$status_level['code']."'>".$status_level['code']." - ".$status_level['value']."</option>";
                endforeach; ?>
              </select>
              <input type="submit" class='navbar_submit gd_rp_submit' value="Status" name="set_gd"><br>
            </div>
        <?php
        echo "<div> Candidates in Panel : ".count($student_list_cst_gd_2)."</div><br/>";
        foreach ($student_list_cst_gd_2 as $key => $student) {
          //print_r($student);
          if($student['gd_round'] == $i && $student['gd_panel'] == $j) {
            $student_max_status = max($student['status_levels']);

            if ($student_max_status == $status_levels['exit']['code']
             || $student_max_status == $status_levels['requestaway']['code']
             || $student_max_status == $status_levels['sendaway']['code']) {
                $next_company = $common_pool['name'];
                require $models_path.'model_next_company.php';
            } elseif ($student_max_status == $status_levels['sent']['code']) {
                $next_company = $student['next_company'];
            }
            require $views_path.'view_card_decisions.php'; ?>

            <div class='status-<?php echo $student_max_status; ?>'>
              <label>
                <input type='checkbox' class='gd_checkbox <?php echo $i.$j; ?>' name='gd_students[]' value='<?php echo $student['id']; ?>' >
                  <input size="1" type="text" value="<?php if ($student_data_dream[$student['place_id']] == '1') echo 'D'; ?>" readonly name="dream" class="transparent_background">

                  <a <?php if ($student['student_gender'] == 'F' || $student['student_gender'] == 'Female') echo 'STYLE="color: #ff1a8c"'; ?> target='_blank' href='<?php echo $website_uri.$path_elements[0].'/sst/'.$student['place_id']; ?>'>
                    <?php echo $student['place_id']." ".$student['student_name']." ".$student_max_status; ?>
                  </a>
              </label>
                <?php
                if(array_key_exists($student['place_id'], $other_shortlists))
                {
                    $stat_value = getStudentStatus($other_shortlists[$student['place_id']]);
                    if (array_key_exists('company', $stat_value)) {
                        ?>

                        <div type="text" size="5" name="current_student_status" class='student_status-1'>
                            <?php echo $stat_value['company']; ?>
                            <?php echo "    -->      "; ?>
                            <?php echo $status_levels[$status_levels_codes[$stat_value['status']]]['value']; ?>
                        </div>


                    <?php }
                }
                ?>
            </div>
            <?php
            //echo "<div class='status-".$student_max_status."'><label><input type='checkbox' class='gd_checkbox ".$i.$j."' name='gd_students[]' value='".$student['id']."' >"."<a target='_blank' href='".$website_uri.$path_elements[0].'/sst/'.$student['place_id']."'>".$student['place_id']." ".$student['student_name']." ".$student_max_status."</a></label></div>";
          }
        }
        ?>
          <label><input type="checkbox" class='gd_checkbox' onClick='toggle(this,<?php echo $i.$j; ?>)'>Toggle All</label>
          </form>
        </div>
        <?php
        if($i == 0 && $j == 0) { ?>
          <?php if ($company_list[$path_elements[2]]['freeflow'] == 1): ?>
            <div class='ul_list_index gd_panel_box'>
              <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="shortlist">
                <div class='gd_box_rxpy_bar'>
                  <input type='hidden' class='gd_rp' value='<?php echo $path_elements[2]; ?>' name='company' readonly>
                  <input type="submit" class='navbar_submit' value="Shortlist" name="shortlist"><br>
                </div>
                <?php foreach ($this_non_placed_list as $non_placed_key => $non_placed_student): ?>
                    <label>
                      <input type='checkbox' class='gd_checkbox unshortlisted' name='gd_students[]' value='<?php echo $non_placed_student['id']; ?>' >
                        <a target='_blank' href='<?php echo $website_uri.$path_elements[0].'/sst/'.$student['place_id']; ?>'>
                          <?php echo $non_placed_student['id']." ".$non_placed_student['student_name']; ?>
                        </a>
                    </label>
                <?php endforeach; ?>
              </form>
            </div>
          <?php endif; ?>
        </div>
        <?php }
      }
      }
    }
    if($right_side_open == 1) {
      echo "</div>";
    }
  }

 ?>
