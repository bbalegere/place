    </div>
    <footer class="site-footer">
      <?php
        $finish = microtime(TRUE);
        $total_time = round(($finish - $process_start_time)*1000, 0);
        $memory_usage = round(memory_get_usage()/(1024*1024), 1);
      ?>
        PLACE - Placement Control Engine<br>Agrim Gupta (<?php echo $total_time ?>ms)
        <!-- Created by Agrim Gupta PGP 2015-17 -->
        <!-- Enhanced by Ithi Garg & Srividya Ganesan PGP 2016-18 -->
    </footer>
  </body>
</html>
