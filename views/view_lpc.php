<div class="pyro">




<div class="ul_list_index">
   <?php
    if(isset($placed_student_list) && isset($base_student_list)) {
		$x=sizeof($placed_student_list);
      echo "<div class='lpc_count'>".$x."/".sizeof($base_student_list)."</div><br>";
      if(sizeof($placed_student_list) > 270 && sizeof($placed_student_list) < 300) {
        echo "<div class='lpc_message'>That's more LPs than Trump's seats!</div>";
      } elseif(sizeof($placed_student_list) >= 300 && sizeof($placed_student_list) < 320) {
        echo "<div class='lpc_message'>This is spartaaaa!!!</div>";
      } elseif(sizeof($placed_student_list) > 319 && sizeof($placed_student_list) < 333) {
        echo "<div class='lpc_message'>We just beat poor Sehwag!</div>";
      } elseif(sizeof($placed_student_list) > 332 && sizeof($placed_student_list) <= 333) {
        echo "<div class='lpc_message'>Triple Nelson attained! Time to hop your legs!</div>";
      } elseif(sizeof($placed_student_list) > 336 && sizeof($placed_student_list) <= 340) {
        echo "<div class='lpc_message'>That's more LPs than NDA's Lok Sabha seats!</div>";
      } elseif(sizeof($placed_student_list) > 340 && sizeof($placed_student_list) <= 345) {
        echo "<div class='lpc_message'>More LPs than Sanath Jayasuriya's highest runs!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 0) {
        echo "<div class='lpc_message'>Entire batch placed! Go home guys!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 1) {
        echo "<div class='lpc_message'>Last LP remains! Gather around guys!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 2) {
        echo "<div class='lpc_message'>And then there were two!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 3) {
        echo "<div class='lpc_message'>The 3 elves, Immortal, wisest, and fairest of all beings!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 4) {
        echo "<div class='lpc_message'>Just one boundary needed!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 5) {
        echo "<div class='lpc_message'>Start counting them on your fingers!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 6) {
        echo "<div class='lpc_message'>Billy Bowden's favorite shot needed!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 7) {
        echo "<div class='lpc_message'>The 7 Dwarf lords, great miners and craftsmen of the mountain halls!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 8) {
        echo "<div class='lpc_message'>That's one byte of LPs remaining!</div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 9) {
        echo "<div class='lpc_message'>Nine rings for the race of men!</div>";
      } elseif((sizeof($base_student_list)-$x) == 10) {
        echo "<div class='lpc_message'><a href='https://www.youtube.com/watch?v=ANv5UfZsvZQ' target='_blank'>Launch sequence! (Click)</a></div>";
      } elseif((sizeof($base_student_list)-$x) == 11) {
        echo "<div class='lpc_message'><a href='http://www.nasa.gov/mission_pages/apollo/missions/apollo11.html' target='_blank'>One giant leap for placement! (Click)</a></div>";
      } elseif((sizeof($base_student_list)-$x) == 12) {
        echo "<div class='lpc_message'><a href='http://fortune.com/2016/07/28/elon-musk-twelve-against-the-gods/' target='_blank'>Twelve against the Gods! (Click)</a></div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) == 13) {
        echo "<div class='lpc_message'><a href='https://en.wikipedia.org/wiki/Happy_number' target='_blank'>Happy Number! (Click)</a></div>";
      } elseif((sizeof($base_student_list)-sizeof($placed_student_list)) < 50) {
        echo "<div class='lpc_message'>Just ".(sizeof($base_student_list)-sizeof($placed_student_list))." more LP to go!</div>";
      } else {
        echo "<div class='lpc_message'>We're getting there! ".(sizeof($base_student_list)-sizeof($placed_student_list))." more LP to go!</div>";
      }
    } else {
      echo "0/".sizeof($base_student_list)."<br>";
    }
    ?>
 </div>

  <div class="ul_list_index unscheduled">
    <h3>Placed Candidates</h3>
    <table>
      <thead><th>ID</th><th>Student</th><th>Company</th><th>Placed Time</th></thead>
      <?php foreach ($placed_student_list as $key => $placed_student): ?>
        <tr>
          <td><?php echo $placed_student['place_id']; ?></td>
          <td><?php echo $placed_student['student_name']; ?></td>
          <td><?php echo $company_list[$placed_student['company']]['company_name']; ?></td>
          <td><?php echo date("H:i", $placed_student['status_timestamps'][$status_levels['placed']['code']-1]); ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>

  <div class="ul_list_index unscheduled">
    <h3>Non-Placed Candidates</h3>
    <table>
      <thead><th>ID</th><th>Student</th>
          
          
          
          <?php
          if($displayCriticality==true)
          {
           $i =0; 
          
          foreach($criticalityInfo as $curr => $criticality)
          {
              if(count($criticality)!=0)
              {
          
          ?>
          <th><?php echo "Slot ".$i ?></th>
         
          <?php 
              $i = $i+1;
              } }  }
          
          ?>
          
         <th>Location</th></thead>
      <?php
        foreach ($non_placed_list as $key => $non_placed_student):
          $current_location = $non_placed_student['current_location'];
          if($current_location == 'cp') { $current_location_name = 'CP'; }
          else { $current_location_name = $company_list[$current_location]['company_name']; }
          ?>
        <tr>
          <td><?php echo $non_placed_student['id']; ?></td>
          <td><?php echo $non_placed_student['student_name']; ?></td>
            
            <?php 
               if($displayCriticality==true)
               {
                  foreach($criticalityInfo as $curr => $criticality)
                  {
                      if(count($criticality)!=0)
                      {
             ?>
            <td><?php
                      if(array_key_exists($non_placed_student['id'],$criticality))
                       {
                        echo $criticality[$non_placed_student['id']];
                       }
                       else
                       {
                        echo "-";
                       }
            ?></td>
            
            <?php 
                                                                                     
                   }
              }
        }
                                                                                     
                    ?>
          <td><?php echo $current_location_name; ?></td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
</div>