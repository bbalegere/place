
<form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" class='border_bottom' >
      <input type="text" size="16" value="Company" name="company_name" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Schedule Alert" name="schedule_alert" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Queue Length" name="company_panels" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Max In Time (min)" name="threshold_time" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Avg In Time (min)" name="avg_intime" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Inside Panel" name="count_inside" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Panels" name="company_panels" class='alert_entries alert_header' readonly>
      <input type="text" size="16" value="Placed" name="company_placed" class='alert_entries alert_header' readonly>
</form>

<div>
    


<?php
    $buffer_time_min = 5;
    $allocated_time = 45;

    foreach($company_panel_mapping as $key => $cp_entry) {
        if ($key != "zzlp" && $key != "zzDummy") {
            $company_panels_grouped[$cp_entry['status']][] = $cp_entry;
        }
    }
    if(isset($company_panels_grouped))
    {
        ksort($company_panels_grouped);
    }

    for($i=0; $i<=3; $i++)
    {
        if(!isset($company_panels_grouped[$i]) || sizeof($company_panels_grouped[$i])==0)
        {
            continue;
        }

        ?>
        <div class="status_header">Status: <?php echo $company_status_levels[$i]; ?></div>

        <?php

        if(array_key_exists($i, $company_panels_grouped))
        {
            $data_list = $company_panels_grouped[$i];
            foreach($data_list as $key => $cp_entry)
            {
                ?>
                <input type="text" size="16" class="alert_entries company_<?php echo $company_status_levels[$i]; ?>" value="<?php  echo $cp_entry['cname'] ?>" name="company_name" readonly>

                  <?php
                  // Count placed candidates in each company
                  $cp_entry['placed'] = R::count($tables['entry']['name'], "company=:company AND stat11=11", [":company" => $cp_entry['cname']]);

                   /* Count the students inside the panel */
                    $company_candidates = $cp_entry['students'];
                    $in_count = 0;
                    $alert_count = 0;
                    $sch_count = 0;

                    /*Compute & update threshold time*/
                    $max_in_time=0;
                    $sum_in_time=0;
                    $avg_in_time=0;
                    $sum_in_count=0;

                    foreach($company_candidates as $comp_key => $stud_entry)
                    {
                        if($stud_entry['status_levels'][$status_levels['exit']['code']-1] == $status_levels['exit']['code'] AND $stud_entry['later'] == 0)
                        {
                            $sum_in_time = $sum_in_time + round((($stud_entry['status_timestamps'][$status_levels['exit']['code']-1]-$stud_entry['status_timestamps'][$status_levels['inside']['code']-1]))/60,0);
                            $sum_in_count++; // sum of past in count
                        }
                        /*If candidate is still in the panel*/
                        if($stud_entry['status_levels'][$status_levels['exit']['code']-1] == 0 AND $stud_entry['status_levels'][$status_levels['inside']['code']-1] == $status_levels['inside']['code'])
                        {
                            $in_count++; // current in count
                            $in_time = round((time()-$stud_entry['status_timestamps'][$status_levels['inside']['code']-1])/60,0);
                        }
                        else {
                            $in_time = 0;
                        }
                        $max_in_time = ($in_time > $max_in_time) ? $in_time : $max_in_time;
                    }
                    $avg_in_time=($sum_in_count>0) ? round($sum_in_time/$sum_in_count, 0) : 0;

                    $queue_candidates = $cp_entry['scheduled'];
                    $sch_count =0;

                    foreach($queue_candidates as $q_key => $sch_entry)
                    {
                        if($sch_entry['status_levels'][$status_levels['requested']['code']]==0)
                        {
                            $sch_count++;
                        }
                        if(isset($placed_student_list[$sch_entry['place_id']]))
                        {
                            $sch_count = $sch_count - 1;
                        }
                    }
                    $alert_count =  (2 * $cp_entry['panels']) -  $in_count - $sch_count;

                    if($alert_count>0)
                    {

                        ?>
                        <input type="text" size="16" class="alert_entries " value="<?php  echo $alert_count ?>" name="schedule_alert" readonly>

                        <?php
                    }
                    else
                    {
                        ?>
                        <input type="text" size="16" class="alert_entries" value=" - " name="schedule_alert" readonly>

                        <?php
                    }
                    ?>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $sch_count ?>" name="count_inside" readonly>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $max_in_time ?>" name="count_inside" readonly>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $avg_in_time ?>" name="count_inside" readonly>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $in_count  ?>" name="count_inside" readonly>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $cp_entry['panels'] ?>" name="company_panels" readonly>
                    <input type="text" size="16" class="alert_entries" value="<?php echo $cp_entry['placed'] ?>" name="company_placed" readonly><br/>
                    <?php
                }
            }//end of processing for companies of a particular status

    }

    
?>
</div>