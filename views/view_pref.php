<script type="text/javascript">
    $(document).ready(function () {
        $('html, body').hide();
        var desiredHeight = $(window).height() - 800;
        if (window.location.hash) {
            setTimeout(function () {
                $('html, body').scrollTop().show();
                $('html, body').animate({
                    scrollTop: $(window.location.hash).offset().top - desiredHeight
                }, 1000)
            }, 0);
        }
        else {
            $('html, body').show();
        }
    });
</script>

<div id="pref-container">

    <div id="column1">
        <?php
        $columnNo = 1;
        $otherColumn = 2;
        require $views_path . 'view-pref-column.php';
        ?>
    </div>

    <div id="column2">
        <?php
        $columnNo = 2;
        $otherColumn = 1;
        require $views_path . 'view-pref-column.php';
        ?>
    </div>


</div>

