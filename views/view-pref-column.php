<br/><br/>
<div id="pref-navbar">
    <form id="pref-form" action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8">
        Day:
        <select name="day-<?php echo $columnNo; ?>" class='navbar_dropdown_list'>
            <option value="0" <?php if ($pref_options[$columnNo]['day'] == -1 || $pref_options[$columnNo]['day'] == 0) echo "selected"; ?> >
                0
            </option>
            <option value="1" <?php if ($pref_options[$columnNo]['day'] == 1) echo "selected"; ?> >1</option>
            <option value="2" <?php if ($pref_options[$columnNo]['day'] == 2) echo "selected"; ?> >2</option>
        </select>

        Slot:
        <select name="slot-<?php echo $columnNo; ?>" class='navbar_dropdown_list'>
            <option value="0" <?php if ($pref_options[$columnNo]['slot'] == -1 || $pref_options[$columnNo]['slot'] == 0) echo "selected"; ?>>
                0
            </option>
            <option value="1" <?php if ($pref_options[$columnNo]['slot'] == 1) echo "selected"; ?>>1</option>
            <option value="2" <?php if ($pref_options[$columnNo]['slot'] == 2) echo "selected"; ?> >2</option>
        </select>
        <input type="submit" value="Submit" name="submit-<?php echo $columnNo; ?>" disabled>

        <input type="submit" value="Sort By Criticality" name="sort-<?php echo $columnNo; ?>" disabled>

        <input type="submit" value="Sort By ID" name="sortid-<?php echo $columnNo; ?>" disabled>

        <input type="hidden" value="<?php echo $pref_options[$otherColumn]['sort']; ?>"
               name="sort-view-<?php echo $otherColumn; ?>" readonly>
        <input type="hidden" value="<?php echo $pref_options[$otherColumn]['day']; ?>"
               name="day-<?php echo $otherColumn; ?>" readonly>
        <input type="hidden" value="<?php echo $pref_options[$otherColumn]['slot']; ?>"
               name="slot-<?php echo $otherColumn; ?>" readonly>
        <input type="hidden" value="<?php echo $pref_options[$columnNo]['sort']; ?>"
               name="sort-view-<?php echo $columnNo; ?>" readonly>


    </form>
    <br/><br/>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: srividyag
 * Date: 10/02/18
 * Time: 9:44 PM
 */

$arrayToSort = array();

if ($pref_options[$columnNo]['sort'] == 1) {
    foreach ($pref as $studPref) {
        foreach ($studPref as $key => $value) {
            if (!isset($arrayToSort[$key])) {
                $arrayToSort[$key] = array();
            }
            $arrayToSort[$key][] = $value;
        }
    }
    $orderby = "criticality";
    array_multisort($arrayToSort[$orderby], SORT_ASC, $pref);
}


foreach ($pref as $key => $prefEntry) {

    if ((($pref_options[$columnNo]['day'] != -1 || $pref_options[$columnNo]['slot'] != -1) && $prefEntry['day'] == $pref_options[$columnNo]['day'] && $prefEntry['slot'] == $pref_options[$columnNo]['slot']) ||
        ($pref_options[$columnNo]['day'] == -1 || $pref_options[$columnNo]['slot'] == -1)) {

        ?>


        <div id="pref-card-container">

        <?php
        if (array_key_exists($prefEntry['id'], $stud_list)) {
            $stud = $stud_list[$prefEntry['id']];
            $flag = false;
            if ($prefEntry != null) {

                for ($i = 1; $i <= 20; $i++) {
                    if ($prefEntry['pref' . $i] != '' && $prefEntry['pref' . $i] != NULL) {
                        $student = $stud[$prefEntry['pref' . $i]];
                        $cmp = $company_list[$student['company']];
                        $flag = true;
                        require $views_path . 'view_pref_card.php';
                    }
                }
                if ($flag) {
                    echo "<br/><br/>";
                } else {
                    $student = null;
                    $disp_place_id = $prefEntry['id'];
                    require $views_path . 'view_pref_card.php';
                    //Display only the place ID
                    echo "<br/><br/>";
                }

            }
        } else {


            $student = null;
            $disp_place_id = $prefEntry['id'];
            require $views_path . 'view_pref_card.php';
            //Display only the place ID
            echo "<br/><br/>";
        }


        ?></div><?php
    }
}
?>