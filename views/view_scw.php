<?php
  foreach ($base_student_list as $student_key => $student) {
?>

    <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo $student['id']; ?>" class='form-row'>
      <input type="text" size="4"  value="<?php  echo $student['id']; ?>" name="place_id" readonly>
      <input type="text" size="8"  value="<?php  echo $student['student_roll']; ?>" name="student_roll" readonly>
      <input type="text" size="1"  value="<?php  echo $student['student_section']; ?>" name="student_section" readonly>
      <input type="text" size="16" value="<?php  echo ucwords(strtolower($student['student_name'])); ?>" name="student_name" readonly>
      <input type="text" size="6"  value="<?php  echo $student['student_gender']; ?>" name="student_gender" readonly>
      <input type="text" size="10" value="<?php  if($student['current_location']=='0') { $student['current_location'] = $common_pool['value']; } echo $student['current_location']; ?>" name="current_location" readonly>

      <?php
        if(isset($short_list) && array_key_exists($student['id'],$short_list)){
            $this_short_list = $short_list[$student['id']];
      ?>
        <select name="location" class = 'scw-location-list'>
          <option value="<?php echo $common_pool['name']; ?>" selected>CP</option>
          <?php
            ksort($this_short_list);
            foreach ($this_short_list as $company_key => $company) {
              if(isset($company_list[$company['company_name']])) { ?>
          <option value="<?php echo $company['company_name']; ?>"><?php echo $company_list[$company['company_name']]['company_name']; ?></option>
          <?php }} ?>
        </select>
        <?php if($path_elements[0] == 'control') { ?>
        <input type="submit" value="Location" name="submit" class="scw-button" style="font-size: 12px; height: 20px;"> |
            <?php } ?>
            <select name="company" class = 'scw-company-list'>
                <option value="<?php echo $common_pool['name']; ?>" selected disabled>Company</option>
                <?php
                  ksort($this_short_list);
                  foreach ($this_short_list as $company_key => $company) {
                    if(isset($company_list[$company['company_name']])) { ?>
                <option value="<?php echo $company['company_name']; ?>"><?php echo $company_list[$company['company_name']]['company_name']." --> ".max($company['status_levels'])." ".$status_levels[$status_levels_codes[max($company['status_levels'])]]["value"]; ?></option>
                <?php }} ?>
            </select>
        <?php if($path_elements[0] == 'control') { ?>
            <select name="status">
               <option value="blank_status" selected disabled>Status</option>
                <?php foreach ($status_levels as $status_level) { ?>
                    <option value = <?php echo $status_level['code']; ?>><?php echo $status_level['code']." - ".$status_level['value']; ?></option>
                <?php } ?>
            </select>
            <input type="submit" value="Status" name="submit" class="scw-button" style="font-size: 12px; height: 20px;">
            <?php } ?>
      <?php } else { /*?>
          <select name="company">
              <option value="<?php echo $common_pool['name']; ?>"><?php echo $common_pool['value']; ?></option>
          </select>
      <?php } */ } ?>


    </form>

<?php } ?>
