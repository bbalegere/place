<?php
$gd = 0;
    if(isset($_POST['submit']) && $user_logged_in == 1) {
      header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/".$_POST['company']); die();
    }
    elseif($user_logged_in == 1 && $num_path_elements == 2) {
      if($base_user_list[$_SESSION['user']]['role'] == 'company') {
        header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/".$base_user_list[$_SESSION['user']]['company']);
      }
    }
    elseif ($user_logged_in == 1 && $num_path_elements == 4 && $path_elements[3] == 'gd') {
      $gd = 1;
    }
    elseif ($user_logged_in == 1 && $num_path_elements == 4 && $path_elements[3] == 'gd_panels') {
      $gd = 2;
    }
    elseif($user_logged_in == 1 && $num_path_elements >= 2) {
      if($base_user_list[$_SESSION['user']]['role'] == 'company' && $path_elements[2] != $base_user_list[$_SESSION['user']]['company']) {
        header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/".$base_user_list[$_SESSION['user']]['company']);
      }
    }

    if($gd == 1) {
      require_once $views_path.'view_cst_gd.php';
    }
    elseif($gd == 2 && $base_user_list[$_SESSION['user']]['role'] == 'control') {
      require_once $views_path.'view_cst_gd_panels.php';
    }
    else {
      if($path_elements[0] == 'control') {
        require_once $views_path.'view_cst_in_control.php';
      } else {
        require_once $views_path.'view_cst_in.php';
      }
    }
?>
