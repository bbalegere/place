<table class="company-list-table">
    <thead>
        <tr>
            <th>Company Code</th>
            <th>Company Name</th>
            <th>Company Status</th>
            <th>Company Room</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($companyList as $key => $company) {
            ?>
            <tr>
                <td><?php echo $company["company"] ?></td>
                <td><?php echo $company["company_name"] ?></td>
                <td><?php
                    echo isset($company_status_levels[$company["company_status"]]) ? $company_status_levels[$company["company_status"]] : "Unknown";
                ?></td>
                <td><?php echo $company["company_room"] ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

