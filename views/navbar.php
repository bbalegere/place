<?php

$company_status = '';
  if($num_path_elements > 2 && isset($company_list[$path_elements[2]]) && ($path_elements[1] == 'cst' || $path_elements[1] == 'cstv')) {
    switch ($company_list[$path_elements[2]]['company_status']) {
      case 0:
        $company_status = 'on';
        break;

      case 1:
        $company_status = 'paused';
        break;

      case 2:
        $company_status = 'stopped';
        break;

      case 3:
        $company_status = 'exited';
        break;

      default:
        $company_status = '';
        break;
    }
      if ($path_elements[1] == 'pref') {

      }
    if(isset($company_list[$path_elements[2]]) && ($path_elements[0] == 'control' || $path_elements[0] == 'placecom')) {
      $form_submitted = 0;
      $current_company = R::load($tables['company_list']['name'],$company_list[$path_elements[2]]['id']);

      if(isset($_POST['start_status'])) {
        $current_company['company_status'] = 0;
        $form_submitted = 1;
      } elseif (isset($_POST['pause_status'])) {
        $current_company['company_status'] = 1;
        $form_submitted = 1;
      } elseif (isset($_POST['stop_status'])) {
        $current_company['company_status'] = 2;
        $form_submitted = 1;
      } elseif (isset($_POST['exit_status'])) {
        $current_company['company_status'] = 3;
        $form_submitted = 1;
      }

      if(isset($_POST['freeflow_on'])) {
        $current_company['freeflow'] = 0;
        $form_submitted = 1;
      } elseif(isset($_POST['freeflow_off'])) {
        $current_company['freeflow'] = 1;
        $form_submitted = 1;
      }

        if(isset($_POST['modify_panel'])) {
            if(is_numeric($_POST['panels'])){
                $current_company['panels'] = $_POST['panels'];
                $form_submitted = 1;
            } else{
                echo '<script language="javascript">';
                echo 'alert("Please enter a number")';
                echo '</script>';
            }

        }

      if($form_submitted == 1) {
        R::store($current_company);
        header("Location: ".$request_uri_for_form);
        die();
      }
    }
   
  }

 ?>

<nav class="<?php echo $company_status; ?>">
  <div class="nav-wrapper">
    <div class="col s12 display-wrapper">
        <?php
        if(isset($_SESSION['user'])) { ?>
        <form action="<?php echo $request_uri_for_logout; ?>" method="post" accept-charset="UTF-8" name="logout" class="logout_navBar">
            <input type="submit" value="Logout" name="logout" class="logout_nav_button reg-elements reg-button">
            <input type="text" value="<?php echo $_SESSION['user']; ?>" name="username" class="logout_nav_user reg-elements username login-elements" readonly>

        </form>
        <?php } ?>
            <a href=<?php echo "\"".$server_address.$site_address."\""; ?> class="breadcrumb loginout">&#127968;</a>
      <?php if(isset($_SESSION['user'])) { ?>
            <a href=<?php echo "\"".$server_address.$site_address."user\""; ?> class="breadcrumb loginout">&#128272;</a>
      <?php }	else { ?>
            <a href=<?php echo "\"".$server_address.$site_address."user\""; ?> class="breadcrumb loginout">&#128275;</a>
      <?php } ?>

        <?php
        if(($num_path_elements > 2) && ($path_elements[1] == 'cst' || $path_elements[1] == 'cstv') && isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['type'] == 'g') {
          if($path_elements[0] != 'control') { ?>
            <a href=<?php echo "\"".$server_address.$site_address.$path_elements[0]."/".$path_elements[1]."/".$path_elements[2]."/gd\""; ?> class="breadcrumb gd_switch">gd</a>
        <?php }
            if($path_elements[0] == 'control') { ?>
              <a href=<?php echo "\"".$server_address.$site_address.$path_elements[0]."/".$path_elements[1]."/".$path_elements[2]."/gd_panels\""; ?> class="breadcrumb gd_switch">gd</a>
            <?php }
              }
            if (isset($path_elements[1])) {
              if(($path_elements[1] == 'cst' || $path_elements[1] == 'cstv' || $path_elements[1] == 'sst') && $num_path_elements > 2) { } else {?>
          <a href=<?php echo "\"".$server_address.$site_address.$path_elements[0]."/".$path_elements[1]."\""; ?> class="breadcrumb"><?php echo $path_elements[1]; ?></a>
      <?php }
                
            
            }	
        
        
        ?>
      <?php $student_selected=0;
        $student_name="";
        if (isset($path_elements[2])) { ?>
          <a href=<?php echo "\"".$server_address.$site_address.$path_elements[0]."/".$path_elements[1]."/".$path_elements[2]."\""; ?> class="breadcrumb">
            <?php
              if(isset($company_list[$path_elements[2]]) && ($path_elements[1] == 'cst' || $path_elements[1] == 'cstv')) {
                  /* Commented and replaced with the company code in the header to reduce the width */
                  //echo $company_list[$path_elements[2]]['company_name'];
                  echo $company_list[$path_elements[2]]['company_name'];
              } else {
                $student_selected = $path_elements[2];  
                echo $path_elements[2];
              }
            ?>
          </a>
      <?php }	?>
      <?php
        if($num_path_elements >= 2) {
          if($path_elements[1] == 'cst' || $path_elements[1] == 'cstv') {
            if(in_array($path_elements[1],$allowed_pages_list[$path_elements[0]]) && $navbar_dropdown_required == 1) { ?>
            <div class="navbar_dropdown">
              <form action="<?php echo $website_uri.$path_elements[0]."/".$path_elements[1]; ?>" method="post" accept-charset="UTF-8" name="company_tracker">
                <select name="company" class='navbar_dropdown_list'>
                  <option value="cp">Select</option>
                  <?php foreach ($company_list as $key => $company) {
                      if($company['company_status'] != 3) {
                    ?>
                      <option value="<?php echo $company['company']; ?>">
                        <?php echo $company['company_name']; ?>
                      </option>
                  <?php }} ?>
                </select>
                  <input type="submit" value="Submit" name="submit" class="navbar_submit">

              </form>

            </div>
          <?php
          }
          if($num_path_elements > 2) {
            if(array_key_exists($path_elements[2],$panels_running)) {
              $panels_running_display = $panels_running[$path_elements[2]]['panels_running'];
            } else {
              $panels_running_display = 0;
            }
            if(array_key_exists($path_elements[2],$queues_running)) {
              $queues_running_display = $queues_running[$path_elements[2]]['queues_running'];
            } else {
              $queues_running_display = 0;
            }
            if(isset($company_list[$path_elements[2]])) { ?>
            <div class="navbar_panel_count"> <?php echo $panels_running_display."/".$company_list[$path_elements[2]]['panels']; ?></div> <?php
            }
            if(isset($path_elements[0]) && $path_elements[0] == 'control') {
            $current_company_room = $current_company['room']; ?>
            <div class="navbar_panel_count"> <?php echo "Queue: ".$queues_running_display; ?></div>
            <div class="navbar_panel_count"> <?php echo "Room: ".$current_company_room; ?></div>    <?php
            }
            if(isset($student_dream_count) && isset($path_elements[0]) && $path_elements[0] == 'control') {
                foreach ($other_shortlists as $key => $stud) {
                    if (!isset($placed_student_list[$key]) && isset($student_data_dream[$key]) && $student_data_dream[$key] == 1) {
                        $student_dream_count += 1;
                    }
                }
                ?>

                <div class="navbar_panel_count" id="studentDreamCount"> <?php echo "Dream: ".$student_dream_count; ?></div><?php
            }
          }
             /* Display the Scheduling efficiency for GD Companies */
             if(isset($path_elements[2]) && $company_list[$path_elements[2]]['type'] == 'g')
             {
                         if(isset($in_company_people[$path_elements[2]])) {

                             $count =0;
                             foreach($student_list as $key => $student)
                             {
                                 if(array_key_exists($student['place_id'], $placed_student_list))
                                     $count++;
                             }

                             $purged_shortlist = sizeof($student_list_cst) - $count;

                             ?>
                             <div class="navbar_panel_count">
                                 <?php
                             echo "GD Efficiency: " . $in_company_people[$path_elements[2]]['in_company'] . "/" . $in_company_people[$path_elements[2]]['company_shortlists']; ?></div>
                             <?php
                         }
             }
        }
          elseif($path_elements[1] == 'sst') {
             if(in_array($path_elements[1],$allowed_pages_list[$path_elements[0]])){ ?>
              <div class="navbar_dropdown">
                <form action="<?php echo $website_uri.$path_elements[0]."/".$path_elements[1]; ?>" method="post" accept-charset="UTF-8" name="company_tracker">
                    <input list="students" name="place_id" class="sst_list">
                    <datalist id="students">
                        <option value="none">Select</option>
                        <?php
                        foreach ($base_student_list as $key => $student) {
                            if($student_selected == $student['id'])
                            {
                                $student_name=$student['student_name'];
                            }

                            ?>
                            <option value="<?php echo $student['id']; ?>">
                                <?php
                                echo $student['id']." - ".ucwords(strtolower($student['student_name'])); ?>
                            </option>
                        <?php } ?>
                    </datalist>
                  <input type="submit" value="Submit" name="submit" class="navbar_submit">
                 
                </form>
              </div>
         <div class="navbar_panel_count"><?php if($student_name != ""){echo $student_name;} ?></div>
          <?php
            }
          }
          elseif ($path_elements[1] == 'mts') {
            if(in_array($path_elements[1],$allowed_pages_list[$path_elements[0]])) { ?>
              <div class="navbar_dropdown">
                <form action="<?php echo $website_uri.$path_elements[0]."/".$path_elements[1]; ?>" method="post" accept-charset="UTF-8" name="company_tracker">
                  <select name="mobile_tracker" class='navbar_dropdown_list'>
                    <option value="-1">Range</option>
                    <?php for ($i=0; $i <= 8; $i++) { ?>
                      <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php } ?>
                  </select>
                  <input type="submit" value="Submit" name="submit" class="navbar_submit">
                </form>
              </div>
        <?php }
          }


            if($num_path_elements > 2 && ($path_elements[1] == 'cst' || $path_elements[1] == 'cstv') && $path_elements[0] == 'control') {
              if(isset($company_tracker_list[$path_elements[2]])) {
              $company_trackers = $company_tracker_list[$path_elements[2]];
              foreach ($company_trackers as $key => $company_tracker) { ?>
                <span class='breadcrumb'><?php echo $company_tracker['username']; ?></span>
              <?php } }
            }

        } ?>
        <?php if($num_path_elements > 2 && ($path_elements[0] == 'control' || $path_elements[0] == 'placecom') && $path_elements[1] == 'cst') { ?>
        <div class='navbar_right'>
                <form action="<?php echo $request_uri_for_form ?>" method="post" accept-charset="UTF-8" name="modify_panel_form" class="modify-panel-cst-form">
                    <input type="text" name="panels" class="modify-panel-cst-input">
                    <input type="submit" value="Modify" name="modify_panel" class="navbar_submit">
                </form>
                <form action="<?php echo $request_uri_for_form ?>" method="post" accept-charset="UTF-8" name="company_status_change" class="company_status_change">
                  <input type="submit" value="Start" name="start_status" class="company_status_button navbar_submit on">
                  <input type="submit" value="Pause" name="pause_status" class="company_status_button navbar_submit paused">
                  <input type="submit" value="Stop" name="stop_status" class="company_status_button navbar_submit stopped">
                  <input type="submit" value="Exit" name="exit_status" class="company_status_button navbar_submit exited">
                </form>
                <form action="<?php echo $request_uri_for_form ?>" method="post" accept-charset="UTF-8" name="freeflow_change" class="company_status_change">
                  <?php if (isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['freeflow'] == 1): ?>
                      <input type="submit" value="Free: ON" name="freeflow_on" class="company_status_button navbar_submit">
                  <?php endif; ?>
                  <?php if (isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['freeflow'] == 0): ?>
                      <input type="submit" value="Free: OFF" name="freeflow_off" class="company_status_button navbar_submit">
                  <?php endif; ?>

                </form>
              </div>
        <?php    } ?>
    </div>
  </div>
</nav>