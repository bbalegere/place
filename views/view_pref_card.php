<?php
/**
 * Created by PhpStorm.
 * User: srividyag
 * Date: 11/01/18
 * Time: 12:48 AM
 */


if ($student == null) {
    ?>

    <div class="card pref-card" id="<?php echo $disp_place_id; ?>">

        <div class="card-details status-0">
            <div class="card-details-left">
                <div class="place_id"><?php echo $disp_place_id; ?></div>
                <div class="panel_id"><?php
                    echo 0;
                    ?></div>
                <div class="time_level"><?php echo 'NA'; ?></div>
            </div>
            <div class="card-details-right">
                <div class="queue_level">-</div>
                <div class="company">No Shortlists
                    <br/>
                    <span class="company_display_status"></span>

                </div>

                <div class="queue_level">-</div>
            </div>

        </div>
    </div>

<?php } else {


    require $views_path . 'view_card_basic_data.php';

    ?>


    <div class="card pref-card" id="<?php echo $student['place_id']; ?>">


        <div class="card-details status-<?php echo $student_max_status; ?>">

            <div class="card-details-left">
                <div class="place_id"><?php echo $student['place_id']; ?></div>
                <div class="panel_id"><?php
                    echo $card_panel_counts;
                    ?></div>
                <div class="time_level"><?php echo $time_to_display; ?></div>
            </div>

            <div class="card-details-right">


                <div class="queue_level">Queue: <?php echo $queues_running_grid_disp; ?></div>

                <?php
                if (isset($company_list[$temp_company_name])) {
                    $statusLevel = $company_status_levels[$company_list[$temp_company_name]['company_status']];
                } else {
                    $statusLevel = "";
                }

                ?>
                <div class="company"><span class="company_display">

        <?php
        if (isset($display_company)) {
            echo $display_company;
        } elseif (isset($company_list[$temp_company_name])) {
            echo $company_list[$temp_company_name]['company_name'];
        }
        ?>
       </span>
                    <br/>
                    <span class="company_display_status"><?php echo $statusLevel; ?></span>
                </div>
                <div class="queue_level"><?php echo $status_levels[$status_levels_codes[$student_max_status]]["value"]; ?></div>

            </div>


        </div>
</div>

<?php } ?>




