<script lang="javascript">
function toggle(source,rxpy) {
  checkboxes = document.getElementsByClassName(rxpy);
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
</script>

<?php
$right_side_open = 0;
$right_side_close = 0;
  if(isset($student_list_cst_gd) && isset($company_list[$path_elements[2]]) && $company_list[$path_elements[2]]['type'] == 'g') {
    foreach ($student_list_cst_gd as $key1 => $student_list_cst_gd_1) {
      foreach ($student_list_cst_gd_1 as $key2 => $student_list_cst_gd_2) {
        $i = $key1;
        $j = $key2;
        if(($i == 0 && $j == 0) || ($i != 0 && $j != 0)) {
          if($i == 0 && $j == 0) {
            echo "<div class = 'gd_unpanelled'>";
          } elseif($right_side_open == 0) {
            echo "<div class = 'gd_panelled'>";
            $right_side_open = 1;
            ksort($student_list_cst_gd_2);
          }
        ?>
        <div class='ul_list_index gd_panel_box status-<?php echo $gd_panels_min_status[$i][$j]; ?>'>
          <div class='gd_panel_box_header'>R<?php echo $i; ?>-P<?php echo $j." - Min ".$gd_panels_min_status[$i][$j]." ".$status_levels[$status_levels_codes[$gd_panels_min_status[$i][$j]]]['value']; ?></div>
          <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo 'gd-'.$i.'-'.$j; ?>">
            <div class='gd_box_rxpy_bar'>
              <?php
              $gd_panels_next_status = $status_levels[$status_levels[$status_levels_codes[$gd_panels_min_status[$i][$j]]]['button_name']]['code'];
                if(in_array($gd_panels_next_status,$communicate_permissions[$path_elements[0]]) && $gd_panels_next_status < $status_levels['sent']['code'] && $gd_panels_next_status > $status_levels['arrived']['code']) {
               ?>
              <input type='hidden' class='gd_rp' value='<?php echo $gd_panels_next_status; ?>' name='gd_status' readonly>
              <input type='hidden' class='gd_rp' value='<?php echo $path_elements[2]; ?>' name='company' readonly>
              <input type='text' class='gd_rp' style='width: 100px; font-size: 16px;' value='<?php echo $status_levels[$status_levels_codes[$gd_panels_min_status[$i][$j]]]['button_value']; ?>' name='gd_status_text' readonly>
              <input type='submit' class='navbar_submit gd_rp_submit' value="Status" name="set_gd"><br>
              <?php } ?>
            </div>
        <?php

        foreach ($student_list_cst_gd_2 as $key => $student) {
          //print_r($student);
          if($student['gd_round'] == $i && $student['gd_panel'] == $j) {
            $student_max_status = max($student['status_levels']);
            if ($student_max_status == $status_levels['exit']['code']
             || $student_max_status == $status_levels['requestaway']['code']
             || $student_max_status == $status_levels['sendaway']['code']) {
                $next_company = $common_pool['name'];
                require $models_path.'model_next_company.php';
            } elseif ($student_max_status == $status_levels['sent']['code']) {
                $next_company = $student['next_company'];
            }
            require $views_path.'view_card_decisions.php';
            echo "<div class='status-".$student_max_status."'><label><input type='checkbox' class='gd_checkbox ".$i.$j."' name='gd_students[]' value='".$student['id']."' >"."<a target='_blank' href='".$website_uri.$path_elements[0].'/sst/'.$student['place_id']."'>".$student['place_id']." ".$student['student_name']." ".$student_max_status."</a></label></div>";
          }
        }
        ?>
          <label><input type="checkbox" class='gd_checkbox' onClick='toggle(this,<?php echo $i.$j; ?>)'>Toggle All</label>
          </form>
        </div>
        <?php
        if($i == 0 && $j == 0) {
          echo "</div>";
        }
      }
      }
    }
    if($right_side_open == 1) {
      echo "</div>";
    }
  }


 ?>
