<?php
$card_count_display = 0;
if(isset($student_list_ncw)) {
  $card_count_display = sizeof($student_list_ncw);
}
?>
    <div class='card-list card-list-<?php echo $status_levels['requested']['code']; ?> <?php if($path_elements[0] == 'control') { echo "control-layout-ncw"; } ?>'>
      <div class='card-list-header'><?php echo $status_levels['requested']['text'].' '.$card_count_display; ?></div>
      <?php
      if(!empty($student_list_ncw)) {
        foreach ($student_list_ncw as $key => $student) {
          if (!array_key_exists($student['place_id'],$placed_student_list)) {
            require $views_path.'view_student_card.php';
          }
        }
      } else { echo "<div class='ncw_empty_list'>No Pending In-Requests!</div>"; } ?>
    </div>

    <?php
    $card_count_display = 0;
    if(isset($student_list_ncwa)) {
      $card_count_display = sizeof($student_list_ncwa);
    }
    ?>

    <div class='card-list card-list-<?php echo $status_levels['requestaway']['code']; ?> <?php if($path_elements[0] == 'control') { echo "control-layout-ncw"; } ?>'>
      <div class='card-list-header'><?php echo $status_levels['requestaway']['text'].' '.$card_count_display; ?></div>
      <?php
      if(!empty($student_list_ncwa)) {
        foreach ($student_list_ncwa as $key => $student) {
          if (!array_key_exists($student['place_id'],$placed_student_list)) {
            $next_company = $common_pool['name'];
            require $models_path.'model_next_company.php';
            require $views_path.'view_student_card.php';
          }
        }
      } else { echo "<div class='ncw_empty_list'>No Pending Out-Requests!</div>"; } ?>
    </div>


    <?php
    $card_count_display = 0;
    if(isset($student_list_ita1)) {
      $card_count_display = sizeof($student_list_ita1);
    }
    ?>
    <div class='card-list card-list-<?php echo $status_levels['arrived']['code']; ?> <?php if($path_elements[0] == 'control') { echo "control-layout-ita"; } ?>'>
      <div class='card-list-header'><?php echo $status_levels['arrived']['text'].' '.$card_count_display; ?></div>
      <?php
        if(!empty($student_list_ita1)) {
          foreach ($student_list_ita1 as $key => $student) {
            if (!array_key_exists($student['place_id'],$placed_student_list)) {
              require $views_path.'view_student_card.php';
            }
          }
        } else { echo "<div class='ncw_empty_list'>No Arrivals!</div>"; }
      ?>
    </div>

    <?php
    $card_count_display = 0;
    if(isset($student_list_ita2)) {
      $card_count_display = sizeof($student_list_ita2);
    }
    ?>
    <div class='card-list card-list-<?php echo $status_levels['inside']['code']; ?> <?php if($path_elements[0] == 'control') { echo "control-layout-ita"; } ?>'>
      <div class='card-list-header'><?php echo $status_levels['inside']['text'].' '.$card_count_display; ?></div>
      <?php
      if(!empty($student_list_ita2)) {
        foreach ($student_list_ita2 as $key => $student) {
          if (!array_key_exists($student['place_id'],$placed_student_list)) {
            require $views_path.'view_student_card.php';
          }
        }
      } else { echo "<div class='ncw_empty_list'>No Panels Running!</div>"; }
      ?>
    </div>


        <?php
        $card_count_display = 0;
        if(isset($student_list_ita3)) {
          $card_count_display = sizeof($student_list_ita3);
        }
        ?>
    <div class='card-list card-list-<?php echo $status_levels['exit']['code']; ?> <?php if($path_elements[0] == 'control') { echo "control-layout-ita"; } ?>'>
      <div class='card-list-header'><?php echo $status_levels['exit']['text'].' '.$card_count_display; ?></div>
      <?php
      if(!empty($student_list_ita3)) {
        foreach ($student_list_ita3 as $key => $student) {
          if (!array_key_exists($student['place_id'],$placed_student_list)) {
            $next_company = $common_pool['name'];
            require $models_path.'model_next_company.php';
            require $views_path.'view_student_card.php';
          }
        }
      } else { echo "<div class='ncw_empty_list'>No One Exited!</div>"; }
      ?>
    </div>
