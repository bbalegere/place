<!DOCTYPE html>
<html lang="en">
<head>

    <title>
        <?php
        if ($num_path_elements > 2) {
            if (($path_elements[1] == 'cst' || $path_elements[1] == 'cstv') && isset($company_list[$path_elements[2]])) {
                echo $company_list[$path_elements[2]]['company_name'];
            } else {
                echo $path_elements[2];
            }
        } elseif ($num_path_elements > 0) {
            echo $path_elements[$num_path_elements - 1];
        } else {
            echo "PLACE";
        }
        ?>
    </title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="theme-color" content="#3b5999">
    <?php
    if (isset($path_elements[1])) {
        if ($path_elements[1] == 'ncw') {
            echo "<meta http-equiv='refresh' content='5'>";
        } elseif ($path_elements[1] == 'ita') {
            echo "<meta http-equiv='refresh' content='15'>";
        } elseif ($path_elements[1] == 'cpp') {
            echo "<meta http-equiv='refresh' content='30'>";
        } elseif ($path_elements[1] == 'lpc') {
            echo "<meta http-equiv='refresh' content='30'>";
        } elseif ($path_elements[1] == 'ica') {
            echo "<meta http-equiv='refresh' content='10'>";
        }
    }
    ?>
    <link rel="icon" sizes="192x192" href="<?php echo $server_address; ?>/iimblogo.png">

    <style>
        <?php
            //require $site_path."views/scripts/css/materialize.min.css";
            $mobile_client = 0;
            $css_buffer = file_get_contents($site_path."views/scripts/css/style.css");
            $css_buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css_buffer);
            $css_buffer = str_replace(': ', ':', $css_buffer);
            $css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css_buffer);
            echo $css;

            if(stristr($_SERVER['HTTP_USER_AGENT'], "Android") || stristr($_SERVER['HTTP_USER_AGENT'], "iPhone") || stristr($_SERVER['HTTP_USER_AGENT'], "Mobile")){
                $mobile_client = 1;
                $css_buffer = file_get_contents($site_path."views/scripts/css/mobile-style.css");
                $css_buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $css_buffer);
                $css_buffer = str_replace(': ', ':', $css_buffer);
                $css = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css_buffer);
                echo $css;
            }
            //require $site_path."views/scripts/css/style.css";
         ?>
    </style>
    <?php if (isset($path_elements[0]) && $path_elements[0] == 'company') { ?>
        <script lang="javascript">
            setInterval(function () {
                if (!alert('Please click on ok/close to refresh the page')) {
                    window.location.reload();
                }
            }, 45000);
        </script>
    <?php } ?>
</head>

<body>
<?php require_once $site_path . "views/navbar.php"; ?>
<div class="container">
    <?php
    if (isset($red_alert) && isset($path_elements[0])) {
        if ($path_elements[0] == 'control') {
            foreach ($red_alert as $key => $alert_message) {
                echo "<h1>" . $alert_message . "</h1>";
            }
        }
    }

    ?>
