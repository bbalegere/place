<?php
  $sys_buttons = array(
    'optimizedb' => 'Optimize DB',
    'optimizelog' => 'Optimize Log',
    'clearlog' => 'Clear Log',
    'clearuserlog' => 'Logout All Users',
    'clearbackuplog' => 'Clear Backup Log',
    'clearmobilepref' => 'Clear Backloop Channel',
    'resetcompanytrackers' => 'Reset Company Trackers',
  )

 ?>
 <?php if ($path_elements[0] == 'admin'): ?>
   <div class='ul_list_index sys_box'>
     <?php foreach ($sys_buttons as $sys_buttons_code => $sys_buttons_value): ?>
       <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="<?php echo $sys_buttons_code; ?>" style='display:inline-block;'>
         <input type="submit" class='navbar_submit sys_submit' value="<?php echo $sys_buttons_value; ?>" name="<?php echo $sys_buttons_code; ?>">
       </form>
     <?php endforeach; ?>
   </div>
 <?php endif; ?>

 <div class='ul_list_index sys_box'>
 <h3>System Health</h3>
   <table class='sys_table' width='100vw'>
     <th>L0</th><th>Proc Time</th><th>Visits</th>

     <?php foreach ($sys_stats_mini as $sys_key1 => $sys_stat): ?>
       <tr><?php foreach ($sys_stat as $sys_key2 => $stat): ?>
         <td><?php if($sys_key2 != 'processing_time') { echo $stat; }
                   else { echo round($stat*1000)." ms"; } ?></td>
       <?php endforeach; ?></tr>
     <?php endforeach; ?></table>
 </div>

 <div class='ul_list_index sys_box'>
 <h3>System Health</h3>
   <table class='sys_table' width='100vw'>
     <th>L0</th><th>L1</th><th>Proc Time</th><th>Visits</th>

     <?php foreach ($sys_stats_compact as $sys_key1 => $sys_stat): ?>
       <tr><?php foreach ($sys_stat as $sys_key2 => $stat): ?>
         <td><?php if($sys_key2 != 'processing_time') { echo $stat; }
                   else { echo round($stat*1000)." ms"; } ?></td>
       <?php endforeach; ?></tr>
     <?php endforeach; ?></table>
 </div>

 <div class='ul_list_index sys_box'>
 <h3>Users</h3>
   <table class='sys_table' width='100vw'>
     <th>Role</th><th>User</th><th>IP</th><th>Proc Time</th><th>Visits</th>

     <?php foreach ($sys_stats_users as $sys_key1 => $sys_stat): ?>
       <tr><?php foreach ($sys_stat as $sys_key2 => $stat): ?>
         <td><?php
           if($sys_key2 == 'processing_time') { echo round($stat*1000)." ms"; }
           elseif($sys_key2 == 'http_user_agent') {
             if(stristr($stat, "Android") || stristr($stat, "iPhone") || stristr($stat, "Mobile")) {
               echo "Mobile";
             } else {
               echo "PC";
             }
           }
           else { echo $stat; }
         ?></td>
       <?php endforeach; ?></tr>
     <?php endforeach; ?></table>
 </div>
