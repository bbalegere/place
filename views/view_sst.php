<?php
if($path_elements[0] == 'company' && !isset($company_list[$base_user_list[$_SESSION['user']]['company']])) {
  echo "<div class='ul_list_index'> This company is not available </div>";
}
elseif($settings['ALLOW_SST_FOR_INTERVIEW'] == 0 &&
      ($path_elements[0] == 'company'
    && isset($company_list[$base_user_list[$_SESSION['user']]['company']])
    && $company_list[$base_user_list[$_SESSION['user']]['company']]['type'] == 'i')) {
  echo "<div class='ul_list_index'> This function is currently not available for you. Please contact a floor coordinator.</div>";
}
else {
  if(isset($student_list_sst)) {
    if(isset($_POST['submit'])) {
      header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/".$_POST['place_id']);
    }
    if (isset($_POST['submit']) || array_key_exists(2,$path_elements)) {
      echo "<div class='ul_list_index'>cp";
      foreach ($student_list_sst as $key => $student_tracer) {
        echo " &rArr; ".$student_tracer['company'];
      }
      echo "</div>";
      echo "<div class='ul_list_index'><table>";
      echo "<thead>"
          ."<th>Prev</th>"
          ."<th>Current</th>"
          ."<th>Next</th>"
          ."<th>Round</th>"
          ."<th>Panel</th>";

      for ($i=$min_status+1; $i <= $max_status ; $i++) {
        echo "<th>".$status_levels[$status_levels_codes[$i]]['value']."</th>";
      }

      echo "</thead>";
      foreach ($student_list_sst as $key => $student_tracer) {
        if($student_tracer['previous_company'] == '0' || $student_tracer['previous_company'] == 'cp') { $student_tracer['previous_company'] = $common_pool['name']; $previous_company = $common_pool['value']; }
        if($student_tracer['next_company'] == '0' || $student_tracer['next_company'] == 'cp')     { $student_tracer['next_company'] = $common_pool['name']; $next_company = $common_pool['value'];}

        if(isset($company_list[$student_tracer['previous_company']])) { $previous_company = $company_list[$student_tracer['previous_company']]['company_name']; }
        if(isset($company_list[$student_tracer['next_company']])) { $next_company = $company_list[$student_tracer['next_company']]['company_name']; }
        if(isset($company_list[$student_tracer['company']])) { $current_company = $company_list[$student_tracer['company']]['company_name']; }

        echo "<tr>"
        ."<td>".$previous_company."</td>"
        ."<td>".$current_company."</td>"
        ."<td>".$next_company."</td>"
        ."<td>".$student_tracer['gd_round']."</td>"
        ."<td>".$student_tracer['gd_panel']."</td>";
        for ($i=$min_status; $i < $max_status; $i++) {
          if($student_tracer['status_timestamps'][$i] != 0) {
            $status_time = date("H:i", $student_tracer['status_timestamps'][$i]);
          } else {
            $status_time = "-";
          }
          echo "<td>".$status_time."</td>";
        }
        echo "</tr>";
      }
      echo "</table></div>";
    }
  }
}
  /*for ($i=$max_status; $i > $min_status; $i--) {
    if(in_array($i,$card_visibility_permissions[$path_elements[0]])){
      $student = $student_tracer;
      $student['status_levels'] = array($i);
      $student['status_timestamps'] = array($student_tracer['status_timestamps'][$i-1]);
      print_r($student);
      require $views_path.'view_student_card.php';
    }
  }
$php_timestamp_date = date("H:i", $php_timestamp);
  */
?>
