<?php
if($path_elements[0] == 'backloop' || $path_elements[0] == 'control') {
?>

<div class='ul_list_index'>
  <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="cpp">
    <select name="place_id" class='gd_status_dropdown'>
      <option selected disabled>Student</option>
      <?php foreach ($base_student_list as $key => $base_student): ?>
        <option value='<?php echo $base_student['id']; ?>'><?php echo $base_student['id'].' '.$base_student['student_name']; ?></option>
      <?php endforeach; ?>
    </select>

    <?php for ($i=1; $i <= 3; $i++) { ?>
      <select name="choice<?php echo $i; ?>" class='gd_status_dropdown'>
        <option selected disabled>Choice <?php echo $i; ?></option>
        <?php foreach ($company_list as $key => $company): ?>
          <?php if ($company['company_status'] < 3): ?>
            <option value='<?php echo $company['company']; ?>'><?php echo $company['company_name']; ?></option>
          <?php endif; ?>
        <?php endforeach; ?>
      </select>
    <?php } ?>
      <select name="role" class='gd_status_dropdown'>
        <option selected disabled>Role</option>
        <option value="mobile">Mobile</option>
        <option value="floor">Floor</option>
        <option value="any">Both</option>
      </select>
    <input type="submit" class='navbar_submit gd_rp_submit' value="Ask" name="ask" style='margin-left: 10px;'>
  </form>
</div>
<?php } ?>

<div class='ul_list_index'>
  <h3>Preference Requests (in last 2 hours)</h3>
  <table>
    <thead><th>Asked to</th><th>ID</th><th>Location</th><th>Student</th><th>Pref 1</th><th>Pref 2</th><th>Pref 3</th><th>Chosen</th><th>Time</th>
      <?php if ($path_elements[0] == 'backloop' || $path_elements[0] == 'control'): ?>
        <th>Delete</th>
      <?php endif; ?>
    </thead>
  <?php $counter = 1;
    foreach ($mobile_requests as $key => $mobile_request): ?>
    <tr>
      <td>
        <?php
          if($mobile_request['role']=='mobile') { echo 'Mobile'; }
          elseif($mobile_request['role']=='floor') { echo 'Floor'; }
          elseif($mobile_request['role']=='any') { echo 'Open'; }
        ?>
    </td>
      <td><?php echo $mobile_request['place_id']; ?></td>
      <td><?php
            $cpp_current_location = $base_student_list[$mobile_request['place_id']]['current_location'];
            if($cpp_current_location == $common_pool['name']) {
              $cpp_current_location_name = $common_pool['value'];
            } elseif(isset($company_list[$cpp_current_location])) {
              $cpp_current_location_name = $company_list[$cpp_current_location]['company_name'];
            } else {
              $cpp_current_location_name = "Unknown";
            }
            echo $cpp_current_location_name; ?></td>
      <td><?php echo $base_student_list[$mobile_request['place_id']]['student_name']; ?></td>
      <?php for($i = 1; $i <= 3; $i++) {
        if(is_null($mobile_request['choice'.$i])) {
          $display_company = '---';
        } elseif(isset($company_list[$mobile_request['choice'.$i]])) {
          $display_company = $company_list[$mobile_request['choice'.$i]]['company_name'];
        } else {
          $display_company = '---';
        }?>
        <td><?php echo $display_company; ?></td>
      <?php } ?>
      <td>
        <?php if($mobile_request['choice'] == '---' || is_null($mobile_request['choice'])) { echo "---"; }
              elseif($mobile_request['choice'] != '0' && isset($company_list[$mobile_request['choice']])) {
                    echo $company_list[$mobile_request['choice']]['company_name'];
              }
              elseif($path_elements[0] == $mobile_request['role'] || ($mobile_request['role'] == 'any' && ( $path_elements[0] == 'mobile' || $path_elements[0] == 'floor' ))) { ?>
                <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="cpp">
                  <input type='hidden' name='id' value='<?php echo $mobile_request['id']; ?>'>
                  <select name="choice" class='gd_status_dropdown'>
                    <option selected disabled>Choose</option>
                    <?php for ($i=1; $i <= 3; $i++) {
                    if(isset($company_list[$mobile_request['choice'.$i]])) { ?>
                      <option value='<?php echo $mobile_request['choice'.$i]; ?>'><?php echo $company_list[$mobile_request['choice'.$i]]['company_name']; ?></option>
                    <?php }} ?>
                    <option value='---'>NoTA</option>
                  </select>
                  <input type="submit" class='navbar_submit gd_rp_submit' value="Tell" name="tell" style='margin-left: 10px;'>
                </form>
          <?php } else {
              echo "";
          }
            ?>
      </td>
      <td><?php echo date("H:i",$mobile_request['time_asked']); ?></td>
      <?php if ($path_elements[0] == 'backloop' || $path_elements[0] == 'control'): ?>
        <td>
          <form action="<?php echo $request_uri_for_form; ?>" method="post" accept-charset="UTF-8" name="cpp">
            <input type='hidden' name='id' value='<?php echo $mobile_request['id']; ?>'>
            <input type="submit" class='navbar_submit gd_rp_submit' value="Delete" name="delete">
          </form>
        </td>
      <?php endif; ?>
    </tr>

  <?php endforeach; ?>
  </table>
</div>
