<?php

$series_start = 100;
$series_end = 999;

if(isset($_POST['submit']) && $user_logged_in == 1) {
  header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/".$_POST['mobile_tracker']); die();
}
elseif($num_path_elements == 2 && $user_logged_in == 1) {
  $series_start = 100;
  $series_end = 999;
}
elseif($num_path_elements >= 2 && $user_logged_in == 1) {
//  if($base_user_list[$_SESSION['user']]['role'] == 'mobile') {
    switch ($path_elements[2]) {
      case -1:
        header("Location: ".$website_uri.$path_elements[0]."/".$path_elements[1]."/0"); die();
      case 0:
        break;

      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
        $series_start = $path_elements[2]*100;
        $series_end = $path_elements[2]*100 + 100;
        break;

      case 8:
        $series_start = $path_elements[2]*100;
        $series_end = $path_elements[2]*100 + 200;

      default:
        break;
    }
//  }
}
?>

<?php
$card_count_display = 0;
if(isset($student_list_mts1)) {
  $card_count_display = sizeof($student_list_mts1);
}
?>

    <div class='card-list card-list-<?php echo $status_levels['scheduled']['code']; ?> <?php echo "mobile-layout-mts"; ?>'>
      <div class='card-list-header'><?php echo $status_levels['scheduled']['text'].' '.$card_count_display; ?></div>
      <?php
      if(isset($student_list_mts1)) {
          //echo print_r($student_list_mts1);
          $mts_company = "cp";
          
        foreach ($student_list_mts1 as $key => $student) {
            if($student['company'] != $mts_company)
            {
                if($company_list[$student['company']]['company_status']==2) 
                {
                ?><div class='card-list-header-company company_stop'><?php echo $student['company']; ?></div><?php 
                }
                else if($company_list[$student['company']]['company_status']==3){ 
                ?><div class='card-list-header-company company_exit'><?php echo $student['company']; ?></div><?php 
                }
                else
                {
                ?><div class='card-list-header-company'><?php echo $student['company']; ?></div><?php   
                }
                $mts_company = $student['company'];
            }
            
          if ((!array_key_exists($student['place_id'],$placed_student_list))
          &&  ($base_student_list[$student['place_id']]['current_location'] == $common_pool['name'])) 
          {
            if($student['place_id'] >= $series_start && $student['place_id'] < $series_end) 
            {
              require $views_path.'view_student_card.php';
            }
          }
        }
      }
      // else { echo "Hurray! All clear here!"; }
      ?>
    </div>

    <?php
    $card_count_display = 0;
    if(isset($student_list_mts2)) {
      $card_count_display = sizeof($student_list_mts2);
    }
    ?>

    <div class='card-list card-list-<?php echo $status_levels['intransit']['code']; ?> <?php echo "mobile-layout-mts"; ?>'>
      <div class='card-list-header'>Sent from CP<?php echo ' '.$card_count_display; ?></div>
      <?php
      if(isset($student_list_mts2)) {
        foreach ($student_list_mts2 as $key => $student) {
          if ((!array_key_exists($student['place_id'],$placed_student_list))
          &&  ($base_student_list[$student['place_id']]['current_location'] == $common_pool['name'])) {
            if($student['place_id'] >= $series_start && $student['place_id'] < $series_end) {
              require $views_path.'view_student_card.php';
            }
          }
        }
      }
    
      ?>
    </div>
    <div class = 'ul_list_index mobile_history'>
      <h3>Sent from CP (in last 2 hours)</h3>
      <table>
        <thead><th>ID</th><th>Student</th><th>To</th><th>Time</th></thead>
        <?php
        if(isset($student_list_mts3)) {
          foreach ($student_list_mts3 as $key => $student) {
            if(time() - $student['status_timestamps'][$status_levels['intransit']['code']-1] < 7200) { ?>
            <tr>
              <td><?php echo $student['place_id']; ?></td>
              <td><?php echo $student['student_name']; ?></td>
              <td><?php
                if(isset($company_list[$student['company']])) {
                  echo $company_list[$student['company']]['company_name'];
                } else { echo "N/A"; }
              ?></td>
              <td><?php echo date("H:i", $student['status_timestamps'][$status_levels['intransit']['code']-1]); ?></td>
            </tr>
          <?php }}
        }
        ?>
      </table>
    </div>
