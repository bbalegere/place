<?php

  ob_start();
  session_start();

  date_default_timezone_set('Asia/Kolkata');
  $process_start_time = microtime(TRUE);
  $system_slash = DIRECTORY_SEPARATOR;
  $site_path = __DIR__.DIRECTORY_SEPARATOR;

  $site_address = '/place/';
  $script_address = '/app-scripts/';
  $namak = uniqid(mt_rand(), true);
  $models_path = $site_path.'models'.DIRECTORY_SEPARATOR;
  $views_path = $site_path.'views'.DIRECTORY_SEPARATOR;
  $controllers_path = $site_path.'controllers'.DIRECTORY_SEPARATOR;
  $data_path = $site_path.'data'.DIRECTORY_SEPARATOR;

  $site_address_count = strlen($site_address);

  if(isset($_SERVER['HTTPS'])) {
    $server_address = 'https://'.$_SERVER['SERVER_ADDR'];
  } else {
    $server_address = 'http://'.$_SERVER['SERVER_ADDR'];
  }

  if(isset($_SERVER['HTTPS'])) {
    $server_address = 'https://'.$_SERVER['SERVER_NAME'];
  } else {
    $server_address = 'http://'.$_SERVER['SERVER_NAME'];
  }

  $website_uri = $server_address.$site_address;
  $scripts_uri = $server_address.$script_address;

  if (strpos($_SERVER['REQUEST_URI'], "?")) {
    $request_uri_clean = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], "?"));
  } else {
    $request_uri_clean = $_SERVER['REQUEST_URI'];
  }

  $request_uri_clean_clean = substr($request_uri_clean,$site_address_count);
  $path_elements = explode("/", $request_uri_clean_clean);
  $num_path_elements = sizeof($path_elements);
  $request_uri_for_form = $server_address.$site_address.$request_uri_clean_clean;
  $request_uri_for_logout=$server_address.$site_address.'user';

  $ajax = 0;

  if($num_path_elements > 1) {
    if($path_elements[0] == "ajax") {
      $ajax = 1;
      $path_elements = array_slice($path_elements, 1);
    }
  }

  if(end($path_elements) == "") { array_pop($path_elements); }

  $num_path_elements = sizeof($path_elements);

  require_once $controllers_path.'sys_ver.php';
  require_once $controllers_path.'config.php';
  ob_end_flush();

  require_once $models_path.'log.php';
 ?>
