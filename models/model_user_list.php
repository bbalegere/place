<?php

  unset($base_user_list);
  $base_user_list = R::findAll($tables['users']['name']);

  foreach ($base_user_list as $oldkey => $user_row) {
    $base_user_list[$user_row['username']] = $base_user_list[$oldkey];
    if($user_row['role'] == 'company') {
      $company_tracker_list[$user_row['company']][] = $base_user_list[$oldkey];
    }
    unset($base_user_list[$oldkey]);
  }
  ksort($base_user_list);
  unset($oldkey);

?>
