<?php
/*$db_host = "localhost";
$db_name = "control1";
$db_user = "control_user";
$db_password = "7HUIQP4ptsH5vS5V";
$db_rb_string = "mysql:host=$db_host;dbname=$db_name";*/

  $system_log_entry = R::dispense($tables['log']['name']);
  if(!isset($_SERVER['HTTP_REFERER'])) { $_SERVER['HTTP_REFERER'] = 0; }
  if(!isset($_SERVER['REMOTE_HOST'])) { $_SERVER['REMOTE_HOST'] = 0; }
  if(!isset($_SERVER['REMOTE_USER'])) { $_SERVER['REMOTE_USER'] = 0; }
  if(!isset($_SERVER['HTTPS'])) { $_SERVER['HTTPS'] = 0; }

  $system_log_entry['https'] = $_SERVER['HTTPS'];
  $system_log_entry['remote_addr'] = $_SERVER['REMOTE_ADDR'];
  $system_log_entry['remote_host'] = $_SERVER['REMOTE_HOST'];
  $system_log_entry['remote_port'] = $_SERVER['REMOTE_PORT'];
  if(isset($_SESSION['user'])) {
    $system_log_entry['remote_user'] = $_SESSION['user'];
  } else {
    $system_log_entry['remote_user'] = 0;
  }
  $system_log_entry['request_uri'] = $_SERVER['REQUEST_URI'];
  $system_log_entry['app_base'] = $site_address;
  for($i = 0; $i < 4; $i++) {
    if(!isset($path_elements[$i])) { $path_elements[$i] = ''; }
    $system_log_entry['path_element_'.$i] = $path_elements[$i];
  }
  $system_log_entry['http_referrer'] = $_SERVER['HTTP_REFERER'];
  $system_log_entry['request_method'] = $_SERVER['REQUEST_METHOD'];
  $system_log_entry['http_user_agent'] = $_SERVER['HTTP_USER_AGENT'];
  $system_log_entry['request_time_float'] = $process_start_time;
  $system_log_entry['processing_time'] = microtime(TRUE) - $process_start_time;

  $system_log_entry_id = R::store( $system_log_entry );

  $temp_query = ' ORDER BY id DESC';
  $system_backup_log_load = R::findOne($tables["backup_log"]["name"],$temp_query);
  if (is_null($system_backup_log_load)) {
    $backup_needed = 1;
  } elseif(($_SERVER['REQUEST_TIME'] - $system_backup_log_load->backup_time) > $backup_interval) {
    $backup_needed = 1;
  } else {
    $backup_needed = 0;
  }

  if ($backup_needed == 1) {
    $system_backup_log_entry = R::dispense($tables['backup_log']['name']);
    $system_backup_log_entry->backup_time = $_SERVER['REQUEST_TIME'];

    $backup_file_location = $data_path.DIRECTORY_SEPARATOR.'sqldumps'.DIRECTORY_SEPARATOR.$db_name.date("Y-m-d-H-i-s").'.sql';
    //$backup_file_location = '/Users/agrim/Desktop'.DIRECTORY_SEPARATOR.$db_name.date("Y-m-d-H-i-s").'.sql';
    $system_backup_log_entry->backup_file_location = $backup_file_location;

    $return_var = NULL;
    $output = NULL;
    if(DIRECTORY_SEPARATOR == '/') {
      $mysqldump_location = '/Applications/XAMPP/xamppfiles/bin/mysqldump';
    } else {
      $mysqldump_location = 'C:\\xampp\\mysql\\bin\\mysqldump.exe';
    }

    $command = "$mysqldump_location -u$db_user -h$db_host -p$db_password $db_name | gzip > $backup_file_location";
    //exec($command);

    /* $data_log_query = "SELECT * INTO OUTFILE 'table.csv'
                          FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
                          LINES TERMINATED BY '\n'
                          FROM table";


/*
    SELECT * INTO OUTFILE 'table.csv'
    FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
    LINES TERMINATED BY '\n'
FROM table
*/
    //$mysqldumpfile = '/Applications/XAMPP/xamppfiles/bin/mysqldump';
    //$command = "$mysqldumpfile --opt -h $db_host -u $db_user -p $db_password ". "$db_name | gzip > $backup_file";
    //exec($command);
    //exec("$mysqldumpfile --user = $db_user --password = $db_password --host = $db_host $db_name > $backup_file");

    //system($command);
    //echo "Backup Done";
    $system_backup_log_entry_id = R::store( $system_backup_log_entry );
  }
 ?>
