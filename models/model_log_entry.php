<?php

// Create the 'entry_id' when requesting through SCW

if (($path_elements[1] == 'scw' ||
        ($path_elements[1] == 'cst' && isset($_POST['change']) && isset($_POST['cst_status'])))
    && isset($_POST['place_id']) && isset($_POST['company'])) {
    if (array_key_exists($_POST['place_id'], $base_student_list) && array_key_exists($_POST['company'], $company_list)) {
        $_POST['id'] = "I" . $_POST['place_id'] . $_POST['company'];
    }
}

if (($path_elements[1] == 'cst' && isset($_POST['change']) && isset($_POST['cst_status']))) {
    $current_status = $_POST['cst_status'];
} else {
    $current_status = $_POST['status'];
}

if (isset($_POST['id']) && isset($current_status)) {
    $entry = R::load($tables['entry']['name'], $_POST['id']);

    // Proceed to do anything IFF the entry exists
    if (!$entry->isEmpty()) {

        // Housekeeping
        unset($entries);
        $entries[0] = $entry;
        unset($entry);

        require $models_path . 'model_traverse_entries.php';
        $entry = $entries[0];
        $student = current($student_list);

        unset($student_list);
        unset($entries);

        // Maximum current status of student
        $student_max_status = max($student['status_levels']);

        // Any kind of change allowed through SCW without any logic implementation

        // Forward movement
        if ($path_elements[1] == 'scw' || ($path_elements[1] == 'cst' && isset($_POST['change']) && isset($_POST['cst_status']))) {
            if ($current_status > $student_max_status) {
                for ($i = $student_max_status + 1; $i <= $current_status; $i++) {
                    $entry['stat' . $i] = $i;
                    $entry['time' . $i] = $_SERVER['REQUEST_TIME'];
                }
            } // Backward movement
            elseif ($current_status < $student_max_status) {
                for ($i = $student_max_status; $i > $current_status; $i--) {
                    $entry['stat' . $i] = '0';
                    $entry['time' . $i] = '0';
                }
            }
        } // Any other request mode applicable only for all non-LP candidates
        elseif (!isset($placed_student_list[$entry['place_id']])) {
            // Adjust next location to CP for any candidate getting LP'ed
            if ($current_status == $status_levels['placed']['code']) {
                $_POST['next_company'] = $common_pool['name'];
            }

            // Prevent accidental backward movement by company tracker through stale page
            if ($student_max_status > $current_status && $path_elements[0] == 'company') {
            } // Forward movement
            elseif ($current_status > $student_max_status) {
                for ($i = $student_max_status + 1; $i <= $current_status; $i++) {
                    $entry['stat' . $i] = $i;
                    $entry['time' . $i] = $_SERVER['REQUEST_TIME'];

                    // IF candidate has 'arrived' - THEN
                    if ($i == $status_levels['arrived']['code']) {

                        // MARK 'previous_company' as the 'current_location'
                        $entry['previous_company'] = $base_student_list[$_POST['place_id']]['current_location'];

                        // AND CHANGE 'current_location' to current 'company'
                        $temp_student = R::load($tables['student_list']['name'], $_POST['place_id']);
                        if (!$temp_student->isEmpty()) {
                            $temp_student['current_location'] = $_POST['company'];
                            $temp_student_id = R::store($temp_student);
                        }

                        // AND debate on whether this should be done - MARK current 'company' as the 'next_company' in the 'previous_company's entry
                        /*  $prev_entry = R::load($tables['entry']['name'], "I".$_POST['place_id'].$entry['previous_company']);
                            if(!$prev_entry->isEmpty()) {
                              $prev_entry['next_company'] = $_POST['company'];
                              $prev_id = R::store($prev_entry);
                            } */
                    } // IF candidate is 'sent' away - THEN
                    elseif ($i == $status_levels['sent']['code']) {

                        // IF 'next_company' is NOT sent by client then calculate it
                        if (!isset($_POST['next_company'])) {
                            require $models_path . 'model_next_company.php';
                            $_POST['next_company'] = $next_company;
                            unset($next_company);
                        }

                        // MARK 'next_company' as the next 'company'
                        $entry['next_company'] = $_POST['next_company'];

                        // IF 'next_company' is 'cp'- THEN
                        if ($entry['next_company'] == $common_pool['name']) {

                            // CHANGE 'current_location' to 'cp'
                            $temp_student = R::load($tables['student_list']['name'], $_POST['place_id']);
                            if (!$temp_student->isEmpty()) {
                                $temp_student['current_location'] = $common_pool['name'];
                                $temp_student_id = R::store($temp_student);
                            }
                        } // ELSE
                        elseif (array_key_exists($entry['next_company'], $company_list)) {

                            // MARK 'intransit' for the 'next_company'
                            $next_entry = R::load($tables['entry']['name'], "I" . $_POST['place_id'] . $_POST['next_company']);
                            if (!$next_entry->isEmpty()) {
                                $next_entry['stat' . $status_levels['intransit']['code']] = $status_levels['intransit']['code'];
                                $next_entry['time' . $status_levels['intransit']['code']] = $_SERVER['REQUEST_TIME'];
                                $next_entry['previous_company'] = $_POST['company'];
                                $next_id = R::store($next_entry);
                            }
                        }
                    }
                }
            } // Backward movement
            elseif ($current_status < $student_max_status) {
                for ($i = $student_max_status; $i > $current_status; $i--) {
                    $entry['stat' . $i] = '0';
                    $entry['time' . $i] = '0';

                    // IF candidate is going from 'sent' to 'sendaway' - THEN
                    if ($i == $status_levels['sent']['code']) {
                        // IF 'next_company' is not 'cp' - THEN
                        if ($entry['next_company'] != $common_pool['name']) {
                            // REMOVE 'intransit' flag in the next 'company'
                            $next_entry = R::load($tables['entry']['name'], "I" . $_POST['place_id'] . $entry['next_company']);
                            if (!$next_entry->isEmpty()) {
                                $next_entry['stat' . $status_levels['intransit']['code']] = '0';
                                $next_entry['time' . $status_levels['intransit']['code']] = '0';
                                $next_id = R::store($next_entry);
                            }
                        }

                        // MARK 'next_company' as '0'
                        $entry['next_company'] = '0';

                        // CHANGE 'current_location' to current 'company'
                        $temp_student = R::load($tables['student_list']['name'], $_POST['place_id']);
                        if (!$temp_student->isEmpty()) {
                            $temp_student['current_location'] = $entry['company'];
                            $temp_student_id = R::store($temp_student);
                        }
                    } // IF candidate is going from 'arrived' to 'intransit' - THEN
                    elseif ($i == $status_levels['arrived']['code'] && $base_student_list[$_POST['place_id']]['current_location'] == $entry['company']) {

                        // CHANGE 'current_location' to 'previous_company'
                        if ($entry['previous_company'] == '0') {
                            $previous_location = $common_pool['name'];
                        } else {
                            $previous_location = $entry['previous_company'];
                        }

                        $temp_student = R::load($tables['student_list']['name'], $_POST['place_id']);
                        if (!$temp_student->isEmpty()) {
                            $temp_student['current_location'] = $previous_location;
                            $temp_student_id = R::store($temp_student);
                        }

                        // MARK 'previous_company' as '0'
                        $entry['previous_company'] = '0';
                    }
                }
            }
        }
        $entry_id = R::store($entry);
    }
}
?>
