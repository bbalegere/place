<?php

  $status_levels = array (
    "idle"       => array("code" => 0, "name" => "idle",        "value" => "Idle",         "text" => "Remaining",     "button_name" => "scheduled",   "button_value" => "Schedule"    ),
    "scheduled"  => array("code" => 1, "name" => "scheduled",   "value" => "Scheduled",    "text" => "Scheduled",    "button_name" => "intransit",   "button_value" => "Mark Sent"   ),
    "intransit"  => array("code" => 2, "name" => "intransit",   "value" => "In Transit",   "text" => "In Transit",           "button_name" => "arrived",     "button_value" => "Mark Arrival"),
    "arrived"    => array("code" => 3, "name" => "arrived",     "value" => "Arrived",      "text" => "Arrived",         "button_name" => "requested",   "button_value" => "In Request"     ),
    "requested"  => array("code" => 4, "name" => "requested",   "value" => "In Requested",    "text" => "In Requests",             "button_name" => "sendin",      "button_value" => "Grant In"       ),
    "sendin"     => array("code" => 5, "name" => "sendin",      "value" => "Send In Now",  "text" => "OK Send In",           "button_name" => "inside",      "button_value" => "Sent In"     ),
    "inside"     => array("code" => 6, "name" => "inside",      "value" => "Inside Panel", "text" => "Inside Panel",            "button_name" => "exit",        "button_value" => "Came Out"    ),
    "exit"       => array("code" => 7, "name" => "exit",        "value" => "Outside Panel","text" => "Exited Panel",            "button_name" => "requestaway", "button_value" => "Out Request"),
    "requestaway"=> array("code" => 8, "name" => "requestaway", "value" => "Out Requested",    "text" => "Out Requests",            "button_name" => "sendaway",    "button_value" => "Grant Out"   ),
    "sendaway"   => array("code" => 9, "name" => "sendaway",    "value" => "Send Away",    "text" => "OK Send Away",         "button_name" => "sent",        "button_value" => "Sent Away"   ),
    "sent"       => array("code" => 10,"name" => "sent",        "value" => "Sent Away",         "text" => "Sent Away",               "button_name" => "placed",      "button_value" => "Placed ??"      ),
    "placed"     => array("code" => 11,"name" => "placed",      "value" => "Placed",       "text" => "Placed!",                 "button_name" => "placed",      "button_value" => "Placed"      ),
  );

  $company_status_levels = array(
      0=>"On",
      1=>"Paused",
      2=>"Stopped",
      3=>"Exited"
  );

  $status_levels_codes = array(
    "idle",
    "scheduled",
    "intransit",
    "arrived",
    "requested",
    "sendin",
    "inside",
    "exit",
    "requestaway",
    "sendaway",
    "sent",
    "placed"
  );

  $profiles = array(
    "company",
    "floor",
    "mobile",
    "placecom",
    "control",
    "system",
    "admin",
    "backloop",
  );

  $communicate_permissions = array(
    "control"  => array(0,1,2,3,4,5,6,7,8,9,10,11),
    "placecom" => array(11),
    "company"  => array(3,4,6,7,8,10),
    "mobile"   => array(2),
    "floor"    => array(),
    "admin"    => array(),
    "system"   => array(),
    "backloop" => array(),
  );

  $card_visibility_permissions = array(
    "control"  => array(0,1,2,3,4,5,6,7,8,9,10,11),
    "placecom" => array(0,1,2,3,4,5,6,7,8,9,10,11),
    "company"  => array(1,2,3,4,5,6,7,8,9,10,11),
    "mobile"   => array(1,2),
    "floor"    => array(0,1,2,3,4,5,6,7,8,9,10,11),
    "admin"    => array(),
    "system"   => array(0,1,2,3,4,5,6,7,8,9,10,11),
    "backloop" => array(0,1,2,3,4,5,6,7,8,9,10,11),
  );

  $min_status = 0;
  $max_status = 11;
  $common_pool = array('name' => "cp", 'value' => "CP");

  $tables = array(
    "entry"        => array("name" => "entry"),
    "company_list" => array("name" => "companylist"),
    "student_list" => array("name" => "studentlist"),
    "permissions"  => array("name" => "permissions"),
    "log"          => array("name" => "log"),
    "backup_log"   => array("name" => "backuplog"),
    "users"        => array("name" => "users"),
    "userlog"      => array("name" => "userlog"),
    "mobilepref"   => array("name" => "mobilepref"),
    "options"      => array("name" => "options"),
      "studentdata" => array("name" => "studentdata"),
      "gridnoblanks" => array("name" => "gridnoblanks"),
      "raw_preferences" => array("name" =>"raw_preferences")
  );

  $allowed_level1_pages = array("control","placecom","company","mobile","floor","admin","system","user","backloop");

  $allowed_pages_list = array(
      "control" => array("scw", "cst", "ncw", "ita", "suw", "sst", "pref", "mts", "lpc", "cpp", "sys", "ica", "clw"),
    "placecom" => array("cst", "lpc", "sst", "ica", "pc", "scw", "clw"),
    "company"  => array("cst", "sst", "clw"),
    "mobile"   => array("mts", "cpp", "clw"),
    "floor"    => array("cstv", "ita", "sst", "lpc", "cpp", "clw"),
    "admin"    => array("reg", "suw", "sys", "clw"),
    "system"   => array("suw", "lpc", "cstv", "sys", "clw"),
    "backloop" => array("cstv", "sst", "lpc", "cpp", "ncw", "clw"),
  );

  $page_names_list = array(
    "scw"   => "<b>SCW</b> - Status Change Window",
    "cst"   => "<b>CST</b> - Company Status Trace",
    "ncw"   => "<b>NCW</b> - Notification Center Window",
    "ita"   => "<b>ITA</b> - Interview Time Alert",
    "suw"   => "<b>SUW</b> - System Update Window",
    "sst"   => "<b>SST</b> - Student Status Trace",
    "cstv"  => "<b>CSTV</b> - View Company Status Trace",
    "mts"   => "<b>MTS</b> - Mobile Tracker Sheet",
    "reg"   => "<b>REG</b> - User Management",
    "lpc"   => "<b>LPC</b> - LP Counter",
    "cpp"   => "<b>CPP</b> - Candidate Preference Page",
    "sys"   => "<b>SYS</b> - System Dashboard",
    "ica" => "<b>ICA</b> - Interview Company Alert",
      "pc" => "<b>PC</b> - Purged Candidate List",
      "pref" => "<b>PREF</b> - Candidate Preference List",
      "clw" => "<b>CLW</b> - Company List Window"
  );

  $backup_interval = 3600;
  $log_interval = 1800;
  $navbar_dropdown_required = 1;

  // Use below code to add any settings in the options table
  /*
  $setting1 = R::dispense($tables["options"]["name"]);
  $setting1['setting'] = "SHOW_CURR_LOC_OF_STAT2_TO_COMPANY";
  $setting1['value'] = 1;
  $id = R::store($setting1);
  */

  $settings = R::findAll($tables["options"]["name"]);
  foreach ($settings as $oldkey => $setting) {
    $settings[$setting['setting']] = $setting['value'];
    unset($settings[$oldkey]);
  }
  //print_r($settings);
$settings['PREF_DAY'] = 0;
$settings['PREF_SLOT'] = 0;

  if(isset($settings['SHOW_IDLE_TO_COMPANY']) && $settings['SHOW_IDLE_TO_COMPANY'] == 1) {
    $card_visibility_permissions['company'][] = 0;
  }

 ?>
