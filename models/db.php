<?php
  require_once $models_path.'rb.php';

  $db_host = "localhost";
  $db_name = "placement";
  $db_user = "root";
  $db_password = "";

  $db_rb_string = "mysql:host=$db_host;dbname=$db_name";
  R::setup($db_rb_string,$db_user,$db_password);
  R::setAutoResolve( TRUE );

?>
