<?php
unset($student_list);
foreach ($entries as $entry_key => $entry) {

    for ($temp_i = $min_status + 1; $temp_i <= $max_status; $temp_i++) {
        $temp_status_codes_for_entry[$temp_i - 1] = $entry['stat' . $temp_i];
        $temp_status_times_for_entry[$temp_i - 1] = $entry['time' . $temp_i];
    }

    $student_list[$entry['id']] = array(
        "id" => $entry['id'],
        "place_id" => $entry['place_id'],
        "student_roll" => $entry['student_roll'],
        "student_section" => $entry['student_section'],
        "student_name" => $entry['student_name'],
        "student_gender" => $entry['student_gender'],
        "previous_company" => $entry['previous_company'],
        "company" => $entry['company'],
        "next_company" => $entry['next_company'],
        "type" => $entry['type'],
        "gd_round" => $entry['gd_round'],
        "gd_panel" => $entry['gd_panel'],
        "shortlist" => $entry['shortlist'],
        "preference" => $entry['preference'],
        "hotlist" => $entry['hotlist'],
        "status_levels" => $temp_status_codes_for_entry,
        "status_timestamps" => $temp_status_times_for_entry,
        "later" => $entry['later'],
        "latertime" => $entry['latertime']
    );
}
?>
