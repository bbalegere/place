<?php
/**
 * Created by PhpStorm.
 * User: srividyag
 * Date: 11/01/18
 * Time: 12:27 AM
 */

/* Fetch all entries */

$entries = R::find($tables['entry']['name']);
require $models_path . 'model_traverse_entries.php';

if (isset($student_list)) {
    $shortlists_student = $student_list;
} else {
    $shortlists_student = array();
}
$stud_list = array();
foreach ($shortlists_student as $key => $shortlists_student_entry) {
    if (array_key_exists($shortlists_student_entry['place_id'], $placed_student_list)) {
        unset($shortlists_student[$key]);
    } else {
        $stud_list[$shortlists_student_entry['place_id']][$shortlists_student_entry['company']] = $shortlists_student_entry;
    }
}

/* Fetch the relative preferences of the candidate */
$gridNoBlankQuery = "select * from gridnoblanks";
$entries = R::getAll($gridNoBlankQuery);
if (isset($entries)) {
    foreach ($entries as $key => $entry) {
        $pref[$key] = $entry;
    }
}

?>