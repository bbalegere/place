<?php
  //unset($_SESSION['user']);
  $page_allowed = 0;
  $page_read = 0;
  $page_view = 0;
  $page_write = 0;
  $page_execute = 0;
  $user_logged_in = 0;

  if((!isset($_SESSION['user'])) || (!isset($_SESSION['loginhash']))) {
    //echo "no user";
    if($num_path_elements == 1) {
      if($path_elements[0] == 'user') {
        $user_logged_in = 1;
        $page_allowed = 1;
        $page_read = 1;
        $page_view = 1;
        $page_write = 1;
        $page_execute = 1;
      }
      else { header("Location: ".$website_uri."user"); die(); }
    } else { header("Location: ".$website_uri."user"); die(); }
  }
  else {
    //echo "Some user";
    $temp_query = " username = \"".$_SESSION['user']."\" AND loginhash = \"".$_SESSION['loginhash']."\"";
    $user_login = R::findOne($tables["userlog"]["name"],$temp_query);
    if(!empty($user_login)) {
      //echo "Some user";
      /*$user_logged_in = 1;
      $page_allowed = 1;
      $page_read = 1;
      $page_view = 1;
      $page_write = 1;
      $page_execute = 1;*/

      switch ($num_path_elements) {
        case 0:
          header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']); die();
          break;

        case 1:
          if($path_elements[0] == 'user') {
            $user_logged_in = 1;
            $page_allowed = 1;
            $page_read = 1;
            $page_view = 1;
            $page_write = 1;
            $page_execute = 1;
          }
          elseif($path_elements[0] != $base_user_list[$_SESSION['user']]['role']) {
            header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']); die();
          }
          elseif($path_elements[0] == $base_user_list[$_SESSION['user']]['role']) {
            if($path_elements[0] == 'company') {
              $navbar_dropdown_required = 0;
            }
            $user_logged_in = 1;
            $page_allowed = 1;
            $page_read = 1;
            $page_view = 1;
            $page_write = 1;
            $page_execute = 1;
          }
          break;

        case 2:
        case 3:
        case 4:
          if($path_elements[0] != $base_user_list[$_SESSION['user']]['role']) {
            header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']); die();
          }
          elseif($path_elements[0] == $base_user_list[$_SESSION['user']]['role']) {
            if($path_elements[0] == 'company') {
              $navbar_dropdown_required = 0;
            }
            unset($allowed_pages);
            $allowed_pages = $allowed_pages_list[$path_elements[0]];
            if(in_array($path_elements[1],$allowed_pages)){
              $user_logged_in = 1;
              $page_allowed = 1;
              $page_read = 1;
              $page_view = 1;
              $page_write = 1;
              $page_execute = 1;
            }
            else {
              header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']); die();
            }
          }
          break;

        default:
          if($path_elements[0] == 'company') {
            $navbar_dropdown_required = 0;
          }
          if($path_elements[0] != $base_user_list[$_SESSION['user']]['role']) {
            header("Location: ".$website_uri.$base_user_list[$_SESSION['user']]['role']); die();
          }
          break;
      }
    }
    else {
      if(isset($_SESSION['user'])) { unset($_SESSION['user']); }
      if(isset($_SESSION['loginhash'])) { unset($_SESSION['loginhash']); }
      header("Location: ".$website_uri."user");
      die();
    }
  }
?>
