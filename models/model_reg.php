<?php

  $form_error = 0;
  if($form_submitted == 1) {
    $user = R::dispense($tables['users']['name']);

    if(!isset($_POST['password'])) { $form_error = 1; }

    if(isset($_POST['username'])) {
      if(!array_key_exists($_POST['username'], $base_user_list)) {
        $user['username'] = $_POST['username'];
      } else { $form_error = 2; }
    } else { $form_error = 1; }

    if(isset($_POST['name'])) {
      if(!array_key_exists($_POST['name'], $base_user_list)) {
        $user['displayname'] = $_POST['name'];
      } else { $form_error = 2; }
    } else { $form_error = 1; }
      
    if(isset($_POST['role'])) {
      $user['role'] = $_POST['role'];
      if($user['role'] == 'company') {
        if(isset($_POST['company'])) {
          $user['company'] = $_POST['company'];
        } else { $form_error = 1; }
      } else { $user['company'] = 0; }
    } else { $form_error = 1; }

    if($form_error == 0) {
      $hash = getSaltedHash($_POST['username'],$_POST['password'],$namak);
      $user['hash'] = $hash;
      $user['namak'] = $namak;
      $user_id = R::store($user);
    }
  } elseif($form_submitted == 2) {
    if(isset($_POST['username'])) {
      $temp_query = " username = \"".$_POST['username']."\"";
      $delete_user = R::findOne($tables['users']['name'],$temp_query);
      R::trash($delete_user);
      $form_error = 4;
    }
  } elseif($form_submitted == 3) {
    if(isset($_POST['username'])) {
      $temp_query = " username = \"".$_POST['username']."\"";
      $modify_user = R::findOne($tables['users']['name'],$temp_query);

      if(isset($_POST['role'])) {
        $modify_user['role'] = $_POST['role'];
        if($modify_user['role'] == 'company') {
          if(isset($_POST['company'])) {
            $modify_user['company'] = $_POST['company'];
          } else { $form_error = 1; }
        } else { $modify_user['company'] = 0; }
      } else { $form_error = 1; }

      if($form_error == 0) {
        $user_id = R::store($modify_user);
        $form_error = 5;
      }
    }
    //echo "User Modified";

  }
if(isset($_POST['reset_users']))
{
    $temp_query = " role = \"company\" ";
    $results = R::findAll($tables['users']['name'], $temp_query);

    if(isset($results))
    {
        foreach($results as $key => $modify_user)
        {
            $modify_user['company'] = 'zzdummy';
            R::store($modify_user);
        }
    }
}

?>
