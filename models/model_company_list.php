<?php

  unset($company_list);
  $company_list = R::findAll($tables['company_list']['name'] );
  foreach ($company_list as $oldkey => $company_row) {
    $company_list[$company_row['company']] = $company_list[$oldkey];
    unset($company_list[$oldkey]);
  }
  ksort($company_list);
  unset($oldkey);

  //print_r($company_list);
  unset($panels_running);
  /*$temp_panel_count_query = "SELECT `company`,COUNT(`id`) as `panels_running`
                             FROM `entry`
                             WHERE (`stat".$status_levels['inside']['code']."` = '".$status_levels['inside']['code']."' AND `stat".$status_levels['exit']['code']."` = '0' )".
                                " OR (`stat".$status_levels['requested']['code']."` = '".$status_levels['requested']['code']."' AND `stat".$status_levels['inside']['code']."` = '0' )".
                                " GROUP BY `company`";*/
  $temp_panel_count_query = "SELECT `company`,COUNT(`id`) as `panels_running`
                             FROM `entry`
                             WHERE (`stat".$status_levels['requested']['code']."` = '".$status_levels['requested']['code']."' AND `stat".$status_levels['exit']['code']."` = '0' )".
                                " GROUP BY `company`";

  $panels_running = R::getAll( $temp_panel_count_query );

  foreach ($panels_running as $oldkey => $panels_running_row) {
    $panels_running[$panels_running_row['company']] = $panels_running[$oldkey];
    unset($panels_running[$oldkey]);
  }

  $temp_panel_query = "SELECT `company`,`place_id`
                       FROM `entry`
                       WHERE (`stat".$status_levels['requested']['code']."` = '".$status_levels['requested']['code']."' AND `stat".$status_levels['exit']['code']."` = '0' )";

  $panel_people = R::getAll( $temp_panel_query );

  foreach ($panel_people as $oldkey => $panel_people_row) {
    //print_r( $panels_running[$panel_people_row['company']] );
    if(isset($placed_student_list[$panel_people_row['place_id']])) {
      $panels_running[$panel_people_row['company']]['panels_running'] = $panels_running[$panel_people_row['company']]['panels_running'] - 1;
    }
  }




  $temp_queue_count_query = "SELECT `company`,COUNT(`id`) as `queues_running`
                             FROM `entry`
                             WHERE (`stat".$status_levels['scheduled']['code']."` = '".$status_levels['scheduled']['code']."' AND `stat".$status_levels['requested']['code']."` = '0' )".
                                " GROUP BY `company`";

  $queues_running = R::getAll( $temp_queue_count_query );

  foreach ($queues_running as $oldkey => $queues_running_row) {
    $queues_running[$queues_running_row['company']] = $queues_running[$oldkey];
    unset($queues_running[$oldkey]);
  }

  $temp_queue_query = "SELECT `company`,`place_id`
                       FROM `entry`
                       WHERE (`stat".$status_levels['scheduled']['code']."` = '".$status_levels['scheduled']['code']."' AND `stat".$status_levels['requested']['code']."` = '0' )";

  $queue_people = R::getAll( $temp_queue_query );

  foreach ($queue_people as $oldkey => $queue_people_row) {
    //print_r( $queues_running[$queue_people_row['company']] );
    if(isset($placed_student_list[$queue_people_row['place_id']])) {
      $queues_running[$queue_people_row['company']]['queues_running'] = $queues_running[$queue_people_row['company']]['queues_running'] - 1;
    }
  }
  /* For GD companies, we want to track the efficiency of scheduling. Fetching data for calculating Candidates_Attended / Total Scheduled Candidates */

  $temp_in_company_query = "SELECT `company`,SUM(IF(`stat".$status_levels['requested']['code']."`= '".$status_levels['requested']['code']."',1,0)) as `in_company`, COUNT(`id`) as `company_shortlists`
                             FROM `entry`
                             GROUP BY `company`";

  $in_company_people = R::getAll( $temp_in_company_query );

  foreach ($in_company_people as $oldkey => $in_company_people_row) {
    $in_company_people[$in_company_people_row['company']] = $in_company_people[$oldkey];
    unset($in_company_people[$oldkey]);
  }

?>
