<?php
  $form_error = 0;
  if($form_submitted == 1) {
    //echo "User tried logging in";
    if(isset($_POST['username']) && isset($_POST['password'])) {
      if(array_key_exists($_POST['username'],$base_user_list)) {
        $hash = getSaltedHash($_POST['username'], $_POST['password'], $base_user_list[$_POST['username']]['namak']);
        if ($hash == $base_user_list[$_POST['username']]['hash']) {
          $user_login = R::dispense($tables["userlog"]["name"]);
          
          $user_login['username'] = $_POST['username'];
          $user_login['loggedin'] = 1;
          $user_login['logintime'] = time();
          $user_login['logouttime'] = 0;
          $user_login['loginhash'] = uniqid(mt_rand(), true);
          $user_login['remote_host'] = $_SERVER['REMOTE_ADDR'];
          $_SESSION['loginhash'] = $user_login['loginhash'];
          $_SESSION['user'] = $_POST['username'];
          $login_id = R::store($user_login);
          $form_error = 9;
        } else { $form_error = 1; }
      } else { $form_error = 1; }
    } else { $form_error = 1; }

  } elseif($form_submitted == 2) {
    //echo "User tried logging out";
    $temp_query = " username = \"".$_POST['username']."\" AND loginhash = \"".$_SESSION['loginhash']."\"";
    $user_login = R::findOne($tables["userlog"]["name"],$temp_query);
    if(!empty($user_login)) {
      $user_login['loggedin'] = 0;
      $user_login['logouttime'] = time();
      unset($_SESSION['user']);
      unset($_SESSION['loginhash']);
      $login_id = R::store($user_login);
    }
  }
?>
