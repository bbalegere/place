<?php

$query = "SELECT `path_element_0`,AVG(`processing_time`) as `processing_time`, COUNT(`id`) as `visits`
         FROM `log`
         GROUP BY `path_element_0`
         ORDER BY `path_element_0`";

$sys_stats_mini = R::getAll( $query );

$query = "SELECT `path_element_0`,`path_element_1`,AVG(`processing_time`) as `processing_time`, COUNT(`id`) as `visits`
         FROM `log`
         GROUP BY `path_element_0`,`path_element_1`
         ORDER BY `path_element_0`,`path_element_1`";

$sys_stats_compact = R::getAll( $query );

$query = "SELECT `path_element_0`,`remote_user`,`http_user_agent`,AVG(`processing_time`) as `processing_time`, COUNT(`id`) as `visits`
         FROM `log`
         WHERE `remote_user` <> '0' AND `path_element_0` <> 'user'
         GROUP BY `path_element_0`,`remote_user`,`http_user_agent`
         ORDER BY `path_element_0`,`remote_user`,`http_user_agent`";

$sys_stats_users = R::getAll( $query );

/*
$query = "SELECT `app_base`,`path_element_0`,`path_element_1`,`path_element_2`,`path_element_3`,AVG(`processing_time`) as `processing_time`, COUNT(`id`) as `visits`
         FROM `log`
         GROUP BY `app_base`,`path_element_0`,`path_element_1`,`path_element_2`,`path_element_3`
         ORDER BY `app_base`,`path_element_0`,`path_element_1`,`path_element_2`,`path_element_3`";

$sys_stats = R::getAll( $query );
*/

?>
