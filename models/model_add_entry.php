<?php

if (array_key_exists($place_id, $base_student_list) && array_key_exists($company, $company_list)) {
    $id = "I" . $place_id . $company;
    $entry = R::load($tables['entry']['name'], $id);

    if ($entry->isEmpty()) {
        $student_roll = $base_student_list[$place_id]['student_roll'];
        $student_section = $base_student_list[$place_id]['student_section'];
        $student_name = $base_student_list[$place_id]['student_name'];
        $student_gender = $base_student_list[$place_id]['student_gender'];

        $prefCount = 999;

        $query_string = "UPDATE `" . $tables['raw_preferences']['name'] . "` SET relative=1 WHERE `process_id`= $place_id AND `company_code`='" . $company . "'";
        R::exec($query_string);

        $comp_query = "Select * from companylist WHERE `company`='".$company."'";
        $companies = R::getRow($comp_query);

        $day = $companies['day'];
        $slot = $companies['slot'];

        $temp_query = "Select * from gridnoblanks WHERE `id`=".$place_id." AND `day` ='".$day."' AND `slot`=".$slot;
        $gridnoblanks = R::getAll($temp_query);

        $isDataPresent = isset($gridnoblanks) && sizeof($gridnoblanks) > 0;

        $tquery = "SELECT * FROM `".$tables['raw_preferences']['name']."` WHERE `process_id`=".$place_id." AND `day`='".$day."' AND `slot`=".$slot." ORDER BY `process_id`,`preference`,`company_code`";
        $prefList = R::getAll($tquery);
        $result = array();
        foreach ($prefList as $element)
        {
            $result[$element['process_id']][] = $element;
        }
        foreach ($result as $result_key => $pref_row)
        {
            $criticality = 0;
            $count = 0;
            $isCompInserted=false;
            foreach ($pref_row as $key => $row) {
                if ($row['relative'] == 1) {
                    ++$count;
                    if ($row['company_code'] == $company) {
                        $prefCount = $count;

                    }
                    if ($isDataPresent) {
                        If($prefCount!=999)
                        {
                            $id = "I" . $place_id . $row['company_code'];
                            if(!$isCompInserted) {
                                $qstring = "INSERT INTO `" . $tables['entry']['name'] . "` (`id`, `place_id`, `student_roll`, `student_section`, `student_name`, `student_gender`, `previous_company`, `company`, `next_company`, `type`, `gd_round`, `gd_panel`, `shortlist`, `preference`, `stat1`, `time1`, `stat2`, `time2`, `stat3`, `time3`, `stat4`, `time4`, `stat5`, `time5`, `stat6`, `time6`, `stat7`, `time7`, `stat8`, `time8`, `stat9`, `time9`, `stat10`, `time10`, `stat11`, `time11`) VALUES ('" . $id . "', '" . $place_id . "', '$student_roll', '$student_section', '$student_name', '$student_gender', '0', '" . $row['company_code'] . "', '0', 'I', '0', '0', '1', '$prefCount','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');";
                                R::exec($qstring);
                                $isCompInserted = true;
                            }
                            else{
                                $update_row_q = "UPDATE `" . $tables['entry']['name'] . "` SET `preference`=".$prefCount." WHERE `id`='".$id."'";
                                R::exec($update_row_q);
                            }
                            ++$prefCount;

                        }

                        $update_row_query = "UPDATE `" . $tables['gridnoblanks']['name'] . "` SET `pref" . $count . "` = '" . $row['company_code'] . "',`criticality` =" . ++$criticality . " WHERE `id`= " .$result_key." AND `day`='".$day."' AND `slot` =".$slot;
                        R::exec($update_row_query);
                    }
                }
            }
        }

        if (!$isDataPresent) {
            $qstring = "INSERT INTO `" . $tables['entry']['name'] . "` (`id`, `place_id`, `student_roll`, `student_section`, `student_name`, `student_gender`, `previous_company`, `company`, `next_company`, `type`, `gd_round`, `gd_panel`, `shortlist`, `preference`, `stat1`, `time1`, `stat2`, `time2`, `stat3`, `time3`, `stat4`, `time4`, `stat5`, `time5`, `stat6`, `time6`, `stat7`, `time7`, `stat8`, `time8`, `stat9`, `time9`, `stat10`, `time10`, `stat11`, `time11`) VALUES ('" . $id . "', '" . $place_id . "', '$student_roll', '$student_section', '$student_name', '$student_gender', '0', '" . $company . "', '0', 'I', '0', '0', '1', '$prefCount','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');";
            R::exec($qstring);
        }



    } else {
        $message[] = $company_list[$company]['company_name'] . " Shortlist for " . $place_id . " " .
            $base_student_list[$place_id]['student_name'] . " already exists";
    }
}

?>
