<?php

/*
Status Change Window (SCW)
  i.	  Allow student status changes
  ii.	  Limit entries by shortlists – and show shortlists in pref. order if possible - N
  iii.	Allow responsive and quick feedback of status change
  iv.	  Allow GD / interview option flexibility
  v.	  Continuous auto-update of actual student statuses
  vi.   Add a separate drop down with all company names (not limited to shortlists) to override in emergency extended shortlist case
*/

/*
  Check if submit button has been pressed.
  If yes then add the entry to the DB.
  If no then bypass and simply show the SCW.
*/

if($page_allowed == 1) {

    if(isset($_POST['submit']) && $_POST['submit'] == 'Status' && $page_execute == 1) {
      require_once $models_path.'model_log_entry.php';
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['submit']) && $_POST['submit'] == 'Location' && $page_execute == 1) {
      require_once $models_path.'model_log_location.php';
      header("Location: ".$request_uri_for_form);
      die();
    }


    require_once $controllers_path.'controller_generic.php';
  }
?>
