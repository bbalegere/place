<?php

/*
Company Status Trace View (CSTV)
  i.	  Show each step-by-step status trace for students at a given company
  ii.	  Multiple steps shown for each status change (like a history)
  iii.	Not to show all companies at once for sake of data transfer load
  iv.	  Allow option to show only final checkpoint statuses for each student – that is, show only the list of students that came and went and not give the full status change history
  v.	  Buttons for requesting input/output of students from panels (IMP)
  vi.	  Show Shortlists for given company
  vii.	Show GD list and Interview list separately (based on previous screenshots)
*/

if($page_allowed == 1) {
  require_once $controllers_path.'controller_generic.php';
}

 ?>
