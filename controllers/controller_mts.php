<?php

/*
Mobile Tracker Sheet (MTS)
  i.	  Shows list of people to transfer from Common Pool to anywhere
  ii.	  Can allows input from Mobiles on which students they pick for tracking and which students are tracked
  iii.	Allows Mobiles to raise red-alerts for missing persons
*/

if($page_allowed == 1) {
  if($page_execute == 1) {
    if(isset($_POST['status'])) {
      if(in_array($_POST['status'],$communicate_permissions[$path_elements[0]])) {
            $form_submitted = 1;
            require_once $models_path.'model_log_entry.php';
      }
      header("Location: ".$request_uri_for_form);
      die();
    }
    require_once $controllers_path.'controller_generic.php';
  }
}

?>
