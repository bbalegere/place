<?php

/*
Mobile Tracker Sheet (MTS)
  i.	  Shows list of people to transfer from Common Pool to anywhere
  ii.	  Can allows input from Mobiles on which students they pick for tracking and which students are tracked
  iii.	Allows Mobiles to raise red-alerts for missing persons
*/

if($page_allowed == 1) {
  if($page_execute == 1) {
    require_once $controllers_path.'controller_generic.php';
  }
}

?>
