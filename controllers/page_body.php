<?php

  switch ($num_path_elements) {
    case 0:
      require_once $controllers_path.'controller_index.php';
      break;

    case 1:
      if(in_array($path_elements[0],$allowed_level1_pages)) {
        require_once $controllers_path.'controller_level1.php';
      }
      break;

    case 2:
    case 3:
    case 4:
      $allowed_pages = $allowed_pages_list[$path_elements[0]];
      if(in_array($path_elements[1],$allowed_pages)){
        require_once $controllers_path.'controller_'.$path_elements[1].'.php';
      }
      break;

    default:
      echo 'Silence is Golden';
      die();
      break;
  }


 ?>
