<?php

/*

Generic Controller

Calls the corresponding model based on page_read flag
Calls the corresponding view based on page_view flag

*/

  if($page_read = 1) {
    require_once $models_path.'model_'.$path_elements[1].'.php';
  }

  if($page_view = 1) {
    //require $models_path.'model_student_list.php';
    //require $models_path.'model_company_list.php';
    require_once $views_path.'page_header.php';
    require_once $views_path.'view_'.$path_elements[1].'.php';
    require_once $views_path.'page_footer.php';
  }

?>
