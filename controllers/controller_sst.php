<?php
/*
Student Status Trace (SST)
  i.	  Show each step-by-step status trace of a given student
  ii.	  Multiple steps shown for each status change (like a history)
  iii.	Not to show all students at once for sake of data transfer load
  iv.	  Allow option to show only final checkpoint statuses at each company – that is, show only the companies they went to and not give the full status change history (Like in previous year’s excel)
*/

if($page_allowed == 1) {
  require_once $controllers_path.'controller_generic.php';
}

 ?>
