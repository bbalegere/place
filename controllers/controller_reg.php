<?php

if($page_allowed == 1) {
    $form_submitted = 0;
    if(isset($_POST['register']) && $page_execute == 1) {
      $form_submitted = 1;
    } elseif (isset($_POST['delete']) && $page_execute == 1) {
      $form_submitted = 2;
    } elseif (isset($_POST['modify']) && $page_execute == 1) {
      $form_submitted = 3;
    }
    require_once $controllers_path.'controller_generic.php';
  }
 ?>
