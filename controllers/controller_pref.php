<?php
/**
 * Created by PhpStorm.
 * User: srividyag
 * Date: 11/01/18
 * Time: 12:37 AM
 */

if ($page_allowed == 1) {
    if ($page_execute == 1) {

        $pref_options = array();
        $pref_options[1] = array();
        $pref_options[1]['sort'] = 0;
        $pref_options[1]['day'] = $settings['PREF_DAY'];
        $pref_options[1]['slot'] = $settings['PREF_SLOT'];

        $pref_options[2] = array();
        $pref_options[2]['sort'] = 0;

        if ($settings['PREF_SLOT'] + 1 <= 2) {
            $pref_options[2]['day'] = $settings['PREF_DAY'];
            $pref_options[2]['slot'] = $settings['PREF_SLOT'] + 1;
        } else if ($settings['PREF_DAY'] + 1 <= 2) {
            $pref_options[2]['day'] = $settings['PREF_DAY'] + 1;
            $pref_options[2]['slot'] = 0;
        }

        //column 1 options
        if (isset($_POST['sort-1']) || isset($_POST['sortid-1']) || isset($_POST['submit-1'])) {

            $view_column = 1;
            retainOtherColumnOptions(2, 1);

            if (isset($_POST['sort-1'])) {
                $pref_options[1]['sort'] = 1;
            }

            if (isset($_POST['sortid-1'])) {
                $pref_options[1]['sort'] = 0;
            }
            if (isset($_POST['submit-1'])) {
                $pref_options[1]['day'] = isset($_POST['day-1']) ? $_POST['day-1'] : -1;
                $pref_options[1]['slot'] = isset($_POST['slot-1']) ? $_POST['slot-1'] : -1;
            }

        }
        //column 2 options
        if (isset($_POST['sort-2']) || isset($_POST['sortid-2']) || isset($_POST['submit-2'])) {

            $view_column = 2;
            retainOtherColumnOptions(1, 2);

            if (isset($_POST['sort-2'])) {
                $pref_options[2]['sort'] = 1;
            }

            if (isset($_POST['sortid-2'])) {
                $pref_options[2]['sort'] = 0;
            }
            if (isset($_POST['submit-2'])) {
                $pref_options[2]['day'] = isset($_POST['day-2']) ? $_POST['day-2'] : -1;
                $pref_options[2]['slot'] = isset($_POST['slot-2']) ? $_POST['slot-2'] : -1;
            }

        }


        require_once $controllers_path . 'controller_generic.php';
    }
}


function retainOtherColumnOptions($colNo, $otherCol)
{

    if (isset($_POST['sort-view-' . $colNo])) {
        $pref_options[$colNo]['sort'] = $_POST['sort-view-' . $colNo];
    }
    if (isset($_POST['day-' . $colNo])) {
        $pref_options[$colNo]['day'] = $_POST['day-' . $colNo];
    }
    if (isset($_POST['slot-' . $colNo])) {
        $pref_options[$colNo]['slot'] = $_POST['slot-' . $colNo];
    }
    if (isset($_POST['day-' . $otherCol])) {
        $pref_options[$otherCol]['day'] = $_POST['day-' . $otherCol];
    }
    if (isset($_POST['slot-' . $otherCol])) {
        $pref_options[$otherCol]['slot'] = $_POST['slot-' . $otherCol];
    }
    if (isset($_POST['sort-view-' . $otherCol])) {
        $pref_options[$otherCol]['sort'] = $_POST['sort-view-' . $otherCol];
    }
}

?>