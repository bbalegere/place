<?php

/*
Company Status Trace (CST)
  i.	  Show each step-by-step status trace for students at a given company
  ii.	  Multiple steps shown for each status change (like a history)
  iii.	Not to show all companies at once for sake of data transfer load
  iv.	  Allow option to show only final checkpoint statuses for each student – that is, show only the list of students that came and went and not give the full status change history
  v.	  Buttons for requesting input/output of students from panels (IMP)
  vi.	  Show Shortlists for given company
  vii.	Show GD list and Interview list separately (based on previous screenshots)
*/

if ($page_allowed == 1) {
    if ($page_execute == 1) {
        //arrived,request,sent_in,came_out,found_wanted
        if (isset($_POST['shortlist']) && $num_path_elements > 3 && $path_elements[3] = 'gd_panels' && isset($_POST['gd_students'])) {
            $company = $path_elements[2];
            foreach ($_POST['gd_students'] as $gd_key => $place_id) {
                require $models_path . 'model_add_entry.php';
            }
            header("Location: " . $request_uri_for_form);
            die();
        } elseif (isset($_POST['shortlist']) && $num_path_elements > 2) {
            $place_id = $_POST['place_id'];
            $company = $path_elements[2];
            require_once $models_path . 'model_add_entry.php';
            header("Location: " . $request_uri_for_form);
            die();
        } /* The status change from CST hits this code */
        elseif (isset($_POST['submitHotlist']) && $_POST['company'] && $_POST['place_id']) {
            $setHotlist = ($_POST['submitHotlist'] == '0' ? 'True' : 'False');
            $hotlistquerystring = "UPDATE `" . $tables['entry']['name'] . "` SET hotlist=". $setHotlist . " WHERE `place_id`='" . $_POST['place_id'] . "' AND `company`='" . $_POST['company'] . "';";
            R::exec($hotlistquerystring);
        }
        elseif (isset($_POST['status']) && isset($_POST['submitStatusChange']) && in_array($_POST['status'], $communicate_permissions[$path_elements[0]])) {
            require_once $models_path . 'model_log_entry.php';
            header("Location: " . $request_uri_for_form);
            die();
        } elseif ($path_elements[0] == 'control' && isset($_POST['set_gd']) && $_POST['set_gd'] == "R/P") {
            require_once $models_path . 'model_gd_entry.php';
            header("Location: " . $request_uri_for_form);
            die();
        } elseif (isset($_POST['set_gd']) && isset($_POST['gd_status']) && $_POST['set_gd'] == "Status" && in_array($_POST['gd_status'], $communicate_permissions[$path_elements[0]])) {
            require_once $models_path . 'model_log_gd_entry.php';
            header("Location: " . $request_uri_for_form);
            die();
        }
        elseif ($path_elements[0] == 'control' && isset($_POST['cst_status']) && isset($_POST['change'])) {
            require_once $models_path . 'model_log_entry.php';
            header("Location: " . $request_uri_for_form);
            die();
        } elseif ($path_elements[0] == 'control' && isset($_POST['later']) && isset($_POST['later_flag'])) {
            // Set later for candidate and update timestamp
            $laterquerystring = "UPDATE `" . $tables['entry']['name'] . "` SET later=True, latertime=". $_SERVER['REQUEST_TIME'] ." WHERE `place_id`='" . $_POST['place_id'] . "' AND `company`='" . $_POST['company'] . "';";
            R::exec($laterquerystring);
        }
        $sortcstview = 0;
        if (isset($_POST['sort'])) {
            $sortcstview = 1;
        }
        require_once $controllers_path . 'controller_generic.php';

    }

}
?>
