<?php

/*
Notification Center Window (NCW)
  i.	  Allow student status changes
  ii.	  Notification pops up with a status change request from a tracker – a simple accept button changes the status with feedback.
  iii.	Notifications can also be merged into the status change
*/

if($page_allowed == 1) {
  if($page_execute == 1) {
    //arrived,request,sent_in,came_out,found_wanted
    if(isset($_POST['status'])) {
      if(in_array($_POST['status'],$communicate_permissions[$path_elements[0]])) {
            $form_submitted = 1;
            require_once $models_path.'model_log_entry.php';
      }
      header("Location: ".$request_uri_for_form);
      die();
    }
    require_once $controllers_path.'controller_generic.php';
  }
}
?>
