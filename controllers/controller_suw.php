<?php

/*
Shortlist Update Window (SUW)
  i.	  Shows shortlists of any company
  ii.	  Allows updating shortlists of any company by individual person
  iii.	Allows bulk updating via CSV files
  iv.	  Can be used for initial shortlist upload as well
*/

if ($page_allowed == 1) {
    if ($page_execute == 1) {
    $message = array();

        function processFileForGD($isPurge)
        {
            if ($_FILES['gd_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['gd_file']['tmp_name']));

                $cols = $csv[0];
                $columnCount = count($cols);
            unset($csv[0]);
                $mappingData = array();
                $finalData = array();
                $i = 0;

                while ($i < $columnCount) {

                    foreach ($csv as $key => $csv_row) {
                    $givenName = $csv_row[$i];
                    $pName = $givenName;

                        if ($givenName != '' && $givenName != NULL) {

                            $id = $givenName;
                            if (array_key_exists($givenName, $mappingData)) {
                            $id = $mappingData[$givenName];

                            } else {

                                $processDataQuery = "select id, process_name from studentdata where name='" . $givenName . "' or process_name='" . $givenName . "'";
                                $data = R::getAll($processDataQuery);

                                if (count($data) > 0) {
                                $id = $data[0]['id'];
                                $pName = $data[0]['process_name'];
                                $mappingData[$givenName] = $id;
                            }

                        }

                        array_push($finalData, array($id, $pName, $cols[$i]));
                    }

                }

                    $i = $i + 1;
            }
                $fp = fopen('php://output', 'w');

                fputcsv($fp, array("ID", "Process_Name", "CompanyID"));
                $fn = "GDProcessed";
                if ($fp) {
                    header('Content-Type: text/csv');
                    header('Content-Disposition: attachment; filename="' . $fn . '.csv"');
                    header('Pragma: no-cache');
                    header('Expires: 0');

                    $lp = array();
                    if ($isPurge == true) {
                        $purgeEntryQuery = "select distinct place_id from entry where stat11=11";
                        $lp = R::getAll($purgeEntryQuery);

                    }
                    $lpList = array();
                    foreach ($lp as $key => $currentEntry) {
                        $lpList[$currentEntry['place_id']] = $currentEntry['place_id'];
                    }
                    // echo print_r($lpList);
                    foreach ($finalData as $key => $row) {
                        //echo print_r($row);
                        if (!(count($lp) > 0 && array_key_exists($row[0], $lpList)))
                            fputcsv($fp, array_values($row));
                    }

                    die;
                }

                $message[] = "File uploaded";

            } else {
                $message[] = "File not uploaded";
            }
        }


        // -----------Calculate Grid No Blanks- BEGIN-----------------------
        if (isset($_POST['grid_list_upload'])) {

            $inputDay = $_POST['day'];
            $inputSlot = $_POST['slot'];
            $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['gridnoblanks']['name'] . "` (" .
                    "`id` int(3) NOT NULL,
                    `day` varchar(15) NOT NULL,
                    `slot` int(3) NOT NULL,
                    `criticality` int(3) NULL,
          `pref1` varchar(70) DEFAULT NULL,
          `pref2` varchar(70) DEFAULT NULL,
          `pref3` varchar(70) DEFAULT NULL,
          `pref4` varchar(70) DEFAULT NULL,
          `pref5` varchar(70) DEFAULT NULL,
          `pref6` varchar(70) DEFAULT NULL,
          `pref7` varchar(70) DEFAULT NULL,
          `pref8` varchar(70) DEFAULT NULL,
          `pref9` varchar(70) DEFAULT NULL,
          `pref10` varchar(70) DEFAULT NULL,
          `pref11` varchar(70) DEFAULT NULL,
          `pref12` varchar(70) DEFAULT NULL,
          `pref13` varchar(70) DEFAULT NULL,
          `pref14` varchar(70) DEFAULT NULL,
          `pref15` varchar(70) DEFAULT NULL,
          `pref16` varchar(70) DEFAULT NULL,
          `pref17` varchar(70) DEFAULT NULL,
          `pref18` varchar(70) DEFAULT NULL,
          `pref19` varchar(70) DEFAULT NULL,
          `pref20` varchar(70) DEFAULT NULL,
          PRIMARY KEY (`id`,`day`,`slot`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);
            $temp_query = "SELECT * FROM `".$tables['raw_preferences']['name']."` WHERE `day`='".$inputDay."' AND `slot`=".$inputSlot." ORDER BY `process_id`,`preference`,`company_code`";
            $preferenceList = R::getAll($temp_query);
            $result = array();
            foreach ($preferenceList as $element) {
                $result[$element['process_id']][] = $element;
            }
             foreach ($result as $result_key => $pref_row) {
                $criticality=0;
                $insert_query = "INSERT INTO `". $tables['gridnoblanks']['name'] . "` (`id`,`day`,`slot`,`criticality`) values (".$result_key.",'".$inputDay."',".$inputSlot.",".$criticality.")";
                 R::exec($insert_query);
                 $count =0;
                 foreach ($pref_row as $key => $row)
                 {
                     if($row['relative']== 1) {
                         $insert_row_query = "UPDATE `" . $tables['gridnoblanks']['name'] . "` SET `pref" . ++$count . "` = '" . $row['company_code'] . "',`criticality` =".++$criticality." WHERE `id`= " . $result_key." AND `day`='".$inputDay."' AND `slot`=".$inputSlot;
                         R::exec($insert_row_query);
                     }
                 }
                }

                $message[] = "File uploaded";

        }
        //---------Calculate Grid No Blanks- END----------------





        if (isset($_POST['student_list_upload'])) {
            if ($_FILES['student_list_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['student_list_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['student_list']['name'] . "` (" .
                    "`id` int(3) NOT NULL,
          `student_roll` int(7) DEFAULT NULL,
          `student_section` varchar(1) DEFAULT NULL,
          `student_name` varchar(46) DEFAULT NULL,
          `student_gender` varchar(6) DEFAULT NULL,
          `current_location` varchar(30) NOT NULL,
          `next_company` varchar(30) NOT NULL,
          `placed` int(11) NOT NULL,
          `placed_company` varchar(30) NOT NULL,
          `dream` BOOLEAN NOT NULL DEFAULT FALSE,
          PRIMARY KEY (`id`),
          UNIQUE (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);

                $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['studentdata']['name'] . "` (" .
                    "`id` int(3) NOT NULL,
          `name` varchar(70) NOT NULL,
          `process_name` varchar(46) NOT NULL,
           UNIQUE (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);
                $studentListValues="";
                $studentDataValues="";

                foreach ($csv as $key => $csv_row) {

                    $studentListValues.="(" . $csv_row[0] . ", " . $csv_row[1] . ", '" . $csv_row[2] . "', '" . $csv_row[3] . "', '" . $csv_row[4] . "', '" . $csv_row[5] . "', '0', 0, '0'),";
                    $studentDataValues.="(" . $csv_row[0] . ", '" . $csv_row[6] . "', '" . $csv_row[3] . "'),";
                }
                if(substr($studentListValues, -1)==",") {
                    $studentListValues = substr($studentListValues, 0, -1);
                    $query_string = "INSERT INTO `" . $tables['student_list']['name'] . "` (`id`,`student_roll`, `student_section`, `student_name`, `student_gender`, `current_location`, `next_company`, `placed`, `placed_company`) VALUES ".$studentListValues;
                    R::exec($query_string);
                }
                if(substr($studentDataValues, -1)==",") {
                    $studentDataValues = substr($studentDataValues, 0, -1);
                    $query_string1 = "INSERT INTO `" . $tables['studentdata']['name'] . "`  (`id`, `name`, `process_name`) VALUES ".$studentDataValues;
                    R::exec($query_string1);
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }


        if (isset($_POST['static_list_upload'])) {

            if ($_FILES['student_list_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['static_list_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);


                foreach ($csv as $key => $csv_row) {

                    $id = "I" . $csv_row[0] . $csv_row[1];
                    $query_string = "UPDATE `" . $tables['entry']['name'] . "` SET time1=" . time() . ", stat1=1, gd_round=" . $csv_row[2] . " , gd_panel=" . $csv_row[3] . " WHERE `id`='" . $id . "'";
                    R::exec($query_string);
                }
                $message[] = "File uploaded";

            } else {
                $message[] = "File not uploaded";
            }
        }
        if (isset($_POST['dream_list_upload'])) {
            if ($_FILES['dream_list_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['dream_list_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                foreach ($csv as $key => $csv_row) {
                    $place_id = $csv_row[0];
                    $entry = R::load($tables['student_list']['name'], $place_id);
                    $query_string = "UPDATE `" . $tables['student_list']['name'] . "` SET dream=True WHERE `id`='" . $place_id . "'";
                    R::exec($query_string);
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }

        if (isset($_POST['hotlist_upload'])) {
            if ($_FILES['hotlist_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['hotlist_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                foreach ($csv as $key => $csv_row) {
                    $place_id = $csv_row[0];
                    $company_code = $csv_row[1];
                    $query_string = "UPDATE `" . $tables['entry']['name'] . "` SET hotlist=True WHERE `place_id`='" . $place_id . "' AND `company`='" . $company_code . "';";
                    R::exec($query_string);
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }

        if (isset($_POST['lp_list_upload'])) {
            if ($_FILES['lp_list_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['lp_list_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                foreach ($csv as $key => $csv_row) {
                    $place_id = $csv_row[0];
                    $id = "I" . $csv_row[0] . "zzlp";

                    $entry = R::load($tables['entry']['name'], $id);
                    if ($entry->isEmpty()) {
                        $student_roll = $base_student_list[$place_id]['student_roll'];
                        $student_section = $base_student_list[$place_id]['student_section'];
                        $student_name = $base_student_list[$place_id]['student_name'];
                        $student_gender = $base_student_list[$place_id]['student_gender'];

                        $place_time = $_SERVER['REQUEST_TIME'];
                        $query_string = "INSERT INTO `" . $tables['entry']['name'] . "` (`id`, `place_id`, `student_roll`, `student_section`, `student_name`, `student_gender`, `previous_company`, `company`, `next_company`, `type`, `gd_round`, `gd_panel`, `shortlist`, `stat1`, `time1`, `stat2`, `time2`, `stat3`, `time3`, `stat4`, `time4`, `stat5`, `time5`, `stat6`, `time6`, `stat7`, `time7`, `stat8`, `time8`, `stat9`, `time9`, `stat10`, `time10`, `stat11`, `time11`) VALUES ('" . $id . "', '" . $place_id . "', '$student_roll', '$student_section', '$student_name', '$student_gender', '0', '" . 'zzlp' . "', '0', 'I', '0', '0', '1','1' '1', $place_time, '2', $place_time, '3', $place_time, '4', $place_time, '5', $place_time, '6', $place_time, '7', $place_time, '8', $place_time, '9', $place_time, '10', $place_time, '11', $place_time);";

                        R::exec($query_string);
                        $message[] = $place_id . " " . $base_student_list[$place_id]['student_name'] . " LPd";

                    }

                }
            }
        }

        if (isset($_POST['gd_upload'])) {
            processFileForGD(false);

        } else if (isset($_POST['gd_purge'])) {
            processFileForGD(true);
        }


        if (isset($_POST['reset_students'])) {
            R::exec("UPDATE " . $tables['student_list']['name'] . " SET `current_location`='cp'");
            R::exec("UPDATE " . $tables['student_list']['name'] . " SET `next_company`='0'");
            R::exec("UPDATE " . $tables['student_list']['name'] . " SET `placed_company`='0'");
            R::exec("UPDATE " . $tables['student_list']['name'] . " SET `placed`='0'");
            $message[] = "All students reset to CP";
        }
        if (isset($_POST['delete_students'])) {
            R::wipe($tables['student_list']['name']);
            $message[] = "All students deleted";
        }
        if (isset($_POST['download_students'])) {
            $result = R::getAll("SELECT `id`,`student_roll`,`student_section`,`student_name`,`student_gender`,`current_location`, `dream` FROM `" . $tables['student_list']['name'] . "`");
            $current_table = $tables['student_list']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('id', 'student_roll', 'student_section', 'student_name', 'student_gender', 'current_location');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        if (isset($_POST['download_dream_students'])) {
            $result = R::getAll("SELECT `id`,`student_roll`,`student_section`,`student_name`,`student_gender`,`current_location`, `dream` FROM `" . $tables['student_list']['name'] . "` WHERE dream=True");
            $current_table = $tables['student_list']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('id', 'student_roll', 'student_section', 'student_name', 'student_gender', 'current_location');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        //----------------------------Raw Preferences upload & download function BEGIN-----------------------------------
        if(isset($_POST['raw_preference_upload'])) {
            if($_FILES['raw_preference_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['raw_preference_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                $query_string = "CREATE TABLE IF NOT EXISTS `raw_preferences` (".
                    "`id` int(4) NOT NULL AUTO_INCREMENT,
                  `process_id` int(3) NOT NULL, 
                  `company_code` varchar(70) NOT NULL,
                  `preference` int(2) NOT NULL,
                  `relative` int(2) NOT NULL,
                  `day` varchar(15) NOT NULL,
                  `slot` int(3) NOT NULL,
                   PRIMARY KEY (`id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);

                $valueList="";
                foreach ($csv as $key => $csv_row) {
                   $valueList.="(".$csv_row[0].", '".$csv_row[1]."', ".$csv_row[2].", 0,'".$csv_row[4]."',".$csv_row[5]."),";
                }
                if(substr($valueList, -1)==",") {
                    $valueList = substr($valueList, 0, -1);
                    $query_string = "INSERT INTO `".$tables['raw_preferences']['name']."` (`process_id`, `company_code`, `preference`,`relative`,`day`,`slot`) VALUES".$valueList;
                    R::exec($query_string);
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }

//----------------------------Raw Preferences upload & download function END-----------------------------------



        if (isset($_POST['get_purged_shortlist'])) {
            $company_name = $_POST['company'];
            $query_string = "SELECT DISTINCT place_id, student_name FROM `" . $tables['entry']['name'] . "` WHERE company='$company_name' AND place_id NOT IN (SELECT DISTINCT place_id FROM `" . $tables['entry']['name'] . "` WHERE stat11='11')";
            $result_company = R::getAll($query_string);
            $current_table = $company_name;
            $row = array('place_id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result_company) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result_company as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }


        if (isset($_POST['get_all_purged_shortlists'])) {
            $in_day = 0;
            $in_slot = 0;

            if (isset($_POST['day_short'])) {
                $in_day = $_POST['day_short'];
            }
            if (isset($_POST['slot_short'])) {
                $in_slot = $_POST['slot_short'];
            }


            $query_string = "Select company from companylist where day = '$in_day' and slot = '$in_slot' ";
            $companylist = R::getAll($query_string);

            ob_end_clean();
            $fp = fopen('php://output', 'w');
            header('Content-Type: text/csv');
            header('Content-Disposition: attachment; filename="Total Purged Shortlists.csv"');
            header('Pragma: no-cache');
            header('Expires: 0');

            foreach ($companylist as $key => $company) {

                $companyname = array_values($company)[0];
                $query = "SELECT DISTINCT place_id, student_name FROM `" . $tables['entry']['name'] . "` WHERE company='$companyname' AND place_id NOT IN (SELECT DISTINCT place_id FROM `" . $tables['entry']['name'] . "` WHERE stat11='11')";
                $studenttotal = R::getAll($query);

                $studentlist = array();
                $studentlist[] = $companyname;
                $studentPIDlist = array();
                $studentPIDlist[] = $companyname;
                $blankrow = array();
                foreach ($studenttotal as $key => $student) {

                    $studentPIDlist[] = array_values($student)[0];
                    $studentlist[] = array_values($student)[1];

                }
                fputcsv($fp, array_values($studentPIDlist));
                fputcsv($fp, array_values($studentlist));
                fputcsv($fp, array_values($blankrow));


            }
            exit();

            }


        if (isset($_POST['get_hotlist'])) {
            $company_name = $_POST['company'];
            $query_string = "SELECT DISTINCT place_id, student_name FROM `" . $tables['entry']['name'] . "` WHERE company='$company_name' AND hotlist=1";
            $result_company = R::getAll($query_string);
            $current_table = $company_name;
            $row = array('place_id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result_company) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table ."_hotlist". '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result_company as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }




        if (isset($_POST['get_non_shortlist'])) {

            $company_name = $_POST['company'];
            $query_string = "SELECT  id, student_name FROM studentlist WHERE id not in (select place_id from entry where company = '$company_name') and id not in (select place_id from entry where stat11 = '11')";
            $result_company = R::getAll($query_string);
            $current_table = $company_name;
            $row = array('place_id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result_company) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result_company as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        if (isset($_POST['get_total_shortlist'])) {
            $in_day = 0;
            $in_slot = 0;

            if (isset($_POST['day_short'])) {
                $in_day = $_POST['day_short'];
            }
            if (isset($_POST['slot_short'])) {
                $in_slot = $_POST['slot_short'];
            }


            $query_string = "Select company from companylist where day = '$in_day' and slot = '$in_slot' ";
            $companylist = R::getAll($query_string);

            foreach ($companylist as $key => $company) {

                $fp = fopen('php://output', 'w+');
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="Total Shortlists.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');

                //fputcsv($fp, array_values($company));
                $companyname = array_values($company)[0];
                $query = "Select place_id from entry where company = '$companyname' and place_id not in (select place_id from entry where stat11='11')";
                $studenttotal = R::getAll($query);
                $studentlist = array();
                $studentlist[] = $companyname;
                foreach ($studenttotal as $key => $student) {
                    $studentlist[] = array_values($student)[0];
                }
                fputcsv($fp, array_values($studentlist));
                fclose($fp);
            }

        }

        if (isset($_POST['add_single_shortlist'])) {
            if (isset($_POST['place_id']) && isset($_POST['company'])) {
                $id = "I" . $_POST['place_id'] . $_POST['company'];
                $entry = R::load($tables['entry']['name'], $id);
                if ($entry->isEmpty()) {
                    $student_roll = $base_student_list[$_POST['place_id']]['student_roll'];
                    $student_section = $base_student_list[$_POST['place_id']]['student_section'];
                    $student_name = $base_student_list[$_POST['place_id']]['student_name'];
                    $student_gender = $base_student_list[$_POST['place_id']]['student_gender'];
                    $preference_extend = 999;
                    $query_string = "INSERT INTO `" . $tables['entry']['name'] . "` (`id`, `place_id`, `student_roll`, `student_section`, `student_name`, `student_gender`, `previous_company`, `company`, `next_company`, `type`, `gd_round`, `gd_panel`, `shortlist`, `preference`, `stat1`, `time1`, `stat2`, `time2`, `stat3`, `time3`, `stat4`, `time4`, `stat5`, `time5`, `stat6`, `time6`, `stat7`, `time7`, `stat8`, `time8`, `stat9`, `time9`, `stat10`, `time10`, `stat11`, `time11`) VALUES ('" . $id . "', '" . $_POST['place_id'] . "', '$student_roll', '$student_section', '$student_name', '$student_gender', '0', '" . $_POST['company'] . "', '0', 'I', '0', '0', '1','$preference_extend','0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');";


                    R::exec($query_string);
                    $message[] = $company_list[$_POST['company']]['company_name'] . " shortlist for " . $_POST['place_id'] . " " . $base_student_list[$_POST['place_id']]['student_name'] . " added";
                } else {
                    $message[] = "Shortlist already exists";
                }
            } else {
                $message[] = "No shortlist added";
            }
        }
        if (isset($_POST['remove_single_shortlist'])) {
            if (isset($_POST['place_id']) && isset($_POST['company'])) {
                $id = "I" . $_POST['place_id'] . $_POST['company'];
                $entry = R::load($tables['entry']['name'], $id);
                if ($entry->isEmpty()) {
                    $message[] = "No shortlist removed";
                } else {
                    R::trash($entry);
                    $message[] = $company_list[$_POST['company']]['company_name'] . " shortlist for " . $_POST['place_id'] . " " . $base_student_list[$_POST['place_id']]['student_name'] . " removed";
                }
            } else {
                $message[] = "No shortlist removed";
            }
        }
        if (isset($_POST['shortlist_upload'])) {
            if ($_FILES['shortlist_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['shortlist_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['entry']['name'] . "` (" .
                    "`id` varchar(40) NOT NULL,
         `place_id` int(3) DEFAULT NULL,
         `student_roll` int(7) DEFAULT NULL,
         `student_section` varchar(1) DEFAULT NULL,
         `student_name` varchar(46) DEFAULT NULL,
         `student_gender` varchar(6) DEFAULT NULL,
         `previous_company` varchar(30) NOT NULL,
         `company` varchar(30) DEFAULT NULL,
         `next_company` varchar(30) DEFAULT NULL,
         `type` varchar(1) DEFAULT NULL,
         `gd_round` int(11) DEFAULT NULL,
         `gd_panel` int(11) NOT NULL,
         `shortlist` int(1) DEFAULT NULL,
         `preference` int(2) DEFAULT NULL,
         `stat1` int(1) DEFAULT NULL,
         `time1` int(1) DEFAULT NULL,
         `stat2` int(1) DEFAULT NULL,
         `time2` int(1) DEFAULT NULL,
         `stat3` int(1) DEFAULT NULL,
         `time3` int(1) DEFAULT NULL,
         `stat4` int(1) DEFAULT NULL,
         `time4` int(1) DEFAULT NULL,
         `stat5` int(1) DEFAULT NULL,
         `time5` int(1) DEFAULT NULL,
         `stat6` int(1) DEFAULT NULL,
         `time6` int(1) DEFAULT NULL,
         `stat7` int(1) DEFAULT NULL,
         `time7` int(1) DEFAULT NULL,
         `stat8` int(1) DEFAULT NULL,
         `time8` int(1) DEFAULT NULL,
         `stat9` int(1) DEFAULT NULL,
         `time9` int(1) DEFAULT NULL,
         `stat10` int(1) DEFAULT NULL,
         `time10` int(1) DEFAULT NULL,
         `stat11` int(1) DEFAULT NULL,
         `time11` int(1) DEFAULT NULL,
         `hotlist` BOOLEAN NOT NULL DEFAULT FALSE,
         `later` BOOLEAN NOT NULL DEFAULT FALSE,
         `latertime` int(1) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);

                foreach ($csv as $key => $csv_row) {

                    $place_id = $csv_row[0];
                    $company = $csv_row[1];
                    $preference = $csv_row[2];

                    require $models_path.'model_add_entry.php';
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }
        if (isset($_POST['entry_table_upload'])) {
            if ($_FILES['entry_table_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['entry_table_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['entry']['name'] . "` (" .
                    "`id` varchar(40) NOT NULL,
         `place_id` int(3) DEFAULT NULL,
         `student_roll` int(7) DEFAULT NULL,
         `student_section` varchar(1) DEFAULT NULL,
         `student_name` varchar(46) DEFAULT NULL,
         `student_gender` varchar(6) DEFAULT NULL,
         `previous_company` varchar(30) NOT NULL,
         `company` varchar(30) DEFAULT NULL,
         `next_company` varchar(30) DEFAULT NULL,
         `type` varchar(1) DEFAULT NULL,
         `gd_round` int(11) DEFAULT NULL,
         `gd_panel` int(11) NOT NULL,
         `shortlist` int(1) DEFAULT NULL,
         `preference` int(2) DEFAULT NULL,
         `stat1` int(1) DEFAULT NULL,
         `time1` int(1) DEFAULT NULL,
         `stat2` int(1) DEFAULT NULL,
         `time2` int(1) DEFAULT NULL,
         `stat3` int(1) DEFAULT NULL,
         `time3` int(1) DEFAULT NULL,
         `stat4` int(1) DEFAULT NULL,
         `time4` int(1) DEFAULT NULL,
         `stat5` int(1) DEFAULT NULL,
         `time5` int(1) DEFAULT NULL,
         `stat6` int(1) DEFAULT NULL,
         `time6` int(1) DEFAULT NULL,
         `stat7` int(1) DEFAULT NULL,
         `time7` int(1) DEFAULT NULL,
         `stat8` int(1) DEFAULT NULL,
         `time8` int(1) DEFAULT NULL,
         `stat9` int(1) DEFAULT NULL,
         `time9` int(1) DEFAULT NULL,
         `stat10` int(1) DEFAULT NULL,
         `time10` int(1) DEFAULT NULL,
         `stat11` int(1) DEFAULT NULL,
         `time11` int(1) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                R::exec($query_string);

                unset($query_string);

                foreach ($csv as $key => $csv_row) {
                    $entry = R::load($tables['entry']['name'], $csv_row[0]);
                    if ($entry->isEmpty()) {
                        $query_string = "INSERT INTO `" . $tables['entry']['name'] . "` (`id`, `place_id`, `student_roll`, `student_section`, `student_name`, `student_gender`, `previous_company`, `company`, `next_company`, `type`, `gd_round`, `gd_panel`, `shortlist`, `stat1`, `time1`, `stat2`, `time2`, `stat3`, `time3`, `stat4`, `time4`, `stat5`, `time5`, `stat6`, `time6`, `stat7`, `time7`, `stat8`, `time8`, `stat9`, `time9`, `stat10`, `time10`, `stat11`, `time11`) VALUES ('$csv_row[0]', '$csv_row[1]', '$csv_row[2]', '$csv_row[3]', '$csv_row[4]', '$csv_row[5]', '$csv_row[6]', '$csv_row[7]', '$csv_row[8]', '$csv_row[9]', '$csv_row[10]', '$csv_row[11]', '$csv_row[12]', '$csv_row[13]', '$csv_row[14]', '$csv_row[15]', '$csv_row[16]', '$csv_row[17]', '$csv_row[18]', '$csv_row[19]', '$csv_row[20]', '$csv_row[21]', '$csv_row[22]', '$csv_row[23]', '$csv_row[24]', '$csv_row[25]', '$csv_row[26]', '$csv_row[27]', '$csv_row[28]', '$csv_row[29]', '$csv_row[30]', '$csv_row[31]', '$csv_row[32]', '$csv_row[33]', '$csv_row[34]');";
                        R::exec($query_string);
                    } else {
                        $duplicates = 1;
                    }
                }
                if (isset($duplicates)) {
                    $msg = " but duplicates were removed";
                } else {
                    $msg = "";
                }
                $message[] = "File uploaded" . $msg;
            } else {
                $message[] = "File not uploaded";
            }
        }
        if (isset($_POST['download_shortlists'])) {
            $result = R::getAll("SELECT `place_id`,`company` FROM `" . $tables['entry']['name'] . "`");
            $current_table = $tables['entry']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('place_id', 'company');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="shortlists.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);

                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }


        if (isset($_POST['download_hotlists'])) {
            $result = R::getAll("SELECT `place_id`,`company` FROM `" . $tables['entry']['name'] . "` WHERE `hotlist`=1");
            $current_table = $tables['entry']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('place_id', 'company');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="hotlists.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);

                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }


        if (isset($_POST['download_entry_table'])) {
            $result = R::getAll("SELECT * FROM `" . $tables['entry']['name'] . "`");
            $current_table = $tables['entry']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('id', 'place_id', 'student_roll', 'student_section', 'student_name', 'student_gender', 'previous_company', 'company', 'next_company', 'type', 'gd_round', 'gd_panel', 'shortlist', 'stat1', 'time1', 'stat2', 'time2', 'stat3', 'time3', 'stat4', 'time4', 'stat5', 'time5', 'stat6', 'time6', 'stat7', 'time7', 'stat8', 'time8', 'stat9', 'time9', 'stat10', 'time10', 'stat11', 'time11');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        if (isset($_POST['download_candidates'])) {

            $result = R::getAll("SELECT DISTINCT id, name FROM `studentdata` where id 
IN(SELECT DISTINCT place_id FROM `entry` where stat10=0 and company IN(select company from companylist where company_status <=1 and type='i' 
and company not in ('zzlp','00dummy')) and place_id not in (select DISTINCT place_id from entry where stat11=11))");

            $current_table = "Candidates";
            if (!$result) die('Couldn\'t fetch records');

            $row = array('id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        if (isset($_POST['lp_students'])) {

            $result = R::getAll("Select DISTINCT id, name from `studentdata` where id IN 
(Select DISTINCT place_id from entry where stat11=11)");

            $current_table = "LPCandidates";
            if (!$result) die('Couldn\'t fetch records');

            $row = array('id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }

        if (isset($_POST['reset_shortlists'])) {
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `previous_company`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `next_company`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `gd_round`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `gd_panel`='0'");
            for ($i = $min_status + 1; $i <= $max_status; $i++) {
                R::exec("UPDATE " . $tables['entry']['name'] . " SET `stat" . $i . "`='0'");
                R::exec("UPDATE " . $tables['entry']['name'] . " SET `time" . $i . "`='0'");
            }
            $message[] = "All shortlists reset";
        }
        if (isset($_POST['delete_shortlists'])) {
            R::wipe($tables['entry']['name']);
            $message[] = "All shortlists deleted";
        }
        if (isset($_POST['reset_non_placed_shortlists'])) {
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `previous_company`='0' WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `next_company`='0'     WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `gd_round`='0' WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            R::exec("UPDATE " . $tables['entry']['name'] . " SET `gd_panel`='0' WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            for ($i = $min_status + 1; $i <= $max_status; $i++) {
                R::exec("UPDATE " . $tables['entry']['name'] . " SET `stat" . $i . "`='0'     WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
                R::exec("UPDATE " . $tables['entry']['name'] . " SET `time" . $i . "`='0'     WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            }
            $message[] = "All non-LP shortlists reset";
        }
        if (isset($_POST['delete_non_placed_shortlists'])) {
            R::exec("DELETE FROM " . $tables['entry']['name'] . " WHERE `stat" . $status_levels['placed']['code'] . "`='0'");
            $message[] = "All non-LP shortlists deleted";
        }
        if (isset($_POST['add_company'])) {
            if (isset($_POST['company']) &&
                isset($_POST['company_name']) &&
                isset($_POST['panels']) &&
                isset($_POST['type']) &&
                isset($_POST['company_status'])) {

                $query = " company = :company";
                $query_bindings = array("company" => $_POST['company']);
                $check_company = R::find($tables['company_list']['name'], $query, $query_bindings);
                if (empty($check_company)) {
                    $new_company = R::dispense($tables['company_list']['name']);
                    $new_company['company'] = $_POST['company'];
                    $new_company['company_name'] = $_POST['company_name'];
                    $new_company['panels'] = $_POST['panels'];
                    $new_company['type'] = $_POST['type'];
                    $new_company['company_status'] = $_POST['company_status'];
                    $new_company['day'] = $_POST['day'];
                    $new_company['slot'] = $_POST['slot'];
                    $new_company['freeflow'] = $_POST['slot'];
                    $id = R::store($new_company);
                    $message[] = "Company " . $_POST['company_name'] . " stored";
                } else {
                    $message[] = "Company " . $_POST['company_name'] . " exists, please use modify feature to make any changes";
                }
            } else {
                $message[] = "No company stored";
            }
        }
        if (isset($_POST['company_list_upload'])) {
            if ($_FILES['company_list_file']['error'] == 0) {
                $csv = array_map('str_getcsv', file($_FILES['company_list_file']['tmp_name']));
                $cols = $csv[0];
                unset($csv[0]);

                $query_string = "CREATE TABLE IF NOT EXISTS `" . $tables['company_list']['name'] . "` (" .
                    " `id` int(2) NOT NULL AUTO_INCREMENT,
           `company` varchar(30) DEFAULT NULL,
           `company_name` varchar(30) NOT NULL,
           `panels` int(1) DEFAULT NULL,
           `type` varchar(1) DEFAULT NULL,
           `company_status` int(11) NOT NULL,
           `day` varchar(5) DEFAULT NULL,
           `slot` varchar(5) DEFAULT NULL,
           `freeflow` int(1) DEFAULT NULL,
           PRIMARY KEY (`id`),
           UNIQUE (`id`)
         ) ENGINE=InnoDB DEFAULT CHARSET=utf8;;";

                R::exec($query_string);

                unset($query_string);

                foreach ($csv as $key => $csv_row) {
                    $query = " company = :company";
                    $query_bindings = array("company" => $csv_row[0]);
                    $check_company = R::findOne($tables['company_list']['name'], $query, $query_bindings);
                    if (empty($check_company)) {
                        $new_company = R::dispense($tables['company_list']['name']);
                        $new_company['company'] = $csv_row[0];
                        $new_company['company_name'] = $csv_row[1];
                        $new_company['panels'] = $csv_row[2];
                        $new_company['type'] = $csv_row[3];
                        $new_company['company_status'] = $csv_row[4];
                        $new_company['day'] = $csv_row[5];
                        $new_company['slot'] = $csv_row[6];
                        $new_company['freeflow'] = $csv_row[7];
                        $id = R::store($new_company);
                    } else {
                        $check_company['company_name'] = $csv_row[1];
                        $check_company['panels'] = $csv_row[2];
                        $check_company['type'] = $csv_row[3];
                        $check_company['company_status'] = $csv_row[4];
                        $check_company['day'] = $csv_row[5];
                        $check_company['slot'] = $csv_row[6];
                        $check_company['freeflow'] = $csv_row[7];
                        $id = R::store($check_company);
                    }
                }
                $message[] = "File uploaded";
            } else {
                $message[] = "File not uploaded";
            }
        }
        if (isset($_POST['download_company_list'])) {
            $result = R::getAll("SELECT `company`,`company_name`,`panels`,`type`,`company_status`,`day`,`slot`,`freeflow` FROM `" . $tables['company_list']['name'] . "`");
            $current_table = $tables['company_list']['name'];
            if (!$result) die('Couldn\'t fetch records');

            $row = array('company', 'company_name', 'panels', 'type', 'company_status', 'day', 'slot', 'freeflow');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }
        if (isset($_POST['modify_company'])) {
            if (isset($_POST['company']) &&
                isset($_POST['company_name']) &&
                isset($_POST['panels']) &&
                isset($_POST['type']) &&
                isset($_POST['company_status']) &&
                isset($_POST['day']) &&
                isset($_POST['slot']) &&
                isset($_POST['freeflow']) && isset($_POST['company_room'])) {

                $query = " company = :company";
                $query_bindings = array("company" => $_POST['company']);
                $check_company = R::findOne($tables['company_list']['name'], $query, $query_bindings);
                if (!empty($check_company)) {

                    $check_company['company_name'] = $_POST['company_name'];
                    $check_company['panels'] = $_POST['panels'];
                    $check_company['type'] = $_POST['type'];
                    $check_company['company_status'] = $_POST['company_status'];
                    $check_company['day'] = $_POST['day'];
                    $check_company['slot'] = $_POST['slot'];
                    $check_company['freeflow'] = $_POST['freeflow'];
                    $check_company['company_room'] = $_POST['company_room'];
                    $id = R::store($check_company);
                    //Trigger refresh. In UI content not updated.
                    header("Refresh:0");
                    $message[] = "Company " . $_POST['company_name'] . " Modified";
                } else {
                    $message[] = "Company " . $_POST['company_name'] . " does not exist";
                }
            } else {
                $message[] = "No company modified";
            }

        }
        if (isset($_POST['in_system_candidates'])) {
            $in_day = 0;
            $in_slot = 0;

            if (isset($_POST['day'])) {
                $in_day = $_POST['day'];
            }
            if (isset($_POST['slot'])) {
                $in_slot = $_POST['slot'];
            }

            $result = R::getAll("SELECT DISTINCT place_id, student_name, shortlist, stat1 FROM `entry` where shortlist=1 and stat1=0 and company IN(select company from companylist where company_status=0 and company not in ('zzlp','00dummy') and day=" . $in_day . " and slot=" . $in_slot . ")");

            $current_table = "InSystemCandidates";

            if (!$result) die('Couldn\'t fetch records');
            $row = array('id', 'student_name');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="' . $current_table . '.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }
        if (isset($_POST['delete_company'])) {
            if (isset($_POST['company'])) {
                $query = " company = :company";
                $query_bindings = array("company" => $_POST['company']);
                $check_company = R::findOne($tables['company_list']['name'], $query, $query_bindings);
                if (!empty($check_company)) {
                    R::trash($check_company);
                    $message[] = "Company " . $_POST['company_name'] . " Deleted";
                } else {
                    $message[] = "Company " . $_POST['company_name'] . " does not exist";
                }
            } else {
                $message[] = "No company deleted";
            }
        }
        /*
         * Swapnil's code: LP download SUW*/
        if (isset($_POST['download_LP_List'])) {
            $temp_query = "SELECT  `place_id`,`student_roll`, `student_name`,`company` FROM `".$tables['entry']['name']."` WHERE `stat".$status_levels['placed']['code']."`=".$status_levels['placed']['code']." ORDER BY `time".$status_levels['placed']['code']."` DESC";
            $result = R::getAll($temp_query);
            if (!$result) die('Couldn\'t fetch records');

            $row = array('Process_id', 'Student_roll','Student_Name','Company_code');
            $fp = fopen('php://output', 'w');

            if ($fp && $result) {
                header('Content-Type: text/csv');
                header('Content-Disposition: attachment; filename="LP_LIST.csv"');
                header('Pragma: no-cache');
                header('Expires: 0');
                fputcsv($fp, $row);
                foreach ($result as $key => $row) {
                    fputcsv($fp, array_values($row));
                }
                die;
            }
        }
        require_once $controllers_path . 'controller_generic.php';
    }
}

?>
