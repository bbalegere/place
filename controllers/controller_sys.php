<?php

if($page_allowed == 1) {
  if($page_execute == 1) {
    if(isset($_POST['optimizedb'])) {
      // add Indexes
      $optim_list[$tables['entry']['name']] = array('id', 'place_id', 'previous_company', 'company', 'next_company','stat1','time1','stat2','time2','stat3','time3','stat4','time4','stat5','time5','stat6','time6','stat7','time7','stat8','time8','stat9','time9','stat10','time10','stat11','time11');
      $optim_list[$tables['student_list']['name']] = array('id', 'current_location');
      $optim_list[$tables['company_list']['name']] = array('id', 'company');
      foreach ($optim_list as $optim_list_key => $optim_list_table) {
        foreach ($optim_list_table as $key => $optim_list_column) {
          $query = "ALTER TABLE `".$optim_list_key."` ADD INDEX IF NOT EXISTS `".$optim_list_column."` (`".$optim_list_column."`)";
          R::exec($query);
        }
      }
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['optimizelog'])) {
      $optim_list[$tables['log']['name']] = array('id','remote_addr','remote_user','request_uri','http_referrer','request_method','request_time_float','processing_time','path_element_0','path_element_1','path_element_2','path_element_3','app_base');
      foreach ($optim_list as $optim_list_key => $optim_list_table) {
        foreach ($optim_list_table as $key => $optim_list_column) {
          $query = "ALTER TABLE `".$optim_list_key."` ADD INDEX IF NOT EXISTS `".$optim_list_column."` (`".$optim_list_column."`)";
          R::exec($query);
        }
      }
      $delta = microtime(TRUE) - $log_interval;
      $query = "DELETE FROM `".$tables['log']['name']."` WHERE `request_time_float` < ".$delta;
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['clearlog'])) {
      $query = "DROP TABLE `".$tables['log']['name']."`";
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['clearuserlog'])) {
      $query = "DROP TABLE `".$tables['userlog']['name']."`";
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['clearbackuplog'])) {
      $query = "DROP TABLE `".$tables['backup_log']['name']."`";
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['clearmobilepref'])) {
      $query = "DROP TABLE `".$tables['mobilepref']['name']."`";
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif (isset($_POST['resetcompanytrackers'])) {
      $query = "UPDATE `".$tables['users']['name']."` SET `company` = '00dummy' WHERE `role` = 'company'";
      R::exec($query);
      header("Location: ".$request_uri_for_form);
      die();
    }


    require_once $controllers_path.'controller_generic.php';
  }
}

 ?>
