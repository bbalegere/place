<?php

  if($page_allowed == 1) {
    $form_submitted = 0;
    if(isset($_POST['login'])) {
      $form_submitted = 1;
    } elseif (isset($_POST['logout'])) {
      $form_submitted = 2;
    }

    if($form_submitted == 1 || $form_submitted == 2) {
      require $models_path."model_user.php";
      header("Location: ".$request_uri_for_form);
      die();
    }

    if($page_view == 1) {
      require $views_path."view_user.php";
    }
  }
 ?>
