<?php

  function card_company_link($func_company_name,$func_company_value) {
    return "<a href=\"".$server_address.$site_address.$path_elements[0]."/cst/".$func_company_name."\" target=\"_blank\" >$func_company_value</a>";
  }

  function getSaltedHash($username, $password, $salt) {
    $hash = $password.$salt.$username;
    for ($i = 0; $i < 25; $i++) {
        $hash = hash('sha512', $username . $hash . $salt . $hash . $password);
    }
    return $hash;
  }
 ?>
