<?php

if($page_allowed == 1) {
  if($page_execute == 1) {

    if(isset($_POST['ask']) && isset($_POST['place_id']) && isset($_POST['choice1'])) {
      $backloop = R::dispense($tables['mobilepref']['name']);
      $backloop['place_id'] = $_POST['place_id'];
      $backloop['choice1'] = $_POST['choice1'];
      if(!isset($_POST['choice2'])) { $_POST['choice2'] = '---'; }
      if(!isset($_POST['choice3'])) { $_POST['choice3'] = '---'; }
      $backloop['choice2'] = $_POST['choice2'];
      $backloop['choice3'] = $_POST['choice3'];
      $backloop['choice'] = '0';
      $backloop['time_asked'] = time();
      $backloop['time_replied'] = 0;
      if(!isset($_POST['role'])) { $_POST['role'] = 'any'; }
      $backloop['role'] = $_POST['role'];
      R::store($backloop);
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif(isset($_POST['tell']) && isset($_POST['choice']) && isset($_POST['id'])) {
      $mobile = R::load($tables['mobilepref']['name'],$_POST['id']);
      if(!$mobile->isEmpty()) {
        $mobile['choice'] = $_POST['choice'];
        $mobile['time_replied'] = time();
        R::store($mobile);
      }
      header("Location: ".$request_uri_for_form);
      die();
    }
    elseif(isset($_POST['delete']) && isset($_POST['id'])) {
      $mobile = R::load($tables['mobilepref']['name'],$_POST['id']);
      if(!$mobile->isEmpty()) {
        R::trash($mobile);
      }
      header("Location: ".$request_uri_for_form);
      die();
    }
    require_once $controllers_path.'controller_generic.php';
  }
}

?>
